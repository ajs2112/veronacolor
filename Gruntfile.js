module.exports = function(grunt){
	//Project configuration
	grunt.initConfig({
		/* CODE SWITCH */
		code_switch: {
            start: {
                options: {
                    environment: 'prod',
                    env_char: '#',
                    blocks: [{
                            code_tag: 'env:dev',
                            grunt_option: 'dev'
                        }, {
                            code_tag: 'env:prod',
                            grunt_option: 'prod'
                        }]
 
                },
                files: {
                	'front/app/app.js' : 'front/app/app.js',
                    'index.html': 'index.html'
                }
            },
            end: {
                options: {
                    environment: 'dev',
                    env_char: '#',
                    blocks: [{
                            code_tag: 'env:dev',
                            grunt_option: 'dev'
                        }, {
                            code_tag: 'env:prod',
                            grunt_option: 'prod'
                        }]
 
                },
                files: {
                	'front/app/app.js' : 'front/app/app.js',
                    'index.html': 'index.html'
                }
            }
        },
        /* JS-HINT */
		jshint:{
			all:['front/app/app.config.js']
		},
		cssmin: {
			pace:{
				src:'front/assets/css/pace.css',
				dest:'front/assets/css/pace.min.css'
			},
			misestilos:{
				src:'front/assets/css/mis-estilos.css',
				dest:'front/assets/css/mis-estilos.min.css'
			},
			menuestilos:{
				src:'front/assets/css/menu-estilos.css',
				dest:'front/assets/css/menu-estilos.min.css'
			}						
		},
		/* CONCAT */
		concat:{
			css: {
				src: [
					//'front/assets/css/pace.min.css',
					'front/assets/css/font-awesome.min.css',
					'front/assets/css/angular-material.min.css',
					'front/assets/css/ui-grid.min.css',
					'front/assets/css/mis-estilos.min.css',
					'front/assets/css/menu-estilos.min.css'					

				],
				dest: 'front/dist/app.min.css'
			},
			vendor: {
				src: [
					//'front/assets/js/pace.min.js',
					'front/assets/js/angular.min.js',
					'front/assets/js/angular-animate.min.js',
					'front/assets/js/angular-aria.min.js',
					'front/assets/js/angular-messages.min.js',
					'front/assets/js/angular-jwt.min.js',
					'front/assets/js/angular-idle.min.js',
					'front/assets/js/angular-ui-router.min.js',
					'front/assets/js/ct-ui-router-extras.min.js',
					'front/assets/js/ocLazyLoad.min.js',
					'front/assets/js/angular-material.min.js',
					'front/assets/js/ui-grid.min.js', 
					'front/assets/js/locale.min.js',
					'front/assets/js/jspdf.min.js',
					'front/assets/js/jspdf.plugin.autotable.min.js',										 
				],
				dest: 'front/dist/vendor.js'
			},
			dist: {
				src: ['front/app/app.js', 'front/app/app.config.js'],
				dest: 'front/dist/app.js'
			}
		},
		/* UGLIFY */
		uglify: {
			dist: {
				src:'front/dist/app.js',
				dest: 'front/dist/app.min.js'
			}
		},

		/* HTMLMIN */
		htmlmin: {                                     // Task 
		    dist: {                                      // Target 
		      options: {                                 // Target options 
		        removeComments: true,
		        collapseWhitespace: true
		      },
		      files: {                                   // Dictionary of files 
		        'front/dist/index.html': 'index.html',     // 'destination': 'source' 
		      }
		    },
	  	},

	  	/* SHELL */
		shell :{
			multiple : {
				command : [
					'copy index.html front\\dist\\newindex.html'
				].join('&&')
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-shell');
	grunt.loadNpmTasks('grunt-code-switch');

	//Default Taks
	grunt.registerTask('default', ['jshint','cssmin','concat','uglify']);

	grunt.registerTask('build', [
       'code_switch:start',
       'concat',
       'uglify',
       'htmlmin',
       //'shell',
       'code_switch:end'
   ]);
};