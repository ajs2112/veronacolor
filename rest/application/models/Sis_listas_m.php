<?php
class Sis_listas_m extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->model("sis_sync_m");
    }

    public function getByCampo($campo){        
        $query = $this->db
        ->select("
                hex(l.id) as id,
                l.campo,
                l.nombre,
                l.descrip, 
                hex(l.id_padre) as id_padre
            ")
        ->from("sistema_listas l")
        ->where('l.campo',$campo)
        ->order_by('nombre')
        ->get();
        
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
    }

    public function getById($id){        
        $query = $this->db
        ->select("l.*")
        ->from("sistema_listas l")
        ->where('id',$id)
        ->order_by('nombre')
        ->get();
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
    }

    public function getByPadre($idPadre){        
        $query = $this->db
        ->select("l.*")
        ->from("sistema_listas l")
        ->where('id_padre',$idPadre)
        ->order_by('nombre')
        ->get();
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
    }

    public function getCampos(){        
        $query = $this->db
        ->select("l.campo")
        ->from("sistema_listas l")
        ->group_by('campo')
        ->get();
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;         
    }    
        
    public function getList($id=NULL){       
        $query = $this->db->query("CALL sistema_listas_SEL()");
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;      
    }

    public function getOne($id=NULL){  
        $query = $this->db->query("CALL sistema_listas_ONE(?)", array('id'=>$id));
        if ($query->num_rows()===1){
            return $query->row_array();
        }
        return NULL;
    }

    public function insert($data){
        //ASIGNA NUEVO ID
        $query = $this->db->query("CALL getUUID()");
        mysqli_next_result($this->db->conn_id);
        $lastUUID= $query->row();
        $data["id"]=$lastUUID->id;
        $data["order_id"]=$lastUUID->order_id;
        $data["last_update"]=$lastUUID->order_id;

        //LLAMA AL PROCEDIMIENTO DE INSERCION
        $procedureName="sistema_listas_INS(?,?,?,?,?,?,?)";
        $procedure = $this->db->query("CALL ".$procedureName, $data);
        mysqli_next_result($this->db->conn_id);
        $result = $procedure->row();
        if ($result->result!=0){
            $savedSQL=$this->sis_sync_m->saveSQL($procedureName,$data);
            return $data["id"];
        }
        return NULL;  
        /*
        $procedure = $this->db->query("CALL sistema_listas_INS(?,?,?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
        */
    }

    public function update($data){
        $procedureName="sistema_listas_UPD(?,?,?,?,?,?,?)";
        $procedure = $this->db->query("CALL ". $procedureName, $data);
        mysqli_next_result($this->db->conn_id);
        $result = $procedure->row();
        if ($result->result!=0){
            $savedSQL=$this->sis_sync_m->saveSQL($procedureName,$data);
            return $data["id"];
        } else {
            return $result;
        }
        return NULL;  
        /*
        $procedure = $this->db->query("CALL sistema_listas_UPD(?,?,?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
        */
    }    

    public function delete($id){
        $procedure = $this->db->query("CALL sistema_listas_DEL(?)", array('id'=>$id));
        $result = $procedure->row();
        if ($result){
            return $result->resultado;
        }
        return NULL;
    }

}
?>