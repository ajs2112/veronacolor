<?php
class Sis_menu_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function getList(){        
        $query = $this->db->query("CALL sistema_menu_SEL(?,?)", array('_id_sistema'=>'ADM','_id'=>""));
        //$query = $this->db->query("CALL sistema_menu_SEL(?)", array('_id'=>""));
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
    }


    public function getListByUser($permisos){
        $query = $this->db->query("CALL sistema_menu_SEL(?,?)", array('_id_sistema'=>'ADM','_id'=>$permisos));
        //$query = $this->db->query("CALL sistema_menu_SEL(?)", array('_id'=>$permisos));
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
        /*
        ->get_compiled_select();
        return $query;
        */
    }    


    public function getOne($id=NULL){    
        $query = $this->db->query("CALL sistema_menu_ONE(?)", array('id'=>$id));
        if ($query->num_rows()===1){
            return $query->row_array();
        }
        return NULL;    
    }

    public function insert($data){
        //ASIGNA NUEVO ID
        $query = $this->db->query("CALL getUUID()");
        mysqli_next_result($this->db->conn_id);
        $lastUUID= $query->row();
        $data["id"]=$lastUUID->id;
        $data["order_id"]=$lastUUID->order_id;
        $data["last_update"]=$lastUUID->order_id;

        //LLAMA AL PROCEDIMIENTO DE INSERCION
        $procedureName="sistema_menu_INS(?,?,?,?,?,?,?,?,?,?,?,?)";
        $procedure = $this->db->query("CALL ".$procedureName, $data);
        mysqli_next_result($this->db->conn_id);
        $result = $procedure->row();
        if ($result->result!=0){
            $savedSQL=$this->sis_sync_m->saveSQL($procedureName,$data);
            return $data["id"];
        }
        return NULL;  
        /*
        $procedure = $this->db->query("CALL sistema_menu_INS(?,?,?,?,?,?,?,?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
        */
    }

    public function update($data){
        $procedureName="sistema_menu_UPD(?,?,?,?,?,?,?,?,?,?,?,?)";
        $procedure = $this->db->query("CALL ". $procedureName, $data);
        mysqli_next_result($this->db->conn_id);
        $result = $procedure->row();
        if ($result->result!=0){
            $savedSQL=$this->sis_sync_m->saveSQL($procedureName,$data);
            return $data["id"];
        } else {
            return $result;
        }
        return NULL;    
        /*
        $procedure = $this->db->query("CALL sistema_menu_UPD(?,?,?,?,?,?,?,?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
        */
    }    

    public function delete($id){
        $procedure = $this->db->query("CALL sistema_menu_DEL(?)", array('id'=>$id));
        $result = $procedure->row();
        if ($result){
            return $result->resultado;
        }
        return NULL;
    }
}
?>