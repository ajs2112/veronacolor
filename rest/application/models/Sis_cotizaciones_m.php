<?php
class Sis_cotizaciones_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function getList(){        
        $query = $this->db->query("CALL sistema_cotizaciones_SEL()");
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
    }


    public function getLast(){
        $query = $this->db->query("CALL sistema_cotizaciones_LAST()");
        if ($query->num_rows()>0){
            return $query->result_array();
        }
        return NULL;            
        /*
        ->get_compiled_select();
        return $query;
        */
    }    

    public function getOne($id=NULL){  
        $query = $this->db->query("CALL sistema_cotizaciones_ONE(?)", array('id'=>$id));
        if ($query->num_rows()===1){
            return $query->row_array();
        }
        return NULL;
    }

    public function insert($data){
        $procedure = $this->db->query("CALL sistema_cotizaciones_INS(?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
    }

    public function update($data){
        $procedure = $this->db->query("CALL sistema_cotizaciones_UPD(?,?,?)", $data);
        $result = $procedure->row();
        if ($result){
            return $result->id;
        }
        return NULL;    
    }    

    public function delete($id){
        $procedure = $this->db->query("CALL sistema_cotizaciones_DEL(?)", array('id'=>$id));
        $result = $procedure->row();
        if ($result){
            return $result->resultado;
        }
        return NULL;
    }
    /*
    */

}
?>