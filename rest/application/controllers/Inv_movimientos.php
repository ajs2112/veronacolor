<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inv_movimientos extends CI_Controller {
	protected $headers;

	public function __construct(){
		parent:: __construct();
		$this->load->helper('authjwt_helper');
		$this->load->model("inv_movimientos_m");
	}


	public function getList(){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$id_tipo=$this->input->post("id_tipo",true);
				$id_operacion=$this->input->post("id_operacion",true);
				$datos=array(
							"id_tipo"=>$id_tipo,
							"id_operacion"=>$id_operacion,
						);

				$data=$this->inv_movimientos_m->getList($datos);
				echo respuesta($auth_user,$data);
			}
		}
	}	

	public function setOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$id_tipo=$this->input->post("id_tipo",true);
				$id_operacion=$this->input->post("id_operacion",true);
				$lista=$this->input->post("lista",true);

				$elimino = $this->inv_movimientos_m->delete($id_producto);
				if ($elimino){
					for($i=0, $ni=count($lista); $i < $ni; $i++){			
						$data=array(
							"id_tipo"=>$id_tipo,
							"id_operacion"=>$id_operacion,
							"id_producto"=>$lista[$i]["id_producto"],
							"id_producto_padre"=>$lista[$i]["id_producto_padre"],
							"id_unidad"=>$lista[$i]["id_unidad"],
							"precio"=>$lista[$i]["precio"],
							"cantidad"=>$lista[$i]["cantidad"],
							"id_impuesto"=>$lista[$i]["id_impuesto"],
							"valor_impuesto"=>$lista[$i]["valor_impuesto"],
						);

						$lastId=$this->inv_movimientos_m->insert($data);		

					}

					if ($lastId){
						echo respuesta($auth_user,$lastId);
					}else{
						echo error_msg(500);
					}
				}
				/*
				*/
			} else {
				echo error_msg(400);
			}
		}
	}

	public function delOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$one=$this->inv_formulas_m->delete($id);
			echo respuesta($auth_user,$one);
		}
	}	

}
