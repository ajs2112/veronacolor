<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sis_empresas extends CI_Controller {
	protected $headers;

	public function __construct(){
		parent:: __construct();
		$this->load->helper('authjwt_helper');
		$this->load->model("sis_empresas_m");
	}


	public function getList(){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$menu=$this->sis_empresas_m->getList();
			echo respuesta($auth_user,$menu);
		}
	}	

	public function getListByUser(){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$fk= implode($this->input->post());			
				$menu=$this->sis_empresas_m->getListByUser($fk);
				echo respuesta($auth_user,$menu);
			} else {
				echo error_msg(400);
			}
		/*
		*/
		}
	}

	public function getOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($id){
				$data=$this->sis_empresas_m->getOne($id);
			} else {
				$data=array(
					"id"=>0,
					"order_id"=>0,
					"last_update"=>0,
					"nombre"=>"Nuevo",					
					"rif"=>"",
					"direccion"=>"",
					"telefono"=>"",
					"es_inactivo"=>"0",
				);
			}

			echo respuesta($auth_user,$data);
		}
	}


	public function setOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$id=$this->input->post("id",true);
				$data=array(
					'id'=>$this->input->post("id",true),	
					'order_id'=>$this->input->post("order_id",true),
					'last_update'=>$this->input->post("last_update",true),
					'nombre'=>$this->input->post("nombre",true),
					'rif'=>$this->input->post("rif",true),
					'direccion'=>$this->input->post("direccion",true),
					'telefono'=>$this->input->post("telefono",true),
					'es_inactivo'=>$this->input->post("es_inactivo",true),
				);

				if ($id!=0){
					$lastId=$this->sis_empresas_m->update($data);
				}else{
					$lastId=$this->sis_empresas_m->insert($data);		
				}


				if ($lastId){
					echo respuesta($auth_user,$lastId);
				}else{
					echo error_msg(500);
				}
			} else {
				echo error_msg(400);
			}
		}
	}

	public function delOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$one=$this->sis_empresas_m->delete($id);
			echo respuesta($auth_user,$one);
		}
	}	

}
