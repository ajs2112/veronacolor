<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sis_cotizaciones extends CI_Controller {
	protected $headers;

	public function __construct(){
		parent:: __construct();
		$this->load->helper('authjwt_helper');
		$this->load->model("sis_cotizaciones_m");
	}


	public function getList(){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$menu=$this->sis_cotizaciones_m->getList();
			echo respuesta($auth_user,$menu);
		}
	}	

	public function getLast(){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$menu=$this->sis_cotizaciones_m->getLast();
			echo respuesta($auth_user,$menu);
		}
	}

	public function getOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($id){
				$data=$this->sis_cotizaciones_m->getOne($id);
			} else {
				$data=array(
					"id"=>0,
					"fecha"=>date("c"),					
					"valor"=>0,
				);
			}

			echo respuesta($auth_user,$data);
		}
	}


	public function setOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$id=$this->input->post("id",true);
				$data=array(
					'id'=>$this->input->post("id",true),	
					'fecha'=>$this->input->post("fecha",true),
					'valor'=>$this->input->post("valor",true),
				);

				if ($id!=0){
					$lastId=$this->sis_cotizaciones_m->update($data);
				}else{
					$lastId=$this->sis_cotizaciones_m->insert($data);		
				}


				if ($lastId){
					echo respuesta($auth_user,$lastId);
				}else{
					echo error_msg(500);
				}
			} else {
				echo error_msg(400);
			}
		}
	}

	public function delOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$one=$this->sis_cotizaciones_m->delete($id);
			echo respuesta($auth_user,$one);
		}
	}	

}
