<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inv_productos extends CI_Controller {
	protected $headers;

	public function __construct(){
		parent:: __construct();
		$this->load->helper('authjwt_helper');
		$this->load->model("inv_productos_m");
	}


	public function getList($idEmpresa,$idCategoria){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$menu=$this->inv_productos_m->getList($idEmpresa,$idCategoria);
			echo respuesta($auth_user,$menu);
		}
	}	

	public function getOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($id){
				$data=$this->inv_productos_m->getOne($id);
			} else {
				$data=array(
					"id"=>0,
					"order_id"=>0,
					"last_update"=>0,
					"id_empresa"=>"",
					"id_producto"=>"",
					"id_tipo"=>"00000000000000000000000000000000",					
					"id_categoria"=>"00000000000000000000000000000000",
					"codigo"=>"",
					"nombre"=>"",
					"nombre_display"=>"",
					"id_unidad_compra"=>"00000000000000000000000000000000",
					"id_unidad_venta"=>"00000000000000000000000000000000",
					"cantidad_unidad_compra"=>0,
					"costo_pct_dcto"=>0,
					"costo_pct_adic"=>0,
					"dolar_costo_anterior"=>0,
					"dolar_costo_dolar"=>0,
					"dolar_pct_utilidad"=>0,
					"factura_costo_anterior"=>0,
					"factura_costo_bruto"=>0,
					"factura_pct_utilidad"=>0,
					"lista_costo_anterior"=>0,
					"lista_costo_bruto"=>0,
					"lista_pct_utilidad"=>0,
					"stock_minimo"=>0,
					"id_impuesto"=>"00000000000000000000000000000000",
					"es_inactivo"=>0,
					"es_editable"=>0,
				);
			}

			echo respuesta($auth_user,$data);
		}
	}


	public function setOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				$id=$this->input->post("id",true);
				$data=array(
					'id'=>$this->input->post("id",true),	
					'order_id'=>$this->input->post("order_id",true),
					'last_update'=>$this->input->post("last_update",true),
					'id_empresa'=>$this->input->post("id_empresa",true),
					'id_producto'=>$this->input->post("id_producto",true),
					'id_tipo'=>$this->input->post("id_tipo",true),
					'id_categoria'=>$this->input->post("id_categoria",true),
					//'codigo'=>$this->input->post("codigo",true),
					//'nombre'=>$this->input->post("nombre",true),
					'nombre_display'=>$this->input->post("nombre_display",true),
					//'id_unidad_compra'=>$this->input->post("id_unidad_compra",true),
					//'id_unidad_venta'=>$this->input->post("id_unidad_venta",true),
					//'cantidad_unidad_compra'=>$this->input->post("cantidad_unidad_compra",true),
					'costo_pct_dcto'=>$this->input->post("costo_pct_dcto",true),
					'costo_pct_adic'=>$this->input->post("costo_pct_adic",true),
					'dolar_costo_anterior'=>$this->input->post("dolar_costo_anterior",true),
					'dolar_costo_dolar'=>$this->input->post("dolar_costo_dolar",true),
					'dolar_pct_utilidad'=>$this->input->post("dolar_pct_utilidad",true),
					'factura_costo_anterior'=>$this->input->post("factura_costo_anterior",true),
					'factura_costo_bruto'=>$this->input->post("factura_costo_bruto",true),
					'factura_pct_utilidad'=>$this->input->post("factura_pct_utilidad",true),
					'lista_costo_anterior'=>$this->input->post("lista_costo_anterior",true),
					'lista_costo_bruto'=>$this->input->post("lista_costo_bruto",true),
					'lista_pct_utilidad'=>$this->input->post("lista_pct_utilidad",true),
					'stock_minimo'=>$this->input->post("stock_minimo",true),
					'id_impuesto'=>$this->input->post("id_impuesto",true),
					'es_inactivo'=>$this->input->post("es_inactivo",true),
					'es_editable'=>$this->input->post("es_editable",true),
				);

				if ($id!=0){
					$lastId=$this->inv_productos_m->update($data);
				}else{
					$lastId=$this->inv_productos_m->insert($data);		
				}


				if ($lastId){
					echo respuesta($auth_user,$lastId);
				}else{
					echo error_msg(500);
				}
			} else {
				echo error_msg(400);
			}
		}
	}

	public function delOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$one=$this->inv_productos_m->delete($id);
			echo respuesta($auth_user,$one);
		}
	}	

}
