<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vnt_operaciones extends CI_Controller {
	protected $headers;

	public function __construct(){
		parent:: __construct();
		$this->load->helper('authjwt_helper');
		$this->load->helper('fechas_helper');
		$this->load->model("vnt_operaciones_m");
		$this->load->model("inv_movimientos_m");
		$this->load->model("inv_existencias_m");
		$this->load->model("inv_almacenes_m");	
		$this->load->model("caj_movimientos_m");	
	}


	public function getList($idEmpresa,$idTipo){
		$this->load->helper('authjwt_helper');
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$menu=$this->vnt_operaciones_m->getList($idEmpresa,$idTipo);
			echo respuesta($auth_user,$menu);
		}
	}	

	public function getOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$idEmpresa=$this->input->post("id_empresa",true);
			$idTipo=$this->input->post("id_tipo",true);
			$id=$this->input->post("id",true);
			if ($id){
				$data=$this->vnt_operaciones_m->getOne($id);
			} else {
				$ultimo=$this->vnt_operaciones_m->getLast($idEmpresa, $idTipo);
				$lastnumero=$ultimo["lastnumero"]+1;
				$lastcontrol=str_pad($lastnumero, 8, '0', STR_PAD_LEFT);

				$data=array(
					"id"=>0,
					"order_id"=>0,
					"last_update"=>0,
					"id_empresa"=>$idEmpresa,
					"id_tipo"=>$idTipo,
					"numero"=>$lastnumero,
					"nro_control"=>$lastcontrol,
					"fecha"=>fechaLocal(date("")),
					"fecha_registro"=>fechaLocal(date("")),
					"id_doc_origen"=>'',
					"id_cliente"=>'',
					"id_cotizacion"=>'',
					"monto_bruto"=>0,
					"pct_descuento"=>0,
					"pct_adicional"=>0,
					"monto_neto"=>0,
					"monto_iva"=>0,
					"monto_total"=>0,
					"id_status"=>0,
					"id_estado"=>0,
					"id_usuario"=>0,
					"observacion"=>"",
					"id_vendedor"=>"",
					"id_vehiculo"=>"",
					"id_chofer"=>"",							
				);
			}

			echo respuesta($auth_user,$data);
		}
	}
	/*
	private function _actualizarExistencia($signo, $idAlmacen, $idProducto, $cantidad){
		//EXISTENCIA
		$obj=array(
			"id"=>"",
			"order_id"=>0,
			"last_update"=>0,
			"id_producto"=>$idProducto,
			"id_almacen"=>$idAlmacen,
			"cantidad"=>$cantidad,
		);					
		switch ($signo) {
			case '+':
				$inv=$this->inv_existencias_m->aumentar($obj);
				break;
			case '-':
				$inv=$this->inv_existencias_m->disminuir($obj);
				break;							
			default:
				# code...
				break;
		}
	}
	*/

	private function _actualizarExistencia($idAlmacen, $idProducto, $cantidad){
		//EXISTENCIA
		$obj=array(
			"id"=>"",
			"order_id"=>0,
			"last_update"=>0,
			"id_producto"=>$idProducto,
			"id_almacen"=>$idAlmacen,
			"cantidad"=>$cantidad,
		);					
		
		$inv=$this->inv_existencias_m->setOne($obj);
	}

	private function _reversarInventario($signo, $idAlmacen, $obj){
		$listaAnterior=	$this->inv_movimientos_m->getList($obj);
		for($i=0, $ni=count($listaAnterior); $i < $ni; $i++){			
			$exis=array(
				"id_producto"=>$listaAnterior[$i]["id_producto"],
				"id_almacen"=>$idAlmacen,
				"cantidad"=>$listaAnterior[$i]["cantidad"],
			);					
			switch ($signo) {
				case '-':
					$inv=$this->inv_existencias_m->disminuir($exis);
					break;
				case '+':
					$inv=$this->inv_existencias_m->aumentar($exis);
					break;							
				default:
					# code...
					break;
			}
		}
	}

	public function setOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				//LLENA VARIABLES
				$tipo=$this->input->post("tipo");
				$head=$this->input->post("head");
				$detail=$this->input->post("detail");
				$pago=$this->input->post("pago");
				//PROCESA HEADER
				$id=$head["id"];
				$data=array(
					'id'=>$head["id"],
					'order_id'=>$head["order_id"],
					'last_update'=>$head["last_update"],
					'id_empresa'=>$head["id_empresa"],
					'id_tipo'=>$head["id_tipo"],
					'numero'=>$head["numero"],
					'nro_control'=>$head["nro_control"],
					'fecha'=>fechaLocal($head["fecha"]),
					'fecha_registro'=>fechaLocal($head["fecha_registro"]),
					'id_doc_origen'=>$head["id_doc_origen"],
					'id_cliente'=>$head["id_cliente"],
					'id_cotizacion'=>$head["id_cotizacion"],
					'monto_bruto'=>$head["monto_bruto"],
					'pct_descuento'=>$head["pct_descuento"],
					'pct_adicional'=>$head["pct_adicional"],
					'monto_neto'=>$head["monto_neto"],
					'monto_iva'=>$head["monto_iva"],
					'monto_total'=>$head["monto_total"],
					'id_status'=>1,
					'id_estado'=>$head["id_estado"],
					'id_usuario'=>$head["id_usuario"],
					'observacion'=>$head["observacion"],
					'id_vendedor'=>$head["id_vendedor"],
					'id_vehiculo'=>$head["id_vehiculo"],
					'id_chofer'=>$head["id_chofer"],
				);

				$almacen=$this->inv_almacenes_m->getDefault($data["id_empresa"]);
				//REGISTRO NUEVO
				if ($id==0){

					$ultimo=$this->vnt_operaciones_m->getLast($data["id_empresa"], $data["id_tipo"]);
					$lastnumero=$ultimo["lastnumero"]+1;
					$lastcontrol=str_pad($lastnumero, 8, '0', STR_PAD_LEFT);

					$data["numero"]=$lastnumero;
					$data["nro_control"]=$lastcontrol;

					$lastId=$this->vnt_operaciones_m->insert($data);		

					//PROCESA DETALLE
					for($i=0, $ni=count($detail); $i < $ni; $i++){			
						$data=array(
							"id"=>"",
							"order_id"=>0,
							"last_update"=>0,
							"id_tipo"=>$head["id_tipo"],
							"id_operacion"=>$lastId,
							"id_producto"=>$detail[$i]["id_producto"],
							"id_producto_padre"=>"00000000000000000000000000000000",
							"id_unidad"=>$detail[$i]["id_unidad"],
							"precio"=>$detail[$i]["precio"],
							"cantidad"=>$detail[$i]["cantidad"],
							"id_impuesto"=>$detail[$i]["id_impuesto"],
							"valor_impuesto"=>$detail[$i]["valor_impuesto"],
						);

						$lastDetail=$this->inv_movimientos_m->insert($data);	
						if ($tipo["signo_inventario"]=="-"){
							$detail[$i]["cantidad"]=$detail[$i]["cantidad"]*-1;
						}
						$this->_actualizarExistencia($almacen["id"], $detail[$i]["id_producto"], $detail[$i]["cantidad"]);
						/*
						switch ($tipo["signo_inventario"]) {
							case '+':
								$this->_actualizarExistencia('+', $almacen["id"], $detail[$i]["id_producto"], $detail[$i]["cantidad"]);
								break;
							case '-':
								$this->_actualizarExistencia('-', $almacen["id"], $detail[$i]["id_producto"], $detail[$i]["cantidad"]);
								break;
							case 'M':
								$this->_actualizarExistencia('-', $almacen["id"], $detail[$i]["id_producto"], $detail[$i]["cantidad"]);						
								$this->_actualizarExistencia('+', $almacen["id"], $detail[$i]["id_producto"], $detail[$i]["cantidad"]);
								break;														
							default:
								# code...
								break;
						}
						*/

					}


					//PROCESA PAGO
					for($i=0, $ni=count($pago); $i < $ni; $i++){			
						$dataPago=array(
							"id"=>"",
							"order_id"=>0,
							"last_update"=>0,
							"fecha"=>fechaLocal($head["fecha"]),
							"id_tipo"=>$head["id_tipo"],
							"id_operacion"=>$lastId,
							"monto"=>$pago[$i]["monto"],
							"id_instrumento"=>$pago[$i]["id_instrumento"],
							"id_cuenta"=>$pago[$i]["id_cuenta"],
							"id_banco"=>$pago[$i]["id_banco"],
							"numero_operacion"=>$pago[$i]["numero_operacion"],
							"id_moneda"=>$pago[$i]["id_moneda"],
							"factor"=>$pago[$i]["factor"],
						);

						$lastPago=$this->caj_movimientos_m->insert($dataPago);	
					}

				}



				if ($lastId){
					echo respuesta($auth_user,$lastId);
				}else{
					echo error_msg(500);
				}
			} else {
				echo error_msg(400);
			}
		}
	}



	public function anularOne(){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			if ($this->input->post()){
				//LLENA VARIABLES
				$tipo=$this->input->post("tipo");
				$head=$this->input->post("head");
				$detail=$this->input->post("detail");

				//PROCESA HEADER
				$id=$head["id"];
				$data=array(
					'id'=>$head["id"],
					'order_id'=>$head["order_id"],
					'last_update'=>$head["last_update"],
					'id_empresa'=>$head["id_empresa"],
					'id_tipo'=>$head["id_tipo"],
					'numero'=>$head["numero"],
					'nro_control'=>$head["nro_control"],
					'fecha'=>fechaLocal($head["fecha"]),
					'fecha_registro'=>fechaLocal($head["fecha_registro"]),
					'id_doc_origen'=>$head["id_doc_origen"],
					'id_cliente'=>$head["id_cliente"],
					'id_cotizacion'=>$head["id_cotizacion"],
					'monto_bruto'=>$head["monto_bruto"],
					'pct_descuento'=>$head["pct_descuento"],
					'pct_adicional'=>$head["pct_adicional"],
					'monto_neto'=>$head["monto_neto"],
					'monto_iva'=>$head["monto_iva"],
					'monto_total'=>$head["monto_total"],
					'id_status'=>2,
					'id_estado'=>$head["id_estado"],
					'id_usuario'=>$head["id_usuario"],
					'observacion'=>$head["observacion"],
					'id_vendedor'=>$head["id_vendedor"],
					'id_vehiculo'=>$head["id_vehiculo"],
					'id_chofer'=>$head["id_chofer"],
				);

				$lastId=$this->vnt_operaciones_m->update($data);
				
				if ($id!=0){//OPERACION EXISTENTE

					//CONFIGURA REGISTRO 
					$obj=array(
						"id_tipo"=>$head["id_tipo"],
						"id_operacion"=>$head["id"],
					);
					$almacen=$this->inv_almacenes_m->getDefault($data["id_empresa"]);

					$listaAnterior=	$this->inv_movimientos_m->getList($obj);
					for($i=0, $ni=count($listaAnterior); $i < $ni; $i++){			
						if ($tipo["signo_inventario"]=="+"){
							$listaAnterior[$i]["cantidad"]=$listaAnterior[$i]["cantidad"]*-1;
						}
						$this->_actualizarExistencia($almacen["id"], $listaAnterior[$i]["id_producto"], $listaAnterior[$i]["cantidad"]);
					}

				}

				if ($lastId){
					echo respuesta($auth_user,$lastId);
				}else{
					echo error_msg(500);
				}
			} else {
				echo error_msg(400);
			}
		}
	}

	public function delOne($id){
		$auth_user=autoriza();
		if (!$auth_user){
			echo error_msg(401);
		} else {
			$one=$this->vnt_operaciones_m->delete($id);
			echo respuesta($auth_user,$one);
		}
	}	

}
