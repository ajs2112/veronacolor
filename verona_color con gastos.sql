/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : verona_color

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-09-25 23:25:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for caja_bancos
-- ----------------------------
DROP TABLE IF EXISTS `caja_bancos`;
CREATE TABLE `caja_bancos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `id_banco` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_tipo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `numero` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for caja_instrumentos
-- ----------------------------
DROP TABLE IF EXISTS `caja_instrumentos`;
CREATE TABLE `caja_instrumentos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `nombre` varchar(255) DEFAULT NULL,
  `es_inactivo` tinyint(1) DEFAULT '0',
  `id_tipo` tinyint(1) DEFAULT '0' COMMENT '1 efectivo, 2 cheque, 3 deposito, 4 punto venta',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for caja_movimientos
-- ----------------------------
DROP TABLE IF EXISTS `caja_movimientos`;
CREATE TABLE `caja_movimientos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `id_tipo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_operacion` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `monto` decimal(20,2) DEFAULT '0.00' COMMENT 'monto del pago',
  `id_instrumento` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_cuenta` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_banco` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `numero_operacion` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'numero de la operacion bancaria',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for compras_operaciones
-- ----------------------------
DROP TABLE IF EXISTS `compras_operaciones`;
CREATE TABLE `compras_operaciones` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_tipo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `numero` int(10) DEFAULT NULL,
  `nro_control` varchar(10) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `id_doc_origen` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_proveedor` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_destino` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `monto_bruto` decimal(10,2) DEFAULT '0.00' COMMENT 'monto bruto',
  `pct_descuento` decimal(10,2) DEFAULT '0.00' COMMENT 'porcentaje de descuento',
  `pct_adicional` decimal(10,2) DEFAULT '0.00' COMMENT 'porcentaje adicional',
  `monto_neto` decimal(10,2) DEFAULT '0.00' COMMENT 'monto neto',
  `monto_iva` decimal(10,2) DEFAULT '0.00' COMMENT 'monto iva',
  `monto_total` decimal(10,2) DEFAULT '0.00' COMMENT 'monto total',
  `id_status` int(11) DEFAULT '0' COMMENT 'identificador del status',
  `id_estado` int(11) DEFAULT '0' COMMENT 'identificador del estado',
  `id_usuario` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del usuario',
  `observacion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'observacion',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for compras_proveedores
-- ----------------------------
DROP TABLE IF EXISTS `compras_proveedores`;
CREATE TABLE `compras_proveedores` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nombre del proveedor',
  `rif` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'rif del proveedor',
  `direccion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'direccion del proveedor',
  `telefono` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'telefono del proveedor',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'correo electronico del proveedor',
  `representante` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'representante legal del proveedor',
  `id_banco` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `numero_cuenta` varchar(26) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nuemero de cuenta',
  `observacion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'datos adicionales',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for empresa_personal
-- ----------------------------
DROP TABLE IF EXISTS `empresa_personal`;
CREATE TABLE `empresa_personal` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `id_tipo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `cedula` varchar(30) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `nombre` varchar(30) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `pct_comision` double(3,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for empresa_vehiculos
-- ----------------------------
DROP TABLE IF EXISTS `empresa_vehiculos`;
CREATE TABLE `empresa_vehiculos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `vehiculo` varchar(30) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `modelo` varchar(30) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `marca` varchar(30) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',
  `color` varchar(30) DEFAULT NULL,
  `placa` varchar(30) DEFAULT NULL,
  `racda` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for gastos_beneficiarios
-- ----------------------------
DROP TABLE IF EXISTS `gastos_beneficiarios`;
CREATE TABLE `gastos_beneficiarios` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nombre del proveedor',
  `rif` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'rif del proveedor',
  `direccion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'direccion del proveedor',
  `telefono` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'telefono del proveedor',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'correo electronico del proveedor',
  `representante` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'representante legal del proveedor',
  `id_banco` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `numero_cuenta` varchar(26) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nuemero de cuenta',
  `observacion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'datos adicionales',
  `pct_retencion` double(6,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for gastos_plan_cuentas
-- ----------------------------
DROP TABLE IF EXISTS `gastos_plan_cuentas`;
CREATE TABLE `gastos_plan_cuentas` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nombre del proveedor',
  `codigo` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'rif del proveedor',
  `descripcion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'direccion del proveedor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for gastos_registro
-- ----------------------------
DROP TABLE IF EXISTS `gastos_registro`;
CREATE TABLE `gastos_registro` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_tipo_operacion` varchar(1) DEFAULT NULL,
  `numero` int(10) DEFAULT NULL,
  `nro_control` varchar(10) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `id_beneficiario` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_partida` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_cuenta` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `descripcion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'observacion',
  `id_tipo_documento` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `nro_control_documento` varchar(16) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0.00\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'monto total',
  `nro_documento` varchar(16) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '0.00\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'monto iva',
  `fecha_documento` datetime DEFAULT NULL,
  `nro_factura_afectada` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `monto_exento` decimal(16,2) DEFAULT '0.00' COMMENT 'monto bruto',
  `base_imponible` decimal(16,2) DEFAULT '0.00' COMMENT 'porcentaje de descuento',
  `pct_alicuota` decimal(6,2) DEFAULT '0.00' COMMENT 'porcentaje adicional',
  `es_retiva` tinyint(1) DEFAULT NULL,
  `es_retislr` tinyint(1) DEFAULT NULL,
  `id_status` int(11) DEFAULT '0' COMMENT 'identificador del status',
  `id_usuario` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del usuario',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for inventario_almacenes
-- ----------------------------
DROP TABLE IF EXISTS `inventario_almacenes`;
CREATE TABLE `inventario_almacenes` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `es_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_atributos
-- ----------------------------
DROP TABLE IF EXISTS `inventario_atributos`;
CREATE TABLE `inventario_atributos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_producto` binary(16) DEFAULT '0000000000000000',
  `nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',
  `valor` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_categorias
-- ----------------------------
DROP TABLE IF EXISTS `inventario_categorias`;
CREATE TABLE `inventario_categorias` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `id_padre` binary(16) DEFAULT '0000000000000000',
  `id_empresa` binary(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_existencias
-- ----------------------------
DROP TABLE IF EXISTS `inventario_existencias`;
CREATE TABLE `inventario_existencias` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_producto` binary(16) DEFAULT NULL,
  `id_almacen` binary(16) DEFAULT NULL,
  `cantidad` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_formulas
-- ----------------------------
DROP TABLE IF EXISTS `inventario_formulas`;
CREATE TABLE `inventario_formulas` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_producto` binary(16) DEFAULT '0000000000000000',
  `id_componente` binary(16) DEFAULT '0000000000000000',
  `id_unidad` binary(16) DEFAULT '0000000000000000',
  `cantidad` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_movimientos
-- ----------------------------
DROP TABLE IF EXISTS `inventario_movimientos`;
CREATE TABLE `inventario_movimientos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_tipo` binary(16) DEFAULT '0000000000000000',
  `id_operacion` binary(16) DEFAULT '0000000000000000',
  `id_producto` binary(16) DEFAULT '0000000000000000',
  `id_producto_padre` binary(16) DEFAULT '0000000000000000',
  `id_unidad` binary(16) DEFAULT '0000000000000000',
  `costo_dolar` double(10,2) DEFAULT '0.00',
  `costo_bs` double(10,2) DEFAULT '0.00',
  `precio` double(10,2) DEFAULT '0.00',
  `cantidad` double(10,2) DEFAULT '0.00',
  `monto` double(10,2) DEFAULT '0.00',
  `id_impuesto` binary(16) DEFAULT '0000000000000000',
  `valor_impuesto` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE,
  KEY `tipo_mov` (`id_tipo`),
  KEY `id_prod` (`id_producto`),
  KEY `id_und_mov` (`id_unidad`),
  CONSTRAINT `id_prod` FOREIGN KEY (`id_producto`) REFERENCES `inventario_productos` (`id`),
  CONSTRAINT `id_und_mov` FOREIGN KEY (`id_unidad`) REFERENCES `sistema_unidades` (`id`),
  CONSTRAINT `tipo_mov` FOREIGN KEY (`id_tipo`) REFERENCES `sistema_operaciones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_operaciones
-- ----------------------------
DROP TABLE IF EXISTS `inventario_operaciones`;
CREATE TABLE `inventario_operaciones` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_tipo` binary(16) DEFAULT '0000000000000000',
  `numero` int(10) DEFAULT NULL,
  `nro_control` varchar(10) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `id_doc_origen` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_origen` binary(16) DEFAULT '0000000000000000',
  `id_destino` binary(16) DEFAULT '0000000000000000',
  `id_usuario` binary(16) DEFAULT '0000000000000000',
  `id_status` tinyint(16) DEFAULT '0' COMMENT '1=procesado 2=anulado 3=pendiente',
  `id_estado` int(11) DEFAULT '0' COMMENT 'identificador del estado',
  `observacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE,
  KEY `tipo_oper` (`id_tipo`),
  KEY `origen` (`id_origen`),
  KEY `destino` (`id_destino`),
  KEY `usuario_oper` (`id_usuario`),
  KEY `status_oper` (`id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for inventario_productos
-- ----------------------------
DROP TABLE IF EXISTS `inventario_productos`;
CREATE TABLE `inventario_productos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT '0',
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_producto` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_tipo` binary(16) DEFAULT '0000000000000000',
  `id_categoria` binary(16) DEFAULT '0000000000000000',
  `codigo` varchar(30) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `nombre_display` varchar(200) DEFAULT NULL,
  `id_unidad_compra` binary(16) DEFAULT '0000000000000000',
  `id_unidad_venta` binary(16) DEFAULT '0000000000000000',
  `cantidad_unidad_compra` double(10,2) DEFAULT '0.00',
  `costo_pct_dcto` double(10,2) DEFAULT '0.00',
  `costo_pct_adic` double(10,2) DEFAULT '0.00',
  `dolar_costo_anterior` double(10,2) DEFAULT '0.00',
  `dolar_costo_dolar` double(10,2) DEFAULT '0.00',
  `dolar_pct_utilidad` double(10,8) DEFAULT '0.00000000',
  `factura_costo_anterior` double(10,2) DEFAULT '0.00',
  `factura_costo_bruto` double(10,2) DEFAULT '0.00',
  `factura_pct_utilidad` double(10,8) DEFAULT '0.00000000',
  `lista_costo_anterior` double(10,2) DEFAULT '0.00',
  `lista_costo_bruto` double(10,2) DEFAULT '0.00',
  `lista_pct_utilidad` double(10,8) DEFAULT '0.00000000',
  `id_impuesto` binary(16) DEFAULT '0000000000000000',
  `es_inactivo` tinyint(1) DEFAULT '0',
  `es_editable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE,
  KEY `tipo` (`id_tipo`),
  KEY `categoria` (`id_categoria`),
  KEY `und_compra` (`id_unidad_compra`),
  KEY `und_venta` (`id_unidad_venta`),
  KEY `impuesto` (`id_impuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for retenciones_islr_detail
-- ----------------------------
DROP TABLE IF EXISTS `retenciones_islr_detail`;
CREATE TABLE `retenciones_islr_detail` (
  `id` binary(16) NOT NULL COMMENT 'identificador del agente',
  `order_id` int(11) DEFAULT NULL,
  `id_head` binary(16) DEFAULT NULL,
  `id_gasto` binary(16) DEFAULT NULL,
  `id_concepto` binary(16) DEFAULT NULL,
  `monto_renglon` double(16,2) DEFAULT NULL,
  `pct_retencion` double(16,2) DEFAULT NULL,
  `sustraendo` double(16,2) DEFAULT NULL,
  `monto_retenido` double(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for retenciones_islr_head
-- ----------------------------
DROP TABLE IF EXISTS `retenciones_islr_head`;
CREATE TABLE `retenciones_islr_head` (
  `id` binary(16) NOT NULL COMMENT 'identificador del agente',
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `comprobante` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `anio` varchar(4) DEFAULT NULL,
  `mes` varchar(2) DEFAULT NULL,
  `fecha_emision` varchar(20) DEFAULT NULL,
  `fecha_registro` varchar(20) DEFAULT NULL,
  `id_beneficiario` binary(16) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for retenciones_iva_detail
-- ----------------------------
DROP TABLE IF EXISTS `retenciones_iva_detail`;
CREATE TABLE `retenciones_iva_detail` (
  `id` binary(16) NOT NULL COMMENT 'identificador del agente',
  `order_id` int(11) DEFAULT NULL,
  `id_head` binary(16) DEFAULT NULL,
  `id_gasto` binary(16) DEFAULT NULL,
  `pct_retencion` double(16,2) DEFAULT NULL,
  `monto_retenido` double(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for retenciones_iva_head
-- ----------------------------
DROP TABLE IF EXISTS `retenciones_iva_head`;
CREATE TABLE `retenciones_iva_head` (
  `id` binary(16) NOT NULL COMMENT 'identificador del agente',
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `comprobante` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `anio` varchar(4) DEFAULT NULL,
  `mes` varchar(2) DEFAULT NULL,
  `fecha_emision` varchar(20) DEFAULT NULL,
  `fecha_registro` varchar(20) DEFAULT NULL,
  `id_beneficiario` binary(16) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_cotizaciones
-- ----------------------------
DROP TABLE IF EXISTS `sistema_cotizaciones`;
CREATE TABLE `sistema_cotizaciones` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `valor` double DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_empresas
-- ----------------------------
DROP TABLE IF EXISTS `sistema_empresas`;
CREATE TABLE `sistema_empresas` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT '0',
  `nombre` varchar(100) DEFAULT NULL,
  `rif` varchar(20) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(24) DEFAULT NULL,
  `es_inactivo` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_impuestos
-- ----------------------------
DROP TABLE IF EXISTS `sistema_impuestos`;
CREATE TABLE `sistema_impuestos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_tipo` binary(16) DEFAULT '0000000000000000',
  `codigo` varchar(10) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `valor_display` varchar(100) DEFAULT NULL,
  `valor` double(6,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_listas
-- ----------------------------
DROP TABLE IF EXISTS `sistema_listas`;
CREATE TABLE `sistema_listas` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `campo` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `id_padre` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_menu
-- ----------------------------
DROP TABLE IF EXISTS `sistema_menu`;
CREATE TABLE `sistema_menu` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_sistema` varchar(10) DEFAULT NULL,
  `id_seccion` tinyint(1) DEFAULT NULL,
  `id_padre` binary(16) NOT NULL DEFAULT '0000000000000000',
  `id_tipo` tinyint(1) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `trigger` varchar(100) DEFAULT NULL,
  `orden_pos` tinyint(1) DEFAULT NULL,
  `icono` varchar(100) DEFAULT NULL,
  `tooltip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_operaciones
-- ----------------------------
DROP TABLE IF EXISTS `sistema_operaciones`;
CREATE TABLE `sistema_operaciones` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_tipo` binary(16) DEFAULT '0000000000000000',
  `nombre` varchar(100) DEFAULT NULL,
  `nombre_display` varchar(100) DEFAULT NULL,
  `descrip` varchar(255) DEFAULT NULL,
  `signo_inventario` varchar(1) DEFAULT NULL,
  `signo_caja` varchar(1) DEFAULT NULL,
  `es_fiscal` tinyint(1) DEFAULT '0',
  `es_autorizado` tinyint(1) DEFAULT '0',
  `es_visible` tinyint(1) DEFAULT '0',
  `es_derivado` tinyint(1) DEFAULT '0',
  `es_transporte` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_productos
-- ----------------------------
DROP TABLE IF EXISTS `sistema_productos`;
CREATE TABLE `sistema_productos` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT '0',
  `codigo` varchar(30) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `empresas` varchar(255) DEFAULT NULL,
  `id_unidad_compra` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_unidad_venta` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `cantidad_unidad_compra` double(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for sistema_unidades
-- ----------------------------
DROP TABLE IF EXISTS `sistema_unidades`;
CREATE TABLE `sistema_unidades` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_tipo` binary(16) DEFAULT '0000000000000000' COMMENT 'sistema unidad en listas',
  `nombre` varchar(100) DEFAULT NULL,
  `nombre_display` varchar(100) DEFAULT NULL,
  `factor_conversion` double(10,6) DEFAULT NULL,
  `es_patron` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `timestamp` (`order_id`) USING BTREE,
  KEY `id_tipo` (`id_tipo`),
  CONSTRAINT `id_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `sistema_listas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for sistema_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `sistema_usuarios`;
CREATE TABLE `sistema_usuarios` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_tipo` tinyint(1) DEFAULT '0',
  `nombre` varchar(255) DEFAULT NULL,
  `cedula` varchar(20) DEFAULT NULL,
  `telefono` varchar(24) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `logname` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `permisos` text,
  `empresas` text,
  `event` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `timestamp` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ventas_clientes
-- ----------------------------
DROP TABLE IF EXISTS `ventas_clientes`;
CREATE TABLE `ventas_clientes` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nombre del proveedor',
  `rif` varchar(20) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'rif del proveedor',
  `direccion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'direccion del proveedor',
  `telefono` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'telefono del proveedor',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'correo electronico del proveedor',
  `representante` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'representante legal del proveedor',
  `limite_credito` double(10,2) DEFAULT '0.00' COMMENT 'identificador del banco en listas',
  `dias_credito` tinyint(3) DEFAULT NULL,
  `observacion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'datos adicionales',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for ventas_operaciones
-- ----------------------------
DROP TABLE IF EXISTS `ventas_operaciones`;
CREATE TABLE `ventas_operaciones` (
  `id` binary(16) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `id_empresa` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del banco en listas',
  `id_tipo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `numero` int(10) DEFAULT NULL,
  `nro_control` varchar(10) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `id_doc_origen` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_cliente` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `monto_bruto` decimal(10,2) DEFAULT '0.00' COMMENT 'monto bruto',
  `pct_descuento` decimal(10,2) DEFAULT '0.00' COMMENT 'porcentaje de descuento',
  `pct_adicional` decimal(10,2) DEFAULT '0.00' COMMENT 'porcentaje adicional',
  `monto_neto` decimal(10,2) DEFAULT '0.00' COMMENT 'monto neto',
  `monto_iva` decimal(10,2) DEFAULT '0.00' COMMENT 'monto iva',
  `monto_total` decimal(10,2) DEFAULT '0.00' COMMENT 'monto total',
  `id_status` int(11) DEFAULT '0' COMMENT 'identificador del status',
  `id_estado` int(11) DEFAULT '0' COMMENT 'identificador del estado',
  `id_usuario` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0' COMMENT 'identificador del usuario',
  `observacion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'observacion',
  `id_vendedor` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_vehiculo` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `id_chofer` binary(16) DEFAULT '0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Procedure structure for bancos_cuentas_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `bancos_cuentas_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bancos_cuentas_INS`(IN `_id` VARCHAR(36), IN `_id_banco` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` VARCHAR(20))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO bancos_cuentas(id, order_id, id_banco, id_tipo, numero) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_banco), UNHEX(_id_tipo), _numero);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for bancos_cuentas_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `bancos_cuentas_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bancos_cuentas_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_banco) as id_banco, 
	b.nombre as banco_nombre, 
	HEX(c.id_tipo) as id_tipo, 
	t.nombre as tipo_nombre, 
	c.numero
FROM bancos_cuentas as c  
LEFT JOIN sistema_listas as b ON c.id_banco = b.id
LEFT JOIN sistema_listas as t ON c.id_tipo = t.id
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for bancos_cuentas_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `bancos_cuentas_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bancos_cuentas_SEL`()
    NO SQL
BEGIN
	SELECT hex(c.id) as id,
							c.order_id,
							hex(c.id_banco) as id_banco,
							b.nombre as banco_nombre,
							hex(c.id_tipo) as id_tipo,
							t.nombre as tipo_nombre,
							c.numero
	FROM bancos_cuentas as c
	LEFT JOIN sistema_listas as b ON c.id_banco = b.id
	LEFT JOIN sistema_listas as t ON c.id_tipo = t.id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for bancos_cuentas_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `bancos_cuentas_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `bancos_cuentas_UPD`(IN `_id` VARCHAR(36), IN `_id_banco` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` VARCHAR(20))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE bancos_cuentas as a
SET a.id_banco=UNHEX(_id_banco), a.id_tipo=UNHEX(_id_tipo), a.numero=_numero
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_bancos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_bancos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_bancos_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_banco` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` VARCHAR(20))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO caja_bancos(id, order_id, id_empresa, id_banco, id_tipo, numero) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_banco), UNHEX(_id_tipo), _numero);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_bancos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_bancos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_bancos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_empresa) as id_empresa, 
	HEX(c.id_banco) as id_banco, 
	b.nombre as banco_nombre, 
	HEX(c.id_tipo) as id_tipo, 
	t.nombre as tipo_nombre, 
	c.numero
FROM caja_bancos as c  
LEFT JOIN sistema_listas as b ON c.id_banco = b.id
LEFT JOIN sistema_listas as t ON c.id_tipo = t.id
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_bancos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_bancos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_bancos_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
	SELECT hex(c.id) as id,
							c.order_id,
							hex(c.id_banco) as id_banco,
							b.nombre as banco_nombre,
							hex(c.id_tipo) as id_tipo,
							t.nombre as tipo_nombre,
							c.numero
	FROM caja_bancos as c
	LEFT JOIN sistema_listas as b ON c.id_banco = b.id
	LEFT JOIN sistema_listas as t ON c.id_tipo = t.id
	WHERE c.id_empresa = UNHEX(_id_empresa);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_bancos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_bancos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_bancos_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_banco` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` VARCHAR(20))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE caja_bancos as a
SET a.id_empresa=UNHEX(_id_empresa), a.id_banco=UNHEX(_id_banco), a.id_tipo=UNHEX(_id_tipo), a.numero=_numero
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_instrumentos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_instrumentos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_instrumentos_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(255), IN `_es_inactivo` TINYINT(1),  IN `_id_tipo` TINYINT(1))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO caja_instrumentos(id, order_id, id_empresa, nombre, es_inactivo, id_tipo) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _nombre, _es_inactivo, _id_tipo);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_instrumentos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_instrumentos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_instrumentos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_empresa) as id_empresa,
	c.nombre,
	c.es_inactivo,
	c.id_tipo
FROM caja_instrumentos as c  
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_instrumentos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_instrumentos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_instrumentos_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
	SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_empresa) as id_empresa,
	c.nombre,
	c.es_inactivo,
	c.id_tipo
FROM caja_instrumentos as c
WHERE c.id_empresa = UNHEX(_id_empresa);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_instrumentos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_instrumentos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_instrumentos_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(255), IN `_es_inactivo` TINYINT(1),  IN `_id_tipo` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE caja_instrumentos as a
SET a.id_empresa=UNHEX(_id_empresa), a.nombre=_nombre, a.es_inactivo=_es_inactivo, a.id_tipo=_id_tipo
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_movimientos_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_movimientos_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_movimientos_DEL`(IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36))
    NO SQL
BEGIN
DELETE m.* 
	FROM caja_movimientos as m 
	WHERE m.id_tipo= UNHEX(_id_tipo) AND m.id_operacion= UNHEX(_id_operacion);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_movimientos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_movimientos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_movimientos_INS`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36), IN `_monto` DOUBLE(10,2), IN `_id_instrumento` VARCHAR(36), IN `_id_cuenta` VARCHAR(36), IN `_id_banco` VARCHAR(36), IN `_numero_operacion` VARCHAR(20))
BEGIN
    SET @my_uuid= ordered_uuid(UUID());
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO caja_movimientos(id, order_id, id_tipo, id_operacion, monto, id_instrumento, id_cuenta, id_banco, numero_operacion) 
		VALUES (@my_uuid, @order_id, UNHEX(_id_tipo), UNHEX(_id_operacion), _monto, UNHEX(_id_instrumento), UNHEX(_id_cuenta), UNHEX(_id_banco), _numero_operacion);

		SELECT HEX(@my_uuid) as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for caja_movimientos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `caja_movimientos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `caja_movimientos_SEL`(IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(m.id) as id, 
			m.order_id, 
			HEX(m.id_tipo) as id_tipo,
			HEX(m.id_operacion) as id_operacion,
			HEX(m.id_instrumento) as id_instrumento, 
			HEX(m.id_cuenta) as id_cuenta, 
			HEX(m.id_banco) as id_banco,
			m.numero_operacion,
			m.monto,
			i.nombre as instrumento_nombre,
			b.nombre as banco_nombre,
			c.numero as cuenta_numero,
			bc.nombre as cuenta_banco_nombre
FROM caja_movimientos as m
LEFT JOIN caja_instrumentos as i ON m.id_instrumento=i.id
LEFT JOIN sistema_listas as b ON m.id_banco=b.id
LEFT JOIN caja_bancos as c ON m.id_cuenta=c.id
LEFT JOIN sistema_listas as bc ON c.id_banco=bc.id
WHERE m.id_tipo = UNHEX(_id_tipo) AND m.id_operacion = UNHEX(_id_operacion);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_operaciones_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_operaciones_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_operaciones_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_doc_origen` VARCHAR(36), IN `_id_proveedor` VARCHAR(36), IN `_id_destino` VARCHAR(36), IN `_monto_bruto` DOUBLE(10,2), IN `_pct_descuento` DOUBLE(10,2), IN `_pct_adicional` DOUBLE(10,2), IN `_monto_neto` DOUBLE(10,2), IN `_monto_iva` DOUBLE(10,2), IN `_monto_total` DOUBLE(10,2), IN `_id_status` VARCHAR(36), IN `_id_estado` VARCHAR(36),IN `_id_usuario` VARCHAR(36), IN `_observacion` VARCHAR(255))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();

		INSERT INTO compras_operaciones(id, order_id, id_empresa, id_tipo, numero, nro_control, fecha, fecha_registro, id_doc_origen, id_proveedor, id_destino, monto_bruto, pct_descuento, pct_adicional, monto_neto, monto_iva, monto_total, id_status, id_estado, id_usuario, observacion) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_tipo), _numero, _nro_control, _fecha, _fecha_registro, UNHEX(_id_doc_origen), UNHEX(_id_proveedor), UNHEX(_id_destino),_monto_bruto, _pct_descuento, _pct_adicional, _monto_neto, _monto_iva, _monto_total, _id_status, _id_estado, UNHEX(_id_usuario), _observacion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_operaciones_LAST
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_operaciones_LAST`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_operaciones_LAST`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
        SELECT 	MAX(o.numero) as lastnumero
								FROM compras_operaciones as o
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_operaciones_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_operaciones_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_operaciones_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(o.id) as id, 
	o.order_id, 
	o.numero, 
	o.nro_control, 
	o.fecha, 
	o.fecha_registro, 
	HEX(o.id_empresa) as id_empresa,
	HEX(o.id_tipo) as id_tipo,
	so.nombre as tipo_operacion_nombre,
	HEX(o.id_doc_origen) as id_doc_origen, 
	HEX(o.id_proveedor) as id_proveedor, 
	p.nombre as proveedor_nombre,
	p.rif as proveedor_rif,
	HEX(o.id_destino) as id_destino,
	a.nombre as almacen_nombre,
	o.monto_bruto,
	o.pct_descuento,
	o.pct_adicional,
	o.monto_neto,
	o.monto_iva,
	o.monto_total,
	o.id_status,
	o.id_estado,
	HEX(o.id_usuario) as id_usuario, 
	u.nombre as usuario_nombre,
	o.observacion
FROM compras_operaciones as o
INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
LEFT JOIN compras_proveedores as p ON o.id_proveedor=p.id
LEFT JOIN inventario_almacenes as a ON o.id_destino=a.id
LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
WHERE o.id = UNHEX(_id) LIMIT 1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_operaciones_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_operaciones_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_operaciones_SEL`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
	SELECT hex(o.id) as id,
                    o.order_id,
										hex(o.id_empresa) as id_empresa,
										hex(o.id_tipo) as id_tipo,
										so.nombre as tipo_operacion_nombre,
										o.numero,
										o.nro_control,
										DATE_FORMAT(o.fecha,'%Y-%m-%dT%TZ') as fecha,
										o.fecha_registro,
										hex(o.id_doc_origen) as id_doc_origen,
										hex(o.id_proveedor) as id_proveedor,
										p.nombre as proveedor_nombre,
										p.rif as proveedor_rif,
										hex(o.id_destino) as id_destino,
										a.nombre as almacen_nombre,
										o.monto_bruto,
										o.pct_descuento,
										o.pct_adicional,
										o.monto_neto,
										o.monto_iva,
										o.monto_total,
										o.id_status,
										o.id_estado,
										hex(o.id_usuario) as id_usuario,
										u.nombre as usuario_nombre,
										o.observacion
        FROM compras_operaciones as o
				INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
				LEFT JOIN compras_proveedores as p ON o.id_proveedor=p.id
				LEFT JOIN inventario_almacenes as a ON o.id_destino=a.id
				LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo)
				ORDER BY o.order_id DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_operaciones_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_operaciones_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_operaciones_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_doc_origen` VARCHAR(36), IN `_id_proveedor` VARCHAR(36), IN `_id_destino` VARCHAR(36), IN `_monto_bruto` DOUBLE(10,2), IN `_pct_descuento` DOUBLE(10,2), IN `_pct_adicional` DOUBLE(10,2), IN `_monto_neto` DOUBLE(10,2), IN `_monto_iva` DOUBLE(10,2), IN `_monto_total` DOUBLE(10,2), IN `_id_status` VARCHAR(36), IN `_id_estado` VARCHAR(36),IN `_id_usuario` VARCHAR(36), IN `_observacion` VARCHAR(255))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE compras_operaciones as o
SET o.id_empresa= UNHEX(_id_empresa),
o.id_tipo= UNHEX(_id_tipo), 
o.numero= _numero, 
o.nro_control= _nro_control, 
o.fecha= _fecha, 
o.fecha_registro= _fecha_registro,
o.id_doc_origen= UNHEX(_id_doc_origen),
o.id_proveedor= UNHEX(_id_proveedor),
o.id_destino= UNHEX(_id_destino),
o.monto_bruto=_monto_bruto,
o.pct_descuento=_pct_descuento,
o.pct_adicional=_pct_adicional,
o.monto_neto=_monto_neto,
o.monto_iva=_monto_iva,
o.monto_total=_monto_total, 
o.id_usuario= UNHEX(_id_usuario), 
o.id_status= _id_status, 
o.observacion= _observacion
WHERE o.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_proveedores_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_proveedores_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_proveedores_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_id_banco` VARCHAR(36), IN `_numero_cuenta` VARCHAR(26), IN `_observacion` VARCHAR(200))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO compras_proveedores(id, order_id, id_empresa, nombre, rif, direccion, telefono, email, representante, id_banco, numero_cuenta, observacion) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _nombre, _rif, _direccion, _telefono, _email, _representante, UNHEX(_id_banco), _numero_cuenta, _observacion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_proveedores_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_proveedores_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_proveedores_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(p.id) as id, p.order_id, HEX(p.id_empresa) as id_empresa, p.nombre, p.rif, p.direccion, p.telefono, p.email, p.representante, HEX(p.id_banco) as id_banco, b.nombre as banco_nombre, p.numero_cuenta, p.observacion
FROM compras_proveedores as p
LEFT JOIN sistema_listas as b ON p.id_banco=b.id
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_proveedores_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_proveedores_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_proveedores_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
        SELECT hex(p.id) as id,
                    p.order_id,
										HEX(p.id_empresa) as id_empresa,
										p.nombre,
										p.rif,
										p.direccion,
										p.telefono,
										p.email,
										p.representante,
										HEX(p.id_banco) as id_banco,
										b.nombre as banco_nombre,
										p.numero_cuenta,
										p.observacion
        FROM compras_proveedores as p
				LEFT JOIN sistema_listas as b ON p.id_banco=b.id
				WHERE p.id_empresa = UNHEX(_id_empresa);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for compras_proveedores_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `compras_proveedores_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `compras_proveedores_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_id_banco` VARCHAR(36), IN `_numero_cuenta` VARCHAR(26), IN `_observacion` VARCHAR(200))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE compras_proveedores as p
SET p.id_empresa=UNHEX(_id_empresa), p.nombre=_nombre, p.rif=_rif, p.direccion=_direccion, p.telefono=_telefono, p.email=_email, p.representante=_representante, p.id_banco=UNHEX(_id_banco), p.numero_cuenta=_numero_cuenta, p.observacion=_observacion
WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_empleados_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_empleados_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_empleados_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_cedula` VARCHAR(20), IN `_nombre` VARCHAR(100), IN `_pct_comision` DOUBLE(3,2))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO empresa_personal(id, order_id, id_empresa, id_tipo, cedula, nombre, pct_comision) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_tipo), _cedula, _nombre, _pct_comision);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_empleados_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_empleados_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_empleados_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_empresa) as id_empresa, 
	HEX(c.id_tipo) as id_tipo, 
	t.nombre as tipo_nombre, 
	c.cedula,
	c.nombre,
	c.pct_comision
FROM empresa_personal as c  
LEFT JOIN sistema_listas as t ON c.id_tipo = t.id
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_empleados_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_empleados_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_empleados_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
	SELECT hex(c.id) as id,
							c.order_id,
							hex(c.id_tipo) as id_tipo,
							t.nombre as tipo_nombre,
							c.cedula,
							c.nombre,
							c.pct_comision
	FROM empresa_personal as c
	LEFT JOIN sistema_listas as t ON c.id_tipo = t.id
	WHERE c.id_empresa = UNHEX(_id_empresa);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_empleados_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_empleados_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_empleados_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_cedula` VARCHAR(20), IN `_nombre` VARCHAR(100), IN `_pct_comision` DOUBLE(3,2))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE empresa_personal as a
SET a.id_empresa=UNHEX(_id_empresa), 
	a.id_tipo=UNHEX(_id_tipo), 
	a.cedula=_cedula,
	a.nombre=_nombre,
	a.pct_comision= _pct_comision
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_vehiculos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_vehiculos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_vehiculos_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_vehiculo` VARCHAR(30), IN `_modelo` VARCHAR(30), IN `_marca` VARCHAR(30), IN `_color` VARCHAR(30), IN `_placa` VARCHAR(30), IN `_racda` VARCHAR(30))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO empresa_vehiculos(id, order_id, id_empresa, vehiculo, modelo, marca, color, placa, racda) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _vehiculo, _modelo, _marca, _color, _placa, _racda);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_vehiculos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_vehiculos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_vehiculos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(c.id) as id, 
	c.order_id, 
	HEX(c.id_empresa) as id_empresa, 
	c.vehiculo,
	c.marca,
	c.modelo,
	c.color,
	c.placa,
	c.racda
FROM empresa_vehiculos as c  
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_vehiculos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_vehiculos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_vehiculos_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
	SELECT hex(c.id) as id,
							c.order_id, 
							HEX(c.id_empresa) as id_empresa, 
							c.vehiculo,
							c.marca,
							c.modelo,
							c.color,
							c.placa,
							c.racda
	FROM empresa_vehiculos as c
	WHERE c.id_empresa = UNHEX(_id_empresa);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for empresa_vehiculos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `empresa_vehiculos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `empresa_vehiculos_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_vehiculo` VARCHAR(30), IN `_modelo` VARCHAR(30), IN `_marca` VARCHAR(30), IN `_color` VARCHAR(30), IN `_placa` VARCHAR(30), IN `_racda` VARCHAR(30))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE empresa_vehiculos as a
SET a.id_empresa=UNHEX(_id_empresa), 
	a.vehiculo=_vehiculo,	
	a.modelo=_modelo,
	a.marca=_marca,
	a.color=_color,
	a.placa=_placa,
	a.racda=_racda
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_beneficiarios_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_beneficiarios_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_beneficiarios_INS`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_id_banco` VARCHAR(36), IN `_numero_cuenta` VARCHAR(26), IN `_observacion` VARCHAR(200), IN `_pct_retencion` DOUBLE(6,2))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO gastos_beneficiarios(id, order_id, nombre, rif, direccion, telefono, email, representante, id_banco, numero_cuenta, observacion, pct_retencion) 
		VALUES (UNHEX(@my_uuid), @order_id, _nombre, _rif, _direccion, _telefono, _email, _representante, UNHEX(_id_banco), _numero_cuenta, _observacion, _pct_retencion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_beneficiarios_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_beneficiarios_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_beneficiarios_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(p.id) as id, p.order_id, p.nombre, p.rif, p.direccion, p.telefono, p.email, p.representante, HEX(p.id_banco) as id_banco, b.nombre as banco_nombre, p.numero_cuenta, p.observacion, p.pct_retencion
FROM gastos_beneficiarios as p
LEFT JOIN sistema_listas as b ON p.id_banco=b.id
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_beneficiarios_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_beneficiarios_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_beneficiarios_SEL`()
    NO SQL
BEGIN
        SELECT hex(p.id) as id,
                    p.order_id,
										p.nombre,
										p.rif,
										p.direccion,
										p.telefono,
										p.email,
										p.representante,
										HEX(p.id_banco) as id_banco,
										b.nombre as banco_nombre,
										p.numero_cuenta,
										p.observacion,
										p.pct_retencion
        FROM gastos_beneficiarios as p
				LEFT JOIN sistema_listas as b ON p.id_banco=b.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_beneficiarios_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_beneficiarios_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_beneficiarios_UPD`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_id_banco` VARCHAR(36), IN `_numero_cuenta` VARCHAR(26), IN `_observacion` VARCHAR(200), IN `_pct_retencion` DOUBLE(6,2))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE gastos_beneficiarios as p
SET p.nombre=_nombre, p.rif=_rif, p.direccion=_direccion, p.telefono=_telefono, p.email=_email, p.representante=_representante, p.id_banco=UNHEX(_id_banco), p.numero_cuenta=_numero_cuenta, p.observacion=_observacion, p.pct_retencion=_pct_retencion
WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_plancuentas_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_plancuentas_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_plancuentas_INS`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_codigo` VARCHAR(20), IN `_descripcion` VARCHAR(200))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO gastos_plan_cuentas(id, order_id, nombre, codigo, descripcion) 
		VALUES (UNHEX(@my_uuid), @order_id, _nombre, _codigo, _descripcion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_plancuentas_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_plancuentas_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_plancuentas_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(p.id) as id, p.order_id, p.nombre, p.codigo, p.descripcion
FROM gastos_plan_cuentas as p
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_plancuentas_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_plancuentas_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_plancuentas_SEL`()
    NO SQL
BEGIN
        SELECT hex(p.id) as id,
                    p.order_id,
										p.nombre,
										p.codigo,
										p.descripcion
        FROM gastos_plan_cuentas as p;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_plancuentas_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_plancuentas_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_plancuentas_UPD`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_codigo` VARCHAR(20), IN `_descripcion` VARCHAR(200))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE gastos_plan_cuentas as p
SET p.nombre=_nombre, p.codigo=_codigo, p.descripcion=_descripcion
WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_registro_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_registro_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_registro_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo_operacion` VARCHAR(1), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_beneficiario` VARCHAR(36), IN `_id_partida` VARCHAR(36), IN `_id_cuenta` VARCHAR(36), IN `_descripcion` VARCHAR(200), IN `_id_tipo_documento` VARCHAR(36), IN `_nro_control_documento` VARCHAR(20), IN `_nro_documento` VARCHAR(20), IN `_fecha_documento` DATETIME, IN `_nro_factura_afectada` VARCHAR(20),  IN `_monto_exento` DOUBLE(16,2), IN `_base_imponible` DOUBLE(16,2), IN `_pct_alicuota` DOUBLE(6,2), IN `_es_retiva` TINYINT(1), IN `_es_retislr` TINYINT(1),  IN `_id_status` VARCHAR(36), IN `_id_usuario` VARCHAR(36))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO gastos_registro(id, order_id, id_empresa, id_tipo_operacion, numero, nro_control, fecha, fecha_registro, id_beneficiario, id_partida, id_cuenta, descripcion, id_tipo_documento, nro_control_documento, nro_documento, fecha_documento, nro_factura_afectada, monto_exento, base_imponible, pct_alicuota, es_retiva, es_retislr, id_status, id_usuario) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _id_tipo_operacion, _numero, _nro_control, _fecha, _fecha_registro, UNHEX(_id_beneficiario), UNHEX(_id_partida), UNHEX(_id_cuenta), _descripcion, UNHEX(_id_tipo_documento), _nro_control_documento, _nro_documento, _fecha_documento, _nro_factura_afectada, _monto_exento, _base_imponible, _pct_alicuota, _es_retiva, _es_retislr, _id_status, UNHEX(_id_usuario));

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_registro_LAST
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_registro_LAST`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_registro_LAST`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
        SELECT 	MAX(o.numero) as lastnumero
								FROM gastos_registro as o
				WHERE o.id_empresa=UNHEX(_id_empresa);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_registro_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_registro_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_registro_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(p.id) as id, 
	p.order_id, 
	HEX(p.id_empresa) as id_empresa,
	p.id_tipo_operacion, 
	p.numero, 
	p.nro_control, 
	p.fecha, 
	p.fecha_registro, 
	HEX(p.id_beneficiario) as id_beneficiario, 
	HEX(p.id_partida) as id_partida, 
	HEX(p.id_cuenta) as id_cuenta, 
	p.descripcion, 
	HEX(p.id_tipo_documento) as id_tipo_documento, 
	p.nro_control_documento, 
	p.nro_documento, 
	p.fecha_documento, 
	p.nro_factura_afectada,
	p.monto_exento, 
	p.base_imponible, 
	p.pct_alicuota, 
	p.es_retiva,
	p.es_retislr,
	p.id_status, 
	HEX(p.id_usuario) as id_usuario
FROM gastos_registro as p
INNER JOIN gastos_beneficiarios as b ON p.id_beneficiario=b.id
INNER JOIN gastos_plan_cuentas as x ON p.id_partida=x.id
INNER JOIN caja_bancos as c ON p.id_cuenta=c.id
INNER JOIN sistema_listas as l ON p.id_tipo_documento=l.id
INNER JOIN sistema_usuarios as u ON p.id_usuario=u.id
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_registro_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_registro_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_registro_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
	SELECT 
		HEX(p.id) as id, 
		p.order_id, 
		HEX(p.id_empresa),
		p.id_tipo_operacion,
		p.numero, 
		p.nro_control, 
		DATE_FORMAT(p.fecha,'%Y-%m-%dT%TZ') as fecha,
		p.fecha_registro, 
		HEX(p.id_beneficiario), 
		b.nombre as beneficiario_nombre,
		b.rif as beneficiario_rif,
		HEX(p.id_partida), 
		x.nombre as partida_nombre,
		HEX(p.id_cuenta), 
		p.descripcion, 
		HEX(p.id_tipo_documento), 
		p.nro_control_documento, 
		p.nro_documento,
		p.nro_factura_afectada, 
		p.fecha_documento, 
		p.monto_exento, 
		p.base_imponible, 
		p.pct_alicuota, 
		p.es_retiva,
		p.es_retislr,
		p.id_status, 
		HEX(p.id_usuario),
		@monto_iva:=ROUND(COALESCE((p.base_imponible*((p.pct_alicuota/100)+1)-p.base_imponible),0),2) as monto_iva,
		@monto_gasto:=ROUND(COALESCE((p.monto_exento+p.base_imponible+(@monto_iva)),0),2) as monto_gasto
	FROM gastos_registro as p
	LEFT JOIN gastos_beneficiarios as b ON p.id_beneficiario=b.id
	LEFT JOIN gastos_plan_cuentas as x ON p.id_partida=x.id
	LEFT JOIN caja_bancos as c ON p.id_cuenta=c.id
	LEFT JOIN sistema_listas as l ON p.id_tipo_documento=l.id
	LEFT JOIN sistema_usuarios as u ON p.id_usuario=u.id
	WHERE p.id_empresa=UNHEX(_id_empresa) 
	ORDER BY p.order_id DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for gastos_registro_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `gastos_registro_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `gastos_registro_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo_operacion` VARCHAR(1), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_beneficiario` VARCHAR(36), IN `_id_partida` VARCHAR(36), IN `_id_cuenta` VARCHAR(36), IN `_descripcion` VARCHAR(200), IN `_id_tipo_documento` VARCHAR(36), IN `_nro_control_documento` VARCHAR(20), IN `_nro_documento` VARCHAR(20), IN `_fecha_documento` DATETIME, IN `_nro_factura_afectada` VARCHAR(20),  IN `_monto_exento` DOUBLE(16,2), IN `_base_imponible` DOUBLE(16,2), IN `_pct_alicuota` DOUBLE(6,2), IN `_es_retiva` TINYINT(1), IN `_es_retislr` TINYINT(1),  IN `_id_status` VARCHAR(36), IN `_id_usuario` VARCHAR(36))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE gastos_registro as p
SET 
p.id_empresa=UNHEX(_id_empresa),
p.id_tipo_operacion=_id_tipo_operacion, 
p.numero=_numero, 
p.nro_control=_nro_control, 
p.fecha=_fecha, 
p.fecha_registro=_fecha_registro, 
p.id_beneficiario=UNHEX(_id_beneficiario), 
p.id_partida=UNHEX(_id_partida), 
p.id_cuenta=UNHEX(_id_cuenta), 
p.descripcion=_descripcion, 
p.id_tipo_documento=UNHEX(_id_tipo_documento), 
p.nro_control_documento=_nro_control_documento, 
p.nro_documento=_nro_documento, 
p.fecha_documento=_fecha_documento, 
p.nro_factura_afectada=_nro_factura_afectada,
p.monto_exento=_monto_exento, 
p.base_imponible=_base_imponible, 
p.pct_alicuota=_pct_alicuota,
p.es_retiva=_es_retiva,
p.es_retislr=_es_retislr, 
p.id_status=_id_status, 
p.id_usuario=UNHEX(_id_usuario)

WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for getUUID
-- ----------------------------
DROP PROCEDURE IF EXISTS `getUUID`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getUUID`()
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		SELECT @my_uuid as id, @order_id as order_id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_almacenes_DEFAULT
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_almacenes_DEFAULT`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_almacenes_DEFAULT`(IN `_id_empresa` VARCHAR(36))
BEGIN
	SELECT HEX(a.id) as id, a.order_id, a.nombre, a.descrip, a.es_default
		FROM inventario_almacenes as a
		WHERE a.id_empresa = UNHEX(_id_empresa)  AND a.es_default= 1 LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_almacenes_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_almacenes_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_almacenes_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(100), IN `_es_default` TINYINT(1))
BEGIN
    #SET @my_uuid= ordered_uuid(UUID());
    #SET @order_id=UNIX_TIMESTAMP();
		SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();

		INSERT INTO inventario_almacenes(id, order_id, id_empresa, nombre, descrip, es_default) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _nombre, _descrip, _es_default);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_almacenes_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_almacenes_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_almacenes_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(a.id) as id, a.order_id, HEX(a.id_empresa) as id_empresa, a.nombre, a.descrip, a.es_default
FROM inventario_almacenes as a
WHERE a.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_almacenes_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_almacenes_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_almacenes_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
        SELECT hex(a.id) as id,
                    a.order_id,
										HEX(a.id_empresa),
										a.nombre,
										a.descrip,
										a.es_default
        FROM inventario_almacenes as a
				WHERE a.id_empresa = UNHEX(_id_empresa);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_almacenes_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_almacenes_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_almacenes_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36),  IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(100), IN `_es_default` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE inventario_almacenes as a
SET a.id_empresa=UNHEX(_id_empresa), a.nombre=_nombre, a.descrip=_descrip, a.es_default=_es_default
WHERE a.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_atributos_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_atributos_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_atributos_DEL`(IN `_id_producto` VARCHAR(36))
    NO SQL
BEGIN
DELETE a.* FROM inventario_atributos as a WHERE a.id_producto = UNHEX(_id_producto);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_atributos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_atributos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_atributos_INS`(IN `_id_producto` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_valor` VARCHAR(100))
BEGIN
    SET @my_uuid= ordered_uuid(UUID());
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO inventario_atributos(id, order_id, id_producto, nombre, valor) 
		VALUES (@my_uuid, @order_id, UNHEX(_id_producto), _nombre, _valor);

		SELECT HEX(@my_uuid) as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_atributos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_atributos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_atributos_SEL`(IN `_id_producto` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(a.id) as id, a.order_id, HEX(a.id_producto), a.nombre, a.valor
FROM inventario_atributos as a
WHERE a.id_producto = UNHEX(_id_producto);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_categorias_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_categorias_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_categorias_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(100), IN `_id_padre` VARCHAR(36))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO inventario_categorias(id, order_id, id_empresa, nombre, descrip, id_padre) 
VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _nombre, _descrip, UNHEX(_id_padre));

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_categorias_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_categorias_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_categorias_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(c.id) as id, c.order_id, HEX(c.id_empresa) as id_empresa, t.nombre as padre_nombre, c.nombre, c.descrip, HEX(c.id_padre) as id_padre
FROM inventario_categorias as c  
LEFT JOIN inventario_categorias as t ON c.id_padre= t.id
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_categorias_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_categorias_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_categorias_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
		SELECT hex(c.id) as id,
								c.order_id,
								hex(c.id_empresa) as id_empresa,
								hex(c.id_padre) as id_padre,
								t.nombre as padre_nombre,
																c.nombre,
																c.descrip
		FROM inventario_categorias as c
		LEFT JOIN inventario_categorias as t ON c.id_padre= t.id
		WHERE c.id_empresa=UNHEX(_id_empresa)
		ORDER BY c.nombre;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_categorias_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_categorias_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_categorias_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(100), IN `_id_padre` VARCHAR(36))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE inventario_categorias as c
SET c.id_empresa=UNHEX(_id_empresa), c.nombre=_nombre, c.descrip=_descrip, c.id_padre=UNHEX(_id_padre)
WHERE c.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_AUM
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_AUM`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_AUM`(IN `_id_producto` VARCHAR(36), IN `_id_almacen` VARCHAR(36), IN `_cantidad` DOUBLE(10,2))
    NO SQL
BEGIN
SET @existe=(SELECT COUNT(e.id) as cuantos FROM inventario_existencias as e WHERE e.id_producto=UNHEX(_id_producto) AND e.id_almacen=UNHEX(_id_almacen));

IF @existe>0 THEN 
	UPDATE inventario_existencias as e
	SET e.cantidad=e.cantidad + _cantidad
	WHERE e.id_producto=UNHEX(_id_producto) AND e.id_almacen=UNHEX(_id_almacen);
ELSE
	SET @new_uuid=uuid();
	SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
	SET @order_id=UNIX_TIMESTAMP();
	INSERT INTO inventario_existencias(id, order_id, id_producto, id_almacen, cantidad) 
	VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_producto), UNHEX(_id_almacen), _cantidad);
END IF;

SELECT _id_producto as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_DIS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_DIS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_DIS`(IN `_id_producto` VARCHAR(36), IN `_id_almacen` VARCHAR(36), IN `_cantidad` DOUBLE(10,2))
    NO SQL
BEGIN
SET @existe=(SELECT COUNT(e.id) as cuantos FROM inventario_existencias as e WHERE e.id_producto=UNHEX(_id_producto) AND e.id_almacen=UNHEX(_id_almacen));

IF @existe>0 THEN 
	UPDATE inventario_existencias as e
	SET e.cantidad=e.cantidad - _cantidad
	WHERE e.id_producto=UNHEX(_id_producto) AND e.id_almacen=UNHEX(_id_almacen);
ELSE
	SET @new_uuid=uuid();
	SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
	SET @order_id=UNIX_TIMESTAMP();
	INSERT INTO inventario_existencias(id, order_id, id_producto, id_almacen, cantidad) 
	VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_producto), UNHEX(_id_almacen), _cantidad);
END IF;

SELECT _id_producto as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_INS`(IN `_id` VARCHAR(36), IN `_id_producto` VARCHAR(36), IN `_id_almacen` VARCHAR(36), IN `_cantidad` DOUBLE(10,2))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO inventario_existencias(id, order_id, id_producto, id_almacen, cantidad) 
VALUES (UNHEX(@my_uuid), @order_id, _id_producto, _id_almacen, _cantidad);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_MAX_ALM
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_MAX_ALM`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_MAX_ALM`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
        SELECT 	HEX(e.id_almacen) as id_almacen, MAX(e.cantidad) as max_cantidad
								FROM inventario_existencias as e
				WHERE e.id_producto=UNHEX(_id);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_ONE`(IN `_id_producto` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(e.id) as id, 
	e.order_id, 
	HEX(e.id_producto) as id_producto, 
	HEX(e.id_almacen) as id_almacen, 
	a.nombre, 
	COALESCE((e.cantidad),0) as cantidad
FROM inventario_existencias as e
RIGHT JOIN inventario_almacenes as a ON e.id_almacen=a.id
WHERE e.id_producto = UNHEX(_id_producto);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_SEL`()
    NO SQL
BEGIN
        SELECT hex(e.id) as id,
                    e.order_id,
										hex(e.id_producto),
										p.nombre,
										p.codigo,
										u.nombre,
										hex(e.id_almacen),
										a.nombre,
										e.cantidad
        FROM inventario_existencias as e
				INNER JOIN inventario_almacenes as a ON e.id_almacen=a.id
				INNER JOIN inventario_productos as p ON e.id_producto=p.id
				INNER JOIN sistema_unidades as u ON p.id_unidad_venta=u.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_existencias_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_existencias_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_existencias_UPD`(IN `_id_producto` VARCHAR(36), IN `_id_almacen` VARCHAR(36), IN `_cantidad` DOUBLE(10,2))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE inventario_existencias as e
SET e.cantidad=_cantidad
WHERE e.id_producto=UNHEX(_id_producto) AND e.id_almacen=UNHEX(_id_almacen);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_formulas_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_formulas_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_formulas_DEL`(IN `_id_producto` VARCHAR(36))
    NO SQL
BEGIN
DELETE f.* FROM inventario_formulas as f WHERE f.id_producto = UNHEX(_id_producto);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_formulas_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_formulas_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_formulas_INS`(IN `_id_producto` VARCHAR(36), IN `_id_componente` VARCHAR(36), IN `_id_unidad` VARCHAR(36), IN `_cantidad` DOUBLE(10,2))
BEGIN
    SET @my_uuid= ordered_uuid(UUID());
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO inventario_formulas(id, order_id, id_producto, id_componente, id_unidad, cantidad) 
		VALUES (@my_uuid, @order_id, UNHEX(_id_producto), UNHEX(_id_componente), UNHEX(_id_unidad), _cantidad);

		SELECT HEX(@my_uuid) as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_formulas_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_formulas_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_formulas_SEL`(IN `_id_producto` VARCHAR(36))
    NO SQL
BEGIN
SET @dolar=(SELECT c.valor FROM sistema_cotizaciones as c  ORDER BY c.id DESC LIMIT 1);
#SET @fct_conv=(SELECT c.valor FROM sistema_cotizaciones as c  ORDER BY c.id DESC LIMIT 1);

SELECT HEX(f.id) as id, 
			f.order_id, 
			HEX(f.id_producto) as id_producto, 
			HEX(f.id_componente) as id_componente, 
			HEX(f.id_unidad) as id_unidad, 
			s.codigo as componente_codigo,
			s.nombre as componente_nombre, 
			uc.nombre_display as componente_unidad, 
			f.cantidad,
			@componente_costo_dolar:=(((p.dolar_costo_dolar * uc.factor_conversion)/u.factor_conversion)* f.cantidad ) as componente_costo_dolar,
			@componente_dolar_costo_bruto:= ROUND(COALESCE((@componente_costo_dolar * @dolar),0),2) as componente_dolar_costo_bruto,
			@componente_dolar_costo_neto:=ROUND(COALESCE(((@componente_dolar_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),@componente_dolar_costo_bruto),2) as componente_dolar_costo_neto 
FROM inventario_formulas as f
INNER JOIN inventario_productos as p ON f.id_componente=p.id
INNER JOIN sistema_productos as s ON p.id_producto=s.id
INNER JOIN sistema_unidades as u ON s.id_unidad_venta=u.id
INNER JOIN sistema_unidades as uc ON f.id_unidad=uc.id
WHERE f.id_producto = UNHEX(_id_producto);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_movimientos_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_movimientos_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_movimientos_DEL`(IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36))
    NO SQL
BEGIN
DELETE m.* 
	FROM inventario_movimientos as m 
	WHERE m.id_tipo= UNHEX(_id_tipo) AND m.id_operacion= UNHEX(_id_operacion);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_movimientos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_movimientos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_movimientos_INS`(IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36), IN `_id_producto` VARCHAR(36), IN `_id_producto_padre` VARCHAR(36), IN `_id_unidad` VARCHAR(36), IN `_precio` DOUBLE(10,2), IN `_cantidad` DOUBLE(10,2), IN `_id_impuesto` VARCHAR(36), IN `_valor_impuesto` DOUBLE(10,2))
BEGIN
    SET @my_uuid= ordered_uuid(UUID());
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO inventario_movimientos(id, order_id, id_tipo, id_operacion, id_producto, id_producto_padre, id_unidad, precio, cantidad, id_impuesto, valor_impuesto) 
		VALUES (@my_uuid, @order_id, UNHEX(_id_tipo), UNHEX(_id_operacion), UNHEX(_id_producto), UNHEX(_id_producto_padre), UNHEX(_id_unidad), _precio, _cantidad, UNHEX(_id_impuesto), _valor_impuesto);

		SELECT HEX(@my_uuid) as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_movimientos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_movimientos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_movimientos_SEL`(IN `_id_tipo` VARCHAR(36), IN `_id_operacion` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(m.id) as id, 
			m.order_id, 
			HEX(m.id_producto) as id_producto, 
			HEX(m.id_producto_padre) as id_producto_padre, 
			HEX(m.id_unidad) as id_unidad,
			HEX(m.id_impuesto) as id_impuesto, 
			s.codigo as producto_codigo,
			s.nombre as producto_nombre, 
			u.nombre_display as producto_unidad, 
			m.cantidad,
			m.precio,
			m.valor_impuesto,
			@monto:=(m.precio * m.cantidad ) as monto,
			@monto_impuesto:=ROUND((@monto*((m.valor_impuesto/100)+1))-@monto ) as monto_impuesto
FROM inventario_movimientos as m
INNER JOIN inventario_productos as p ON m.id_producto=p.id
INNER JOIN sistema_productos as s ON p.id_producto=s.id
INNER JOIN sistema_unidades as u ON m.id_unidad=u.id
WHERE m.id_tipo = UNHEX(_id_tipo) AND m.id_operacion = UNHEX(_id_operacion) AND m.id_producto_padre=UNHEX('00000000000000000000000000000000') ;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_operaciones_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_operaciones_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_operaciones_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_doc_origen` VARCHAR(36), IN `_id_origen` VARCHAR(36), IN `_id_destino` VARCHAR(36), IN `_id_usuario` VARCHAR(36), IN `_id_status` VARCHAR(36), IN `_id_estado` VARCHAR(36),  IN `_observacion` VARCHAR(255))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();

		INSERT INTO inventario_operaciones(id, order_id, id_empresa, id_tipo, numero, nro_control, fecha, fecha_registro, id_doc_origen, id_origen, id_destino, id_usuario, id_status, id_estado, observacion) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_tipo), _numero, _nro_control, _fecha, _fecha_registro, UNHEX(_id_doc_origen), UNHEX(_id_origen), UNHEX(_id_destino), UNHEX(_id_usuario), _id_status, id_estado, _observacion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_operaciones_LAST
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_operaciones_LAST`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_operaciones_LAST`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
        SELECT 	MAX(o.numero) as lastnumero
								FROM inventario_operaciones as o
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_operaciones_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_operaciones_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_operaciones_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(o.id) as id, 
	o.order_id,
	HEX(o.id_empresa) as id_empresa,
	o.numero, 
	o.nro_control, 
	o.fecha, 
	o.fecha_registro, 
	HEX(o.id_tipo) as id_tipo,
	so.nombre as tipo_operacion_nombre,
	HEX(o.id_origen) as id_origen, 
	ao.nombre origen_nombre,
	HEX(o.id_destino) as id_destino, 
	ad.nombre as destino_nombre,
	HEX(o.id_usuario) as id_usuario, 
	o.id_status, 
	u.nombre as usuario_nombre,
	o.observacion
FROM inventario_operaciones as o
INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
LEFT JOIN inventario_almacenes as ao ON o.id_origen=ao.id
LEFT JOIN inventario_almacenes as ad ON o.id_destino=ad.id
LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
WHERE o.id = UNHEX(_id) LIMIT 1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_operaciones_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_operaciones_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_operaciones_SEL`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
        SELECT hex(o.id) as id,
                    o.order_id,
										HEX(o.id_empresa) as id_empresa,
										hex(o.id_tipo) as id_tipo,
										so.nombre as tipo_operacion_nombre,
										o.numero,
										o.nro_control,
										DATE_FORMAT(o.fecha,'%Y-%m-%dT%TZ') as fecha,
										o.fecha_registro,
										hex(o.id_origen) as id_origen,
										ao.nombre origen_nombre,
										hex(o.id_destino) as id_destino,
										ad.nombre as destino_nombre,
										hex(o.id_usuario) as id_usuario,
										u.nombre as usuario_nombre,
										o.id_status,
										o.observacion
        FROM inventario_operaciones as o
				INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
				LEFT JOIN inventario_almacenes as ao ON o.id_origen=ao.id
				LEFT JOIN inventario_almacenes as ad ON o.id_destino=ad.id
				LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo)
				ORDER BY o.order_id DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_operaciones_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_operaciones_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_operaciones_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_origen` VARCHAR(36), IN `_id_destino` VARCHAR(36), IN `_id_usuario` VARCHAR(36), IN `_id_status` VARCHAR(36), IN `_observacion` VARCHAR(255))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE inventario_operaciones as o
SET o.id_empresa=UNHEX(_id_empresa), o.id_tipo= UNHEX(_id_tipo), o.numero= _numero, o.nro_control= _nro_control, o.fecha= _fecha, o.fecha_registro= _fecha_registro,
o.id_origen= UNHEX(_id_origen), o.id_destino= UNHEX(_id_destino), o.id_usuario= UNHEX(_id_usuario), o.id_status= _id_status, o.observacion= _observacion
WHERE o.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_productos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_productos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_productos_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_producto` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_id_categoria` VARCHAR(36), IN `_nombre_display` VARCHAR(200), IN `_costo_pct_dcto` DOUBLE(10,2), IN `_costo_pct_adic` DOUBLE(10,2), IN `_dolar_costo_anterior` DOUBLE(10,2), IN `_dolar_costo_dolar` DOUBLE(10,2), IN `_dolar_pct_utilidad` DOUBLE(10,2), IN `_factura_costo_anterior` DOUBLE(10,2), IN `_factura_costo_bruto` DOUBLE(10,2), IN `_factura_pct_utilidad` DOUBLE(10,2), IN `_lista_costo_anterior` DOUBLE(10,2), IN `_lista_costo_bruto` DOUBLE(10,2), IN `_lista_pct_utilidad` DOUBLE(10,2), IN `_id_impuesto` VARCHAR(36), IN `_es_inactivo` TINYINT(1), IN `_es_editable` TINYINT(1))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO inventario_productos(id, order_id, id_empresa, id_producto, id_tipo, id_categoria, nombre_display, costo_pct_dcto, costo_pct_adic, dolar_costo_anterior, dolar_costo_dolar, dolar_pct_utilidad, factura_costo_anterior, factura_costo_bruto, factura_pct_utilidad, lista_costo_anterior, lista_costo_bruto, lista_pct_utilidad, id_impuesto, es_inactivo, es_editable) 
VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_producto), UNHEX(_id_tipo), UNHEX(_id_categoria), _nombre_display, _costo_pct_dcto, _costo_pct_adic, _dolar_costo_anterior, _dolar_costo_dolar, _dolar_pct_utilidad, _factura_costo_anterior, _factura_costo_bruto, _factura_pct_utilidad, _lista_costo_anterior, _lista_costo_bruto, _lista_pct_utilidad, UNHEX(_id_impuesto), _es_inactivo, _es_editable);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_productos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_productos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_productos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SET @dolar=(SELECT c.valor FROM sistema_cotizaciones as c  ORDER BY c.id DESC LIMIT 1);
SELECT HEX(p.id) as id, p.order_id, 
			HEX(p.id_empresa) as id_empresa,
			HEX(p.id_producto) as id_producto,
			HEX(p.id_tipo) as id_tipo,
			t.nombre as tipo_nombre,
			HEX(p.id_categoria) as id_categoria, 
			c.nombre as categoria_nombre,
			s.codigo, 
			s.nombre, 
			p.nombre_display, 
			HEX(s.id_unidad_compra) as id_unidad_compra, 
			uc.nombre as unidad_compra_nombre,
			HEX(s.id_unidad_venta) as id_unidad_venta, 
			uv.nombre as unidad_venta_nombre,
			s.cantidad_unidad_compra,
			p.costo_pct_dcto,
			p.costo_pct_adic,
			#DOLAR
			p.dolar_costo_anterior,
			p.dolar_costo_dolar,
			@dolar_costo_bruto:= ROUND(COALESCE((p.dolar_costo_dolar * @dolar),0),2) as dolar_costo_bruto,
			@dolar_costo_neto:=ROUND(COALESCE(((@dolar_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),@dolar_costo_bruto),2) as dolar_costo_neto,
			p.dolar_pct_utilidad,
			@dolar_precio_inicial:=ROUND(COALESCE((@dolar_costo_neto*((p.dolar_pct_utilidad/100)+1)),@dolar_costo_neto),2) as dolar_precio_inicial,
			@dolar_precio_final:=ROUND(COALESCE((@dolar_precio_inicial*((i.valor/100)+1)),@dolar_precio_inicial),2) as dolar_precio_final,
			#FACTURA
			p.factura_costo_anterior,
			@factura_costo_dolar:= ROUND(COALESCE((p.factura_costo_bruto / @dolar),0),2) as factura_costo_dolar,
			p.factura_costo_bruto,
			@factura_costo_neto:=ROUND(COALESCE(((p.factura_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.factura_costo_bruto),2) as factura_costo_neto,
			p.factura_pct_utilidad,
			@factura_precio_inicial:=ROUND(COALESCE((@factura_costo_neto*((p.factura_pct_utilidad/100)+1)),@factura_costo_neto),2) as factura_precio_inicial,
			@factura_precio_final:=ROUND(COALESCE((@factura_precio_inicial*((i.valor/100)+1)),@factura_precio_inicial),2) as factura_precio_final,
			#LISTA
			p.lista_costo_anterior,
			@lista_costo_dolar:= ROUND(COALESCE((p.lista_costo_bruto / @dolar),0),2) as lista_costo_dolar,
			p.lista_costo_bruto,
			@lista_costo_neto:=ROUND(COALESCE(((p.lista_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.lista_costo_bruto),2) as lista_costo_neto,
			p.lista_pct_utilidad,
			@lista_precio_inicial:=ROUND(COALESCE((@lista_costo_neto*((p.lista_pct_utilidad/100)+1)),@lista_costo_neto),2) as lista_precio_inicial,
			@lista_precio_final:=ROUND(COALESCE((@lista_precio_inicial*((i.valor/100)+1)),@lista_precio_inicial),2) as lista_precio_final,
			#FINAL
			HEX(p.id_impuesto) as id_impuesto,
			ROUND(COALESCE(GREATEST(@dolar_precio_final,@factura_precio_final,@lista_precio_final),0),2) as precio_venta,
			i.nombre as impuesto_nombre,
			i.valor as impuesto_valor,
			p.es_inactivo,
			p.es_editable,
			COALESCE(e.cantidad,0) as total_existencia
		FROM inventario_productos as p  
		LEFT JOIN sistema_productos as s ON p.id_producto=s.id
		LEFT JOIN sistema_listas as t ON p.id_tipo=t.id
		LEFT JOIN inventario_categorias as c ON p.id_categoria=c.id
		LEFT JOIN sistema_unidades as uc ON s.id_unidad_compra=uc.id
		LEFT JOIN sistema_unidades as uv ON s.id_unidad_venta=uv.id
		LEFT JOIN sistema_impuestos as i ON p.id_impuesto=i.id
		LEFT JOIN (SELECT ie.id_producto,SUM(ie.cantidad) as cantidad
                            FROM inventario_existencias ie 
                            GROUP BY ie.id_producto) e ON p.id=e.id_producto
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_productos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_productos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_productos_SEL`(IN `_id_empresa` VARCHAR(36), IN `_id_categoria` VARCHAR(36))
    NO SQL
BEGIN
	SET @dolar=(SELECT c.valor FROM sistema_cotizaciones as c  ORDER BY c.id DESC LIMIT 1);

	IF _id_categoria = "" THEN 
		SELECT HEX(p.id) as id, p.order_id, 
			HEX(p.id_empresa) as id_empresa,
			HEX(p.id_producto) as id_producto,
			HEX(p.id_tipo) as id_tipo,
			t.nombre as tipo_nombre,
			HEX(p.id_categoria) as id_categoria, 
			c.nombre as categoria_nombre,
			s.codigo, 
			s.nombre, 
			p.nombre_display, 
			HEX(s.id_unidad_compra) as id_unidad_compra, 
			uc.nombre as unidad_compra_nombre,
			HEX(s.id_unidad_venta) as id_unidad_venta, 
			uv.nombre as unidad_venta_nombre,
			s.cantidad_unidad_compra,
			p.costo_pct_dcto,
			p.costo_pct_adic,
			#DOLAR
			p.dolar_costo_anterior,
			p.dolar_costo_dolar,
			@dolar_costo_bruto:= ROUND(COALESCE((p.dolar_costo_dolar * @dolar),0),2) as dolar_costo_bruto,
			@dolar_costo_neto:=ROUND(COALESCE(((@dolar_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),@dolar_costo_bruto),2) as dolar_costo_neto,
			p.dolar_pct_utilidad,
			@dolar_precio_inicial:=ROUND(COALESCE((@dolar_costo_neto*((p.dolar_pct_utilidad/100)+1)),@dolar_costo_neto),2) as dolar_precio_inicial,
			@dolar_precio_final:=ROUND(COALESCE((@dolar_precio_inicial*((i.valor/100)+1)),@dolar_precio_inicial),2) as dolar_precio_final,
			#FACTURA
			p.factura_costo_anterior,
			@factura_costo_dolar:= ROUND(COALESCE((p.factura_costo_bruto / @dolar),0),2) as factura_costo_dolar,
			p.factura_costo_bruto,
			@factura_costo_neto:=ROUND(COALESCE(((p.factura_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.factura_costo_bruto),2) as factura_costo_neto,
			p.factura_pct_utilidad,
			@factura_precio_inicial:=ROUND(COALESCE((@factura_costo_neto*((p.factura_pct_utilidad/100)+1)),@factura_costo_neto),2) as factura_precio_inicial,
			@factura_precio_final:=ROUND(COALESCE((@factura_precio_inicial*((i.valor/100)+1)),@factura_precio_inicial),2) as factura_precio_final,
			#LISTA
			p.lista_costo_anterior,
			@lista_costo_dolar:= ROUND(COALESCE((p.lista_costo_bruto / @dolar),0),2) as lista_costo_dolar,
			p.lista_costo_bruto,
			@lista_costo_neto:=ROUND(COALESCE(((p.lista_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.lista_costo_bruto),2) as lista_costo_neto,
			p.lista_pct_utilidad,
			@lista_precio_inicial:=ROUND(COALESCE((@lista_costo_neto*((p.lista_pct_utilidad/100)+1)),@lista_costo_neto),2) as lista_precio_inicial,
			@lista_precio_final:=ROUND(COALESCE((@lista_precio_inicial*((i.valor/100)+1)),@lista_precio_inicial),2) as lista_precio_final,
			#FINAL
			HEX(p.id_impuesto) as id_impuesto,
			ROUND(COALESCE(GREATEST(@dolar_precio_final,@factura_precio_final,@lista_precio_final),0),2) as precio_venta,
			i.nombre as impuesto_nombre,
			i.valor as impuesto_valor,
			p.es_inactivo,
			p.es_editable,
			COALESCE(e.cantidad,0) as total_existencia
		FROM inventario_productos as p  
		LEFT JOIN sistema_productos as s ON p.id_producto=s.id
		LEFT JOIN sistema_listas as t ON p.id_tipo=t.id
		LEFT JOIN inventario_categorias as c ON p.id_categoria=c.id
		LEFT JOIN sistema_unidades as uc ON s.id_unidad_compra=uc.id
		LEFT JOIN sistema_unidades as uv ON s.id_unidad_venta=uv.id
		LEFT JOIN sistema_impuestos as i ON p.id_impuesto=i.id
		LEFT JOIN (SELECT ie.id_producto,SUM(ie.cantidad) as cantidad
                            FROM inventario_existencias ie 
                            GROUP BY ie.id_producto) e ON p.id=e.id_producto
    WHERE p.id_empresa=UNHEX(_id_empresa) 
		ORDER BY p.nombre;
	ELSE 
		SELECT HEX(p.id) as id, p.order_id, 
			HEX(p.id_empresa) as id_empresa,
			HEX(p.id_producto) as id_producto,
			HEX(p.id_tipo) as id_tipo,
			t.nombre as tipo_nombre,
			HEX(p.id_categoria) as id_categoria, 
			c.nombre as categoria_nombre,
			s.codigo, 
			s.nombre, 
			p.nombre_display, 
			HEX(s.id_unidad_compra) as id_unidad_compra, 
			uc.nombre as unidad_compra_nombre,
			HEX(s.id_unidad_venta) as id_unidad_venta, 
			uv.nombre as unidad_venta_nombre,
			s.cantidad_unidad_compra,
			p.costo_pct_dcto,
			p.costo_pct_adic,
			#DOLAR
			p.dolar_costo_anterior,
			p.dolar_costo_dolar,
			@dolar_costo_bruto:= ROUND(COALESCE((p.dolar_costo_dolar * @dolar),0),2) as dolar_costo_bruto,
			@dolar_costo_neto:=ROUND(COALESCE(((@dolar_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),@dolar_costo_bruto),2) as dolar_costo_neto,
			p.dolar_pct_utilidad,
			@dolar_precio_inicial:=ROUND(COALESCE((@dolar_costo_neto*((p.dolar_pct_utilidad/100)+1)),@dolar_costo_neto),2) as dolar_precio_inicial,
			@dolar_precio_final:=ROUND(COALESCE((@dolar_precio_inicial*((i.valor/100)+1)),@dolar_precio_inicial),2) as dolar_precio_final,
			#FACTURA
			p.factura_costo_anterior,
			@factura_costo_dolar:= ROUND(COALESCE((p.factura_costo_bruto / @dolar),0),2) as factura_costo_dolar,
			p.factura_costo_bruto,
			@factura_costo_neto:=ROUND(COALESCE(((p.factura_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.factura_costo_bruto),2) as factura_costo_neto,
			p.factura_pct_utilidad,
			@factura_precio_inicial:=ROUND(COALESCE((@factura_costo_neto*((p.factura_pct_utilidad/100)+1)),@factura_costo_neto),2) as factura_precio_inicial,
			@factura_precio_final:=ROUND(COALESCE((@factura_precio_inicial*((i.valor/100)+1)),@factura_precio_inicial),2) as factura_precio_final,
			#LISTA
			p.lista_costo_anterior,
			@lista_costo_dolar:= ROUND(COALESCE((p.lista_costo_bruto / @dolar),0),2) as lista_costo_dolar,
			p.lista_costo_bruto,
			@lista_costo_neto:=ROUND(COALESCE(((p.lista_costo_bruto/((p.costo_pct_dcto/100)+1))*((p.costo_pct_adic/100)+1)),p.lista_costo_bruto),2) as lista_costo_neto,
			p.lista_pct_utilidad,
			@lista_precio_inicial:=ROUND(COALESCE((@lista_costo_neto*((p.lista_pct_utilidad/100)+1)),@lista_costo_neto),2) as lista_precio_inicial,
			@lista_precio_final:=ROUND(COALESCE((@lista_precio_inicial*((i.valor/100)+1)),@lista_precio_inicial),2) as lista_precio_final,
			#FINAL
			HEX(p.id_impuesto) as id_impuesto,
			ROUND(COALESCE(GREATEST(@dolar_precio_final,@factura_precio_final,@lista_precio_final),0),2) as precio_venta,
			i.nombre as impuesto_nombre,
			i.valor as impuesto_valor,
			p.es_inactivo,
			p.es_editable,
			COALESCE(e.cantidad,0) as total_existencia
		FROM inventario_productos as p  
		LEFT JOIN sistema_productos as s ON p.id_producto=s.id
		LEFT JOIN sistema_listas as t ON p.id_tipo=t.id
		LEFT JOIN inventario_categorias as c ON p.id_categoria=c.id
		LEFT JOIN sistema_unidades as uc ON s.id_unidad_compra=uc.id
		LEFT JOIN sistema_unidades as uv ON s.id_unidad_venta=uv.id
		LEFT JOIN sistema_impuestos as i ON p.id_impuesto=i.id
		LEFT JOIN (SELECT ie.id_producto,SUM(ie.cantidad) as cantidad
                            FROM inventario_existencias ie 
                            GROUP BY ie.id_producto) e ON p.id=e.id_producto
    WHERE p.id_empresa=UNHEX(_id_empresa) AND p.id_categoria=UNHEX(_id_categoria)
		ORDER BY p.nombre;

	END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for inventario_productos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `inventario_productos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inventario_productos_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_id_producto` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_id_categoria` VARCHAR(36), IN `_nombre_display` VARCHAR(200), IN `_costo_pct_dcto` DOUBLE(10,2), IN `_costo_pct_adic` DOUBLE(10,2), IN `_dolar_costo_anterior` DOUBLE(10,2), IN `_dolar_costo_dolar` DOUBLE(10,2), IN `_dolar_pct_utilidad` DOUBLE(10,2), IN `_factura_costo_anterior` DOUBLE(10,2), IN `_factura_costo_bruto` DOUBLE(10,2), IN `_factura_pct_utilidad` DOUBLE(10,2), IN `_lista_costo_anterior` DOUBLE(10,2), IN `_lista_costo_bruto` DOUBLE(10,2), IN `_lista_pct_utilidad` DOUBLE(10,2), IN `_id_impuesto` VARCHAR(36), IN `_es_inactivo` TINYINT(1), IN `_es_editable` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE inventario_productos as p
SET 
	p.id_empresa=UNHEX(_id_empresa),
	p.id_producto=UNHEX(_id_producto), 
	p.id_tipo=UNHEX(_id_tipo), 
	p.id_categoria=UNHEX(_id_categoria), 
	p.nombre_display=_nombre_display,
	p.costo_pct_dcto=_costo_pct_dcto,
	p.costo_pct_adic=_costo_pct_adic,
	p.dolar_costo_anterior=_dolar_costo_anterior,
	p.dolar_costo_dolar=_dolar_costo_dolar,
	p.dolar_pct_utilidad=_dolar_pct_utilidad,
	p.factura_costo_anterior=_factura_costo_anterior,
	p.factura_costo_bruto=_factura_costo_bruto,
	p.factura_pct_utilidad=_factura_pct_utilidad,
	p.lista_costo_anterior=_lista_costo_anterior,
	p.lista_costo_bruto=_lista_costo_bruto,
	p.lista_pct_utilidad=_lista_pct_utilidad,
	p.id_impuesto=UNHEX(_id_impuesto),
	p.es_inactivo=_es_inactivo,
	p.es_editable=_es_editable

WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for prueba
-- ----------------------------
DROP PROCEDURE IF EXISTS `prueba`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `prueba`()
    NO SQL
BEGIN
SET @valor = UUID();
SET @verga=ordered_uuid(UUID());
SELECT @valor, HEX(@verga);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_caja_cuentas_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_caja_cuentas_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_caja_cuentas_detalle`(IN `_id_empresa` VARCHAR(36), IN `_id_cuenta` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
SELECT 
		HEX(m.id_cuenta) as cuenta_id,
			m.fecha,
			i.nombre as instrumento_nombre,
			m.numero_operacion,
			o.nombre as operacion_nombre,
			v.nro_control,
			c.nombre as cliente_nombre,
			c.rif as cliente_rif,
			m.monto
	FROM caja_movimientos as m
	INNER JOIN caja_instrumentos as i ON m.id_instrumento=i.id
	INNER JOIN ventas_operaciones as v ON m.id_operacion=v.id
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN ventas_clientes as c ON v.id_cliente=c.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND
		m.id_cuenta = UNHEX(_id_cuenta) AND 
		v.id_status=1 
		AND (m.fecha BETWEEN _desde AND _hasta);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_caja_cuentas_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_caja_cuentas_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_caja_cuentas_resumen`(IN `_id_empresa` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
SELECT 
		HEX(m.id_cuenta) as cuenta_id,
		c.numero as cuenta_numero,
		b.nombre as banco_nombre,
		COUNT(DISTINCT m.id_cuenta) as cantidad,
		SUM(m.monto) as monto
	FROM caja_movimientos as m
	INNER JOIN caja_bancos as c ON m.id_cuenta =c.id
	INNER JOIN sistema_listas as b ON c.id_banco =b.id
	INNER JOIN ventas_operaciones as v ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_status=1 
		AND (m.fecha BETWEEN _desde AND _hasta)
	GROUP BY m.id_instrumento;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_caja_instrumentos_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_caja_instrumentos_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_caja_instrumentos_detalle`(IN `_id_empresa` VARCHAR(36), IN `_id_instrumento` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
SELECT 
		HEX(m.id_instrumento) as instrumento_id,
			m.fecha,
			m.numero_operacion,
			o.nombre as operacion_nombre,
			v.nro_control,
			c.nombre as cliente_nombre,
			c.rif as cliente_rif,
			m.monto
	FROM caja_movimientos as m
	INNER JOIN caja_instrumentos as i ON m.id_instrumento=i.id
	INNER JOIN ventas_operaciones as v ON m.id_operacion=v.id
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN ventas_clientes as c ON v.id_cliente=c.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND
		m.id_instrumento = UNHEX(_id_instrumento) AND 
		v.id_status=1 
		AND (m.fecha BETWEEN _desde AND _hasta);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_caja_instrumentos_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_caja_instrumentos_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_caja_instrumentos_resumen`(IN `_id_empresa` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
SELECT 
		HEX(m.id_instrumento) as instrumento_id,
		i.nombre as instrumento_nombre,
		COUNT(DISTINCT m.id_instrumento) as cantidad,
		SUM(m.monto) as monto
	FROM caja_movimientos as m
	INNER JOIN caja_instrumentos as i ON m.id_instrumento=i.id
	INNER JOIN ventas_operaciones as v ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_status=1 
		AND (m.fecha BETWEEN _desde AND _hasta)
	GROUP BY m.id_instrumento;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_compras_operaciones_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_compras_operaciones_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_compras_operaciones_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_tipo` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			v.nro_control,
			v.fecha,
			c.nombre as proveedor_nombre,
			c.rif as proveedor_rif,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM compras_operaciones as v
	INNER JOIN compras_proveedores as c ON v.id_proveedor=c.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_tipo = UNHEX(_id_tipo) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_compras_operaciones_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_compras_operaciones_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_compras_operaciones_resumen`(IN `_id_empresa` VARCHAR(36),  IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			o.nombre,
			COUNT(DISTINCT v.id) as cantidad,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM compras_operaciones as v
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id_tipo;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_compras_proveedores_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_compras_proveedores_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_compras_proveedores_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_proveedor` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			o.nombre as operacion_nombre,
			v.nro_control,
			v.fecha,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM compras_operaciones as v
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_proveedor= UNHEX(_id_proveedor) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id
	ORDER BY v.fecha;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_compras_proveedores_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_compras_proveedores_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_compras_proveedores_resumen`(IN `_id_empresa` VARCHAR(36),  IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			HEX(c.id) as id_cliente,
			HEX(o.id) as id_operacion,
			p.nombre as proveedor_nombre,
			p.rif as proveedor_rif,
			SUM(IF (o.signo_caja='+', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tp,
			SUM(IF (o.signo_caja='N', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tn,
			SUM(IF (o.signo_caja='-', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tm
			FROM compras_operaciones c
			INNER JOIN compras_proveedores p ON c.id_proveedor=p.id				
			INNER JOIN sistema_operaciones o ON c.id_tipo=o.id
			INNER JOIN inventario_movimientos m ON c.id=m.id_operacion
			
		WHERE 
			c.id_empresa = UNHEX(_id_empresa) AND 
			c.id_status=1 AND
			(c.fecha BETWEEN _desde AND _hasta)
		GROUP BY c.id_proveedor
		ORDER BY p.nombre;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_inventario_disponibilidad_formulas
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_inventario_disponibilidad_formulas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_inventario_disponibilidad_formulas`()
    NO SQL
BEGIN
		SELECT 
				HEX(f.id_producto) as id_producto, 
				HEX(p.id_categoria) as id_categoria, 
				c.nombre as categoria_nombre,p.codigo, 
				p.nombre, 
				up.nombre as unidad_nombre, 
			COALESCE(ep.cantidad,0) as total_existencia,
			MIN(COALESCE(e.cantidad,0) / (f.cantidad*u.factor_conversion)) as cantidad_disponible
		FROM inventario_formulas as f
		INNER JOIN inventario_unidades as u ON f.id_unidad = u.id
		LEFT JOIN (SELECT ie.id_producto,SUM(ie.cantidad) as cantidad
																FROM inventario_existencias ie 
																GROUP BY ie.id_producto) e ON f.id_componente=e.id_producto
		LEFT JOIN (SELECT ie.id_producto,SUM(ie.cantidad) as cantidad
																FROM inventario_existencias ie 
																GROUP BY ie.id_producto) ep ON f.id_producto=ep.id_producto                          
		INNER JOIN inventario_productos as p ON f.id_producto=p.id
		INNER JOIN inventario_unidades as up ON p.id_unidad_venta=up.id
		LEFT JOIN inventario_categorias as c ON p.id_categoria=c.id
		GROUP BY f.id_producto;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_inventario_movimientos_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_inventario_movimientos_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_inventario_movimientos_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_producto` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
SELECT 
		id_empresa,
		id_tipo, 
		tipo_nombre, 
		operacion_nombre,
		nro_control,
		fecha,
		id_producto, 
		categoria_nombre,
		producto_nombre,
		producto_codigo,
		producto_unidad_venta, 
		ent as total_entrada,
		neu as total_neutro,
		sal as total_salida 
	FROM(
		SELECT 
			HEX(x.id_empresa) as id_empresa,
			HEX(m.id_tipo) as id_tipo, 
			t.nombre as tipo_nombre,
			o.nombre as operacion_nombre,
			x.nro_control as nro_control,
			x.fecha as fecha,
			HEX(m.id_producto) as id_producto,
			HEX(p.id_categoria) as id_categoria,
			c.nombre as categoria_nombre,
			s.nombre as producto_nombre,
			s.codigo as producto_codigo,
			u.nombre_display as producto_unidad_venta,
			(IF (o.signo_inventario='+', m.cantidad,0)) as ent,
			(IF (o.signo_inventario='N', m.cantidad,0)) as neu,
			(IF (o.signo_inventario='-', m.cantidad,0)) as sal
			FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN sistema_listas t ON o.id_tipo=t.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN inventario_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				m.id_producto = UNHEX(_id_producto) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta) UNION ALL
		SELECT 
			HEX(x.id_empresa) as id_empresa,
			HEX(m.id_tipo) as id_tipo, 
			t.nombre as tipo_nombre,
			o.nombre as operacion_nombre,
			x.nro_control  as nro_control,
			x.fecha as fecha,
			HEX(m.id_producto) as id_producto,
			HEX(p.id_categoria) as id_categoria,
			c.nombre as categoria_nombre,
			s.nombre as producto_nombre,
			s.codigo as producto_codigo,
			u.nombre_display as producto_unidad_venta,
			(IF (o.signo_inventario='+', m.cantidad,0)) as ent,
			(IF (o.signo_inventario='N', m.cantidad,0)) as neu,
			(IF (o.signo_inventario='-', m.cantidad,0)) as sal
			FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN sistema_listas t ON o.id_tipo=t.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN compras_operaciones x ON m.id_operacion=x.id 
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				m.id_producto = UNHEX(_id_producto) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta) UNION ALL
		SELECT 
			HEX(x.id_empresa) as id_empresa,
			HEX(m.id_tipo) as id_tipo, 
			t.nombre as tipo_nombre,
			o.nombre as operacion_nombre,
			x.nro_control as nro_control,
			x.fecha as fecha,
			HEX(m.id_producto) as id_producto,
			HEX(p.id_categoria) as id_categoria,
			c.nombre as categoria_nombre,
			s.nombre as producto_nombre,
			s.codigo as producto_codigo,
			u.nombre_display as producto_unidad_venta,
			(IF (o.signo_inventario='+', m.cantidad,0)) as ent,
			(IF (o.signo_inventario='N', m.cantidad,0)) as neu,
			(IF (o.signo_inventario='-', m.cantidad,0)) as sal
			FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN sistema_listas t ON o.id_tipo=t.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN ventas_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				m.id_producto = UNHEX(_id_producto) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta) 
) x ORDER BY fecha;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_inventario_movimientos_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_inventario_movimientos_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_inventario_movimientos_resumen`(IN `_id_empresa` VARCHAR(36), IN `_id_categoria` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
IF _id_categoria = "" THEN 
	SELECT 
		id_producto, 
		id_categoria,
		categoria_nombre,
		producto_nombre,
		producto_codigo,
		producto_unidad_venta, 
		SUM(tp) as total_entrada,
		SUM(tn) as total_neutro,
		SUM(tm) as total_salida 
	FROM(
		#INVENTARIO
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN inventario_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta)
			GROUP BY m.id_producto UNION ALL
		#VENTAS
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN ventas_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta)
			GROUP BY m.id_producto UNION ALL
		#COMPRAS
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,			
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN compras_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta) 
			GROUP BY m.id_producto
		) X GROUP BY id_producto;
ELSE
	SELECT 
		id_producto, 
		id_categoria,
		categoria_nombre,
		producto_nombre,
		producto_codigo,
		producto_unidad_venta, 
		SUM(tp) as total_entrada,
		SUM(tn) as total_neutro,
		SUM(tm) as total_salida 
	FROM(
		#INVENTARIO
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN inventario_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				p.id_categoria=UNHEX(_id_categoria) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta)
			GROUP BY m.id_producto UNION ALL
		#VENTAS
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN ventas_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				p.id_categoria=UNHEX(_id_categoria) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta)
			GROUP BY m.id_producto UNION ALL
		#COMPRAS
		SELECT 
				HEX(m.id_producto) as id_producto,
				HEX(p.id_categoria) as id_categoria,
				c.nombre as categoria_nombre,
				s.nombre as producto_nombre,
				s.codigo as producto_codigo,
				u.nombre_display as producto_unidad_venta,			
				SUM(IF (o.signo_inventario='+', m.cantidad,0) ) as tp,
				SUM(IF (o.signo_inventario='N', m.cantidad,0) ) as tn,
				SUM(IF (o.signo_inventario='-', m.cantidad,0) ) as tm
				FROM inventario_movimientos m
				INNER JOIN sistema_operaciones o ON m.id_tipo=o.id
				INNER JOIN inventario_productos p ON m.id_producto=p.id
				INNER JOIN sistema_productos s ON p.id_producto=s.id
				INNER JOIN sistema_unidades u ON s.id_unidad_venta=u.id
				INNER JOIN inventario_categorias c ON p.id_categoria=c.id
				INNER JOIN compras_operaciones x ON m.id_operacion=x.id
			WHERE 
				x.id_empresa = UNHEX(_id_empresa) AND 
				p.id_categoria=UNHEX(_id_categoria) AND
				x.id_status=1 AND
				(x.fecha BETWEEN _desde AND _hasta) 
			GROUP BY m.id_producto
		) X GROUP BY id_producto;
END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_clientes_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_clientes_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_clientes_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_cliente` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			o.nombre as operacion_nombre,
			v.nro_control,
			v.fecha,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM ventas_operaciones as v
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_cliente= UNHEX(_id_cliente) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id
	ORDER BY v.fecha;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_clientes_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_clientes_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_clientes_resumen`(IN `_id_empresa` VARCHAR(36),  IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			HEX(c.id) as id_cliente,
			HEX(o.id) as id_operacion,
			c.nombre as cliente_nombre,
			c.rif as cliente_rif,
			SUM(IF (o.signo_caja='+', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tp,
			SUM(IF (o.signo_caja='N', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tn,
			SUM(IF (o.signo_caja='-', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tm
			FROM ventas_clientes c
			INNER JOIN ventas_operaciones x ON x.id_cliente=c.id				
			INNER JOIN sistema_operaciones o ON x.id_tipo=o.id
			INNER JOIN inventario_movimientos m ON x.id=m.id_operacion
			
		WHERE 
			x.id_empresa = UNHEX(_id_empresa) AND 
			x.id_status=1 AND
			(x.fecha BETWEEN _desde AND _hasta)
		GROUP BY c.id
		ORDER BY c.nombre;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_comisiones_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_comisiones_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_comisiones_resumen`(IN `_id_empresa` VARCHAR(36),  IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			HEX(p.id) as id_cliente,
			p.nombre as vendedor_nombre,
			p.cedula as vendedor_cedula,
			SUM(IF (o.signo_caja='+', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tp,
			SUM(IF (o.signo_caja='N', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tn,
			SUM(IF (o.signo_caja='-', (m.precio * m.cantidad)*((m.valor_impuesto/100)+1),0) ) as tm
			FROM ventas_operaciones x 
			INNER JOIN empresa_personal p ON x.id_vendedor=p.id				
			INNER JOIN sistema_operaciones o ON x.id_tipo=o.id
			INNER JOIN inventario_movimientos m ON x.id=m.id_operacion
			
		WHERE 
			x.id_empresa = UNHEX(_id_empresa) AND 
			x.id_status=1 AND
			(x.fecha BETWEEN _desde AND _hasta)
		GROUP BY p.id
		ORDER BY p.nombre;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_operaciones_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_operaciones_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_operaciones_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_tipo` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			v.nro_control,
			v.fecha,
			c.nombre as cliente_nombre,
			c.rif as cliente_rif,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM ventas_operaciones as v
	INNER JOIN ventas_clientes as c ON v.id_cliente=c.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_tipo = UNHEX(_id_tipo) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_operaciones_resumen
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_operaciones_resumen`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_operaciones_resumen`(IN `_id_empresa` VARCHAR(36),  IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			o.nombre,
			COUNT(DISTINCT v.id) as cantidad,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM ventas_operaciones as v
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_status=1 
		AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id_tipo;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for reportes_ventas_usuarios_detalle
-- ----------------------------
DROP PROCEDURE IF EXISTS `reportes_ventas_usuarios_detalle`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reportes_ventas_usuarios_detalle`(IN `_id_empresa` VARCHAR(36),  IN `_id_usuario` VARCHAR(36), IN `_desde` DATETIME, IN `_hasta` DATETIME)
    NO SQL
BEGIN
	SELECT 
			hex(v.id_tipo) as id_tipo,
			o.nombre as operacion_nombre,
			v.nro_control,
			v.fecha,
			c.nombre as cliente_nombre,
			c.rif as cliente_rif,
			SUM((m.precio * m.cantidad)*((m.valor_impuesto/100)+1)) as monto
	FROM ventas_operaciones as v
	INNER JOIN sistema_operaciones as o ON v.id_tipo=o.id
	INNER JOIN ventas_clientes as c ON v.id_cliente=c.id
	INNER JOIN inventario_movimientos as m ON m.id_operacion=v.id
	WHERE 
		v.id_empresa = UNHEX(_id_empresa) AND 
		v.id_usuario= UNHEX(_id_usuario) AND 
		v.id_status=1 
		#AND (v.fecha BETWEEN _desde AND _hasta)
	GROUP BY v.id
	ORDER BY v.fecha;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_cotizaciones_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_cotizaciones_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_cotizaciones_INS`(IN `_id` VARCHAR(36), IN `_fecha` DATETIME, IN `_valor` DOUBLE(10,2))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		SET @ahora=NOW();
INSERT INTO sistema_cotizaciones(id, order_id, fecha_registro, fecha, valor) 
VALUES (UNHEX(@my_uuid), @order_id, @ahora, _fecha, _valor);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_cotizaciones_LAST
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_cotizaciones_LAST`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_cotizaciones_LAST`()
    NO SQL
BEGIN
SELECT HEX(c.id) as id, c.order_id, c.fecha_registro, c.fecha, c.valor
FROM sistema_cotizaciones as c  
ORDER BY c.id DESC LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_cotizaciones_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_cotizaciones_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_cotizaciones_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(c.id) as id, c.order_id, c.fecha_registro, c.fecha, c.valor
FROM sistema_cotizaciones as c  
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_cotizaciones_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_cotizaciones_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_cotizaciones_SEL`()
    NO SQL
BEGIN
        SELECT hex(c.id) as id,
                    c.order_id,
										c.fecha_registro,
										c.fecha,
										c.valor
        FROM sistema_cotizaciones as c;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_cotizaciones_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_cotizaciones_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_cotizaciones_UPD`(IN `_id` VARCHAR(36), IN `_fecha` DATETIME, IN `_valor` DOUBLE(10,2))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_cotizaciones as c
SET c.fecha=_fecha, c.valor=_valor
WHERE c.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_empresas_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_empresas_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_empresas_INS`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(255),  IN `_telefono` VARCHAR(24), IN `_es_inactivo` TINYINT(1))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO sistema_empresas(id, order_id, nombre, rif, direccion, telefono, es_inactivo) 
		VALUES (UNHEX(@my_uuid), @order_id, _nombre, _rif, _direccion, _telefono, _es_inactivo);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_empresas_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_empresas_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_empresas_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(e.id) as id, 
	e.order_id, 
	e.nombre,
	e.rif,
	e.direccion,
	e.telefono,
	e.es_inactivo
FROM sistema_empresas as e  
WHERE e.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_empresas_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_empresas_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_empresas_SEL`(IN `_id` TEXT)
    NO SQL
BEGIN
	IF _id = "" THEN 
		SELECT hex(e.id) as id,
								e.order_id,
								e.nombre,
								e.rif,
								e.direccion,
								e.telefono,
								e.es_inactivo
		FROM sistema_empresas as e;
	ELSE 
    SET @x=REPLACE(_id,'[','');
    SET @y=REPLACE(@x, ']' ,'');
        set @sql = concat("SELECT\r\n     hex(e.id) as id,\r\n    e.order_id,\r\n    e.nombre, \r\n   e.rif,\r\n   e.direccion,\r\n  e.telefono,\r\n   e.es_inactivo\r\n  FROM sistema_empresas as e\r\n   WHERE HEX(e.id) in (" , @y , ")");

        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        SELECT @sql;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_empresas_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_empresas_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_empresas_UPD`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(255),  IN `_telefono` VARCHAR(24), IN `_es_inactivo` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_empresas as e
SET e.nombre=_nombre, e.rif=_rif, e.direccion=_direccion, e.telefono=_telefono, e.es_inactivo=_es_inactivo
WHERE e.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_impuestos_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_impuestos_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_impuestos_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE i.* FROM sistema_impuestos as i WHERE i.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_impuestos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_impuestos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_impuestos_INS`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_codigo` VARCHAR(10), IN `_nombre` VARCHAR(100), IN `_valor_display` VARCHAR(100), IN `_valor` DOUBLE)
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO sistema_impuestos(id, order_id, id_tipo, codigo, nombre, valor_display, valor) 
VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_tipo), _codigo, _nombre, _valor_display, _valor);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_impuestos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_impuestos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_impuestos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(i.id) as id, i.order_id, HEX(i.id_tipo) as id_tipo, t.nombre as tipo_nombre, i.codigo, i.nombre, i.valor_display, i.valor 
FROM sistema_impuestos as i  
LEFT JOIN sistema_listas as t ON i.id_tipo = t.id
WHERE i.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_impuestos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_impuestos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_impuestos_SEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
	IF _id = "" THEN 
        SELECT hex(i.id) as id,
                    i.order_id,
                    hex(i.id_tipo) as id_tipo,
                    t.nombre as tipo_nombre,
                                    i.codigo, 
                                    i.nombre,
                                    i.valor_display,
                                    i.valor
        FROM sistema_impuestos as i
        LEFT JOIN sistema_listas as t ON i.id_tipo = t.id;
	ELSE 
        SELECT hex(i.id) as id,
                    i.order_id,
                    hex(i.id_tipo) as id_tipo,
                    t.nombre as tipo_nombre,
                                    i.codigo, 
                                    i.nombre,
                                    i.valor_display,
                                    i.valor
        FROM sistema_impuestos as i
        LEFT JOIN sistema_listas as t ON i.id_tipo = t.id
        WHERE i.id_tipo=UNHEX(_id);

	END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_impuestos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_impuestos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_impuestos_UPD`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_codigo` VARCHAR(10), IN `_nombre` VARCHAR(100), IN `_valor_display` VARCHAR(100), IN `_valor` DOUBLE(6,0))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_impuestos as i
SET i.id_tipo=UNHEX(_id_tipo), i.codigo=_codigo, i.nombre=_nombre, i.valor_display=_valor_display, i.valor=_valor
WHERE id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_listas_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_listas_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_listas_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE l.* FROM sistema_listas as l WHERE l.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_listas_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_listas_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_listas_INS`(IN `_id` VARCHAR(36), IN `_campo` VARCHAR(100), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(255), IN `_id_padre` VARCHAR(36))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO sistema_listas (id, order_id, campo, nombre, descrip, id_padre) VALUES (UNHEX(@my_uuid), @order_id, _campo, _nombre, _descrip,  UNHEX(_id_padre) );

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_listas_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_listas_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_listas_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(l.id) as id, l.order_id, l.campo, l.nombre, l.descrip, HEX(l.id_padre) as id_padre FROM sistema_listas as l WHERE l.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_listas_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_listas_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_listas_SEL`()
    NO SQL
BEGIN
	SELECT hex(l.id) as id,
                l.order_id,
                l.campo, 
                l.nombre,
                l.descrip,
                hex(l.id_padre) as id_padre,
                p.nombre as padre
	FROM sistema_listas as l
   	LEFT JOIN sistema_listas as p ON l.id_padre = p.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_listas_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_listas_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_listas_UPD`(IN `_id` VARCHAR(36), IN `_campo` VARCHAR(100), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(255), IN `_id_padre` VARCHAR(36))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_listas 
SET campo=_campo, nombre=_nombre, descrip=_descrip,id_padre=UNHEX(_id_padre)
WHERE id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_menu_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_menu_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_menu_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE m.* FROM sistema_menu as m WHERE m.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_menu_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_menu_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_menu_INS`(IN `_id` VARCHAR(36), IN `_id_seccion` TINYINT(1), IN `_id_padre` VARCHAR(36), IN `_id_tipo` TINYINT(1), IN `_nombre` VARCHAR(100), IN `_descrip` VARCHAR(255), IN `_trigger` VARCHAR(100), IN `_orden_pos` TINYINT(1), IN `_icono` VARCHAR(100), IN `_tooltip` VARCHAR(255))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
    
INSERT INTO 
sistema_menu (id, order_id, id_seccion, id_padre, id_tipo, nombre, descrip, `trigger`, orden_pos, icono, tooltip) 
VALUES (UNHEX(@my_uuid), @order_id, _id_seccion, UNHEX(_id_padre), _id_tipo, _nombre, _descrip, _trigger, _orden_pos, _icono, _tooltip);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_menu_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_menu_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_menu_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(m.id) as id, m.order_id, m.id_seccion, HEX(m.id_padre) as id_padre, m.id_tipo, m.nombre, m.descrip, m.trigger, m.orden_pos, m.icono, m.tooltip 
FROM sistema_menu as m WHERE m.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_menu_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_menu_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_menu_SEL`(IN `_id_sistema` VARCHAR(36), IN `_id` TEXT)
    NO SQL
BEGIN

	IF _id = "" THEN 
    	SELECT 
        hex(m.id) as id,
                m.order_id,
                m.id_seccion, 
                hex(m.id_padre) as id_padre,
                m.id_tipo,
                m.nombre,
                m.descrip,
                m.trigger, 
                m.orden_pos, 
                m.icono,
                m.tooltip
        FROM sistema_menu as m
				WHERE m.id_sistema = _id_sistema ;
	ELSE 
    SET @x=REPLACE(_id,'[','');
    SET @y=REPLACE(@x, ']' ,'');
        set @sql = concat("SELECT hex(m.id) as id, m.order_id, m.id_seccion, hex(m.id_padre) as id_padre, m.id_tipo, m.nombre, m.descrip, m.trigger, m.orden_pos, m.icono, m.tooltip  FROM sistema_menu as m WHERE m.id_sistema= '",_id_sistema,"' AND HEX(m.id) in (" , @y , ")");
				#set @sql = concat("SELECT\r\n     hex(m.id) as id,\r\n    m.order_id,\r\n    m.id_seccion, \r\n   hex(m.id_padre) as id_padre,\r\n   m.id_tipo,\r\n  m.nombre,\r\n   m.descrip,\r\n  m.trigger, \r\n   m.orden_pos, \r\n    m.icono,\r\n  m.tooltip\r\n   FROM sistema_menu as m\r\n   WHERE HEX(m.id) in (" , @y , ")");

        PREPARE stmt FROM @sql;
        EXECUTE stmt;
        SELECT @sql;
    END IF;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_menu_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_menu_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_menu_UPD`(
    IN `_id` VARCHAR(36), 
    IN `_id_seccion` TINYINT(1), 
    IN `_id_padre` VARCHAR(36),
    IN `_id_tipo` TINYINT(1), 
    IN `_nombre` VARCHAR(100), 
    IN `_descrip` VARCHAR(255), 
    IN `_trigger` VARCHAR(100),
    IN `_orden_pos` TINYINT(1),
    IN `_icono` VARCHAR(100),
    IN `_tooltip` VARCHAR(255) 
)
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_menu as m 
SET m.id_seccion=_id_seccion, m.id_padre=UNHEX(_id_padre), m.nombre=_nombre, m.descrip=_descrip, m.trigger=_trigger, m.orden_pos=_orden_pos, m.icono=_icono, m.tooltip=_tooltip
WHERE m.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_operaciones_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_operaciones_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_operaciones_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE o.* FROM sistema_operaciones as o WHERE o.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_operaciones_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_operaciones_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_operaciones_INS`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_nombre_display` VARCHAR(100), IN `_descrip` VARCHAR(255), IN `_signo_inventario` VARCHAR(1), IN `_signo_caja` VARCHAR(1), IN `_es_fiscal` TINYINT(1), IN `_es_autorizado` TINYINT(1), IN `_es_visible` TINYINT(1), IN `_es_derivado` TINYINT(1), IN `_es_transporte` TINYINT(1))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO sistema_operaciones(id, order_id, id_tipo, nombre, nombre_display, descrip, signo_inventario, signo_caja, es_fiscal, es_autorizado, es_visible, es_derivado, es_transporte) 
VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_tipo), _nombre, _nombre_display, _descrip, _signo_inventario, _signo_caja, _es_fiscal, _es_autorizado, _es_visible, _es_derivado, _es_transporte);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_operaciones_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_operaciones_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_operaciones_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(o.id) as id, o.order_id, HEX(o.id_tipo) as id_tipo, t.nombre as tipo_nombre, o.nombre, o.nombre_display, o.descrip, o.signo_inventario, o.signo_caja, o.es_fiscal, o.es_autorizado, o.es_visible, o.es_derivado, o.es_transporte
FROM sistema_operaciones as o  
LEFT JOIN sistema_listas as t ON o.id_tipo = t.id
WHERE o.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_operaciones_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_operaciones_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_operaciones_SEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
	IF _id = "" THEN 
        SELECT hex(o.id) as id,
                    o.order_id,
                    hex(o.id_tipo) as id_tipo,
                    t.nombre as tipo_nombre,
                                    o.nombre, 
                                    o.nombre_display,
                                    o.descrip,
                                    o.signo_inventario,
                                    o.signo_caja, 
                                    o.es_fiscal,
                                    o.es_autorizado,
                                    o.es_visible,
																		o.es_derivado,
																		o.es_transporte
        FROM sistema_operaciones as o
        LEFT JOIN sistema_listas as t ON o.id_tipo = t.id;
	ELSE 
        SELECT hex(o.id) as id,
                    o.order_id,
                    hex(o.id_tipo) as id_tipo,
                    t.nombre as tipo_nombre,
                                    o.nombre, 
                                    o.nombre_display,
                                    o.descrip,
                                    o.signo_inventario,
                                    o.signo_caja, 
                                    o.es_fiscal,
                                    o.es_autorizado,
                                    o.es_visible,
																		o.es_derivado,
																		o.es_transporte
        FROM sistema_operaciones as o
        LEFT JOIN sistema_listas as t ON o.id_tipo = t.id
        WHERE o.id_tipo=UNHEX(_id);

	END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_operaciones_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_operaciones_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_operaciones_UPD`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_nombre_display` VARCHAR(100), IN `_descrip` VARCHAR(255), IN `_signo_inventario` VARCHAR(1), IN `_signo_caja` VARCHAR(1), IN `_es_fiscal` TINYINT(1), IN `_es_autorizado` TINYINT(1), IN `_es_visible` TINYINT(1), IN `_es_derivado` TINYINT(1), IN `_es_transporte` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_operaciones as o
SET o.id_tipo=UNHEX(_id_tipo), o.nombre=_nombre, o.nombre_display=_nombre_display, o.descrip=_descrip, o.signo_inventario=_signo_inventario, o.signo_caja=_signo_caja, o.es_fiscal=_es_fiscal, o.es_autorizado=_es_autorizado, o.es_visible=_es_visible, o.es_derivado=_es_derivado, o.es_transporte=_es_transporte
WHERE o.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_productos_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_productos_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_productos_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE p.* FROM sistema_productos as p WHERE p.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_productos_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_productos_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_productos_INS`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_codigo` VARCHAR(30), IN `_empresas` VARCHAR(255),  IN `_id_unidad_compra` VARCHAR(36),  IN `_id_unidad_venta` VARCHAR(36),   IN `_cantidad_unidad_compra` DOUBLE(10,2))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO sistema_productos(id, order_id, codigo, nombre, empresas, id_unidad_compra, id_unidad_venta, cantidad_unidad_compra) 
		VALUES (UNHEX(@my_uuid), @order_id, _codigo, _nombre, _empresas, UNHEX(_id_unidad_compra), UNHEX(_id_unidad_venta), _cantidad_unidad_compra);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_productos_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_productos_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_productos_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT 
	HEX(p.id) as id, 
	p.order_id,
	p.codigo,
	p.nombre,
	p.empresas,
	HEX(p.id_unidad_compra) as id_unidad_compra,
	HEX(p.id_unidad_venta) as id_unidad_venta,
	p.cantidad_unidad_compra
FROM sistema_productos as p  
WHERE p.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_productos_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_productos_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_productos_SEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
	IF _id = "" THEN 
		SELECT 
			HEX(p.id) as id, 
			p.order_id,
			p.codigo,
			p.nombre,
			p.empresas,
			HEX(p.id_unidad_compra) as id_unidad_compra,
			uc.nombre as unidad_compra_nombre,
			HEX(p.id_unidad_venta) as id_unidad_venta,
			uv.nombre as unidad_venta_nombre,
			p.cantidad_unidad_compra
		FROM sistema_productos as p
		LEFT JOIN sistema_unidades as uc ON p.id_unidad_compra=uc.id
		LEFT JOIN sistema_unidades as uv ON p.id_unidad_venta=uv.id;
	ELSE 
		SELECT 
			HEX(p.id) as id, 
			p.order_id,
			p.codigo,
			p.nombre,
			p.empresas,
			HEX(p.id_unidad_compra) as id_unidad_compra,
			uc.nombre as unidad_compra_nombre,
			HEX(p.id_unidad_venta) as id_unidad_venta,
			uv.nombre as unidad_venta_nombre,
			p.cantidad_unidad_compra
		FROM sistema_productos as p
		LEFT JOIN sistema_unidades as uc ON p.id_unidad_compra=uc.id
		LEFT JOIN sistema_unidades as uv ON p.id_unidad_venta=uv.id
		WHERE p.empresas LIKE CONCAT('%', _id, '%');
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_productos_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_productos_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_productos_UPD`(IN `_id` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_codigo` VARCHAR(30), IN `_empresas` VARCHAR(255),  IN `_id_unidad_compra` VARCHAR(36),  IN `_id_unidad_venta` VARCHAR(36),   IN `_cantidad_unidad_compra` DOUBLE(10,2))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_productos as p
SET p.codigo=_codigo, p.nombre=_nombre, p.empresas=_empresas, p.id_unidad_compra=UNHEX(_id_unidad_compra), p.id_unidad_venta=UNHEX(_id_unidad_venta), p.cantidad_unidad_compra=cantidad_unidad_compra
WHERE p.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_unidades_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_unidades_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_unidades_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE u.* FROM sistema_unidades as u WHERE u.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_unidades_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_unidades_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_unidades_INS`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_nombre_display` VARCHAR(100), IN `_factor_conversion` DOUBLE(10,6), IN `_es_patron` TINYINT(1))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO sistema_unidades(id, order_id, id_tipo, nombre, nombre_display, factor_conversion, es_patron) 
VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_tipo), _nombre, _nombre_display, _factor_conversion, _es_patron);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_unidades_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_unidades_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_unidades_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(u.id) as id, u.order_id, HEX(u.id_tipo) as id_tipo, t.nombre as tipo_nombre, u.nombre, u.nombre_display, u.factor_conversion, u.es_patron
FROM sistema_unidades as u  
LEFT JOIN sistema_listas as t ON u.id_tipo = t.id
WHERE u.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_unidades_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_unidades_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_unidades_SEL`()
    NO SQL
BEGIN
		SELECT hex(u.id) as id,
								u.order_id,
								hex(u.id_tipo) as id_tipo,
								t.nombre as tipo_nombre,
																u.nombre,
																u.nombre_display,
																u.factor_conversion,
																u.es_patron
		FROM sistema_unidades as u
		LEFT JOIN sistema_listas as t ON u.id_tipo = t.id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_unidades_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_unidades_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_unidades_UPD`(IN `_id` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_nombre` VARCHAR(100), IN `_nombre_display` VARCHAR(100), IN `_factor_conversion` DOUBLE(10,6), IN `_es_patron` TINYINT(1))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE sistema_unidades as u
SET u.id_tipo=UNHEX(_id_tipo), u.nombre=_nombre, u.nombre_display=_nombre_display, u.factor_conversion=_factor_conversion, u.es_patron=_es_patron
WHERE id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_usuarios_DEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_usuarios_DEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_usuarios_DEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
DELETE u.* FROM sistema_usuarios as u WHERE u.id = UNHEX(_id);
SET @x=TRUE;
SELECT @x as resultado;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_usuarios_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_usuarios_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_usuarios_INS`(IN `_id` VARCHAR(36), IN `_id_tipo` TINYINT(1), IN `_nombre` VARCHAR(255), IN `_cedula` VARCHAR(20), IN `_telefono` VARCHAR(24), IN `_email` VARCHAR(100), IN `_logname` VARCHAR(50), IN `_pass` VARCHAR(255), IN `_empresas` TEXT, IN `_permisos` TEXT)
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
INSERT INTO sistema_usuarios (id, order_id, id_tipo, nombre, cedula, telefono, email, logname, pass, permisos) VALUES (UNHEX(@my_uuid), @order_id, _id_tipo, _nombre, _cedula, _telefono, _email, _logname, _pass, _permisos);

SELECT @my_uuid as id;   

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_usuarios_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_usuarios_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_usuarios_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(u.id) as id, u.order_id, u.id_tipo, u.nombre, u.cedula, u.telefono, u.email, u.logname, u.pass, u.permisos, u.empresas FROM sistema_usuarios as u WHERE u.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_usuarios_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_usuarios_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_usuarios_SEL`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN

	IF _id = "" THEN 
    	SELECT 
        hex(u.id) as id,
                u.order_id,
                u.id_tipo,
                u.nombre,
                u.cedula,
                u.telefono, 
                u.email, 
                u.logname,
                u.pass,
								u.permisos,
								u.empresas,
								u.event
        FROM sistema_usuarios as u
				ORDER BY order_id;
ELSE 

    	SELECT 
        hex(u.id) as id,
                u.order_id,
                u.id_tipo,
                u.nombre,
                u.cedula,
                u.telefono, 
                u.email, 
                u.logname,
                u.pass,
								u.permisos,
								u.empresas,
								u.event
        FROM sistema_usuarios as u
				WHERE u.id_tipo <> _id
				ORDER BY order_id;
  END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sistema_usuarios_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `sistema_usuarios_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sistema_usuarios_UPD`(IN `_id` VARCHAR(36), IN `_id_tipo` TINYINT(1), IN `_nombre` VARCHAR(255), IN `_cedula` VARCHAR(20), IN `_telefono` VARCHAR(24), IN `_email` VARCHAR(100), IN `_logname` VARCHAR(50), IN `_pass` VARCHAR(255), IN `_permisos` TEXT, IN `_empresas` TEXT)
BEGIN
  SET @my_uuid=_id;

	UPDATE sistema_usuarios as u
	SET u.id_tipo=_id_tipo, u.nombre=_nombre, u.cedula=_cedula, u.telefono=_telefono, u.email=_email, u.logname=_logname, u.pass=_pass, u.permisos=_permisos, u.empresas=_empresas
	WHERE u.id=UNHEX(_id);

	SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_clientes_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_clientes_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_clientes_INS`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_limite_credito` DOUBLE(10,2), IN `_dias_credito` DOUBLE(10,2), IN `_observacion` VARCHAR(200))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();
		
		INSERT INTO ventas_clientes(id, order_id, id_empresa, nombre, rif, direccion, telefono, email, representante, limite_credito, dias_credito, observacion) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), _nombre, _rif, _direccion, _telefono, _email, _representante, _limite_credito, _dias_credito, _observacion);

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_clientes_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_clientes_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_clientes_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(c.id) as id, c.order_id, HEX(c.id_empresa) as id_empresa, c.nombre, c.rif, c.direccion, c.telefono, c.email, c.representante, c.limite_credito, c.dias_credito, c.observacion
FROM ventas_clientes as c
WHERE c.id = UNHEX(_id) LIMIT 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_clientes_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_clientes_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_clientes_SEL`(IN `_id_empresa` VARCHAR(36))
    NO SQL
BEGIN
        SELECT hex(c.id) as id,
                    c.order_id,
										hex(c.id_empresa) as id_empresa,
										c.nombre,
										c.rif,
										c.direccion,
										c.telefono,
										c.email,
										c.representante,
										c.limite_credito,
										c.dias_credito,
										c.observacion
        FROM ventas_clientes as c
				WHERE c.id_empresa=UNHEX(_id_empresa);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_clientes_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_clientes_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_clientes_UPD`(IN `_id` VARCHAR(36), IN `_id_empresa` VARCHAR(36), IN `_nombre` VARCHAR(200), IN `_rif` VARCHAR(20), IN `_direccion` VARCHAR(200), IN `_telefono` VARCHAR(45), IN `_email` VARCHAR(100), IN `_representante` VARCHAR(200), IN `_limite_credito` DOUBLE(10,2), IN `_dias_credito` DOUBLE(10,2), IN `_observacion` VARCHAR(200))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE ventas_clientes as c
SET c.id_empresa=UNHEX(_id_empresa), c.nombre=_nombre, c.rif=_rif, c.direccion=_direccion, c.telefono=_telefono, c.email=_email, c.representante=_representante, c.limite_credito=_limite_credito, c.dias_credito=_dias_credito, c.observacion=_observacion
WHERE c.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_operaciones_INS
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_operaciones_INS`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_operaciones_INS`(IN `_id` VARCHAR(36),  IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_doc_origen` VARCHAR(36), IN `_id_cliente` VARCHAR(36), IN `_monto_bruto` DOUBLE(10,2), IN `_pct_descuento` DOUBLE(10,2), IN `_pct_adicional` DOUBLE(10,2), IN `_monto_neto` DOUBLE(10,2), IN `_monto_iva` DOUBLE(10,2), IN `_monto_total` DOUBLE(10,2), IN `_id_status` VARCHAR(36), IN `_id_estado` VARCHAR(36),IN `_id_usuario` VARCHAR(36), IN `_observacion` VARCHAR(255),IN `_id_vendedor` VARCHAR(36),IN `_id_vehiculo` VARCHAR(36),IN `_id_chofer` VARCHAR(36))
BEGIN
    SET @new_uuid=uuid();
    SET @my_uuid= CONCAT(SUBSTR(@new_uuid, 15, 4),SUBSTR(@new_uuid, 10, 4),SUBSTR(@new_uuid, 1,8),SUBSTR(@new_uuid, 20, 4),SUBSTR(@new_uuid, 25));
    SET @order_id=UNIX_TIMESTAMP();

		INSERT INTO ventas_operaciones(id, order_id, id_empresa, id_tipo, numero, nro_control, fecha, fecha_registro, id_doc_origen, id_cliente, monto_bruto, pct_descuento, pct_adicional, monto_neto, monto_iva, monto_total, id_status, id_estado, id_usuario, observacion, id_vendedor, id_vehiculo, id_chofer) 
		VALUES (UNHEX(@my_uuid), @order_id, UNHEX(_id_empresa), UNHEX(_id_tipo), _numero, _nro_control, _fecha, _fecha_registro, UNHEX(_id_doc_origen), UNHEX(_id_cliente),_monto_bruto, _pct_descuento, _pct_adicional, _monto_neto, _monto_iva, _monto_total, _id_status, _id_estado, UNHEX(_id_usuario), _observacion, UNHEX(_id_vendedor), UNHEX(_id_vehiculo), UNHEX(_id_chofer));

		SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_operaciones_LAST
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_operaciones_LAST`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_operaciones_LAST`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
        SELECT 	MAX(o.numero) as lastnumero
								FROM ventas_operaciones as o
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_operaciones_ONE
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_operaciones_ONE`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_operaciones_ONE`(IN `_id` VARCHAR(36))
    NO SQL
BEGIN
SELECT HEX(o.id) as id, 
	o.order_id, 
	HEX(o.id_empresa) as id_empresa,
	o.numero, 
	o.nro_control, 
	o.fecha, 
	o.fecha_registro, 
	HEX(o.id_tipo) as id_tipo,
	so.nombre as tipo_operacion_nombre,
	HEX(o.id_doc_origen) as id_doc_origen, 
	HEX(o.id_cliente) as id_cliente, 
	c.nombre as cliente_nombre,
	c.rif as cliente_rif,
	o.monto_bruto,
	o.pct_descuento,
	o.pct_adicional,
	o.monto_neto,
	o.monto_iva,
	o.monto_total,
	o.id_status,
	o.id_estado,
	HEX(o.id_usuario) as id_usuario, 
	u.nombre as usuario_nombre,
	o.observacion,
	HEX(o.id_vendedor) as id_vendedor, 
	HEX(o.id_vehiculo) as id_vehiculo, 
	HEX(o.id_chofer) as id_chofer
FROM ventas_operaciones as o
INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
LEFT JOIN ventas_clientes as c ON o.id_cliente=c.id
LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
WHERE o.id = UNHEX(_id) LIMIT 1;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_operaciones_SEL
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_operaciones_SEL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_operaciones_SEL`(IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36))
    NO SQL
BEGIN
        SELECT hex(o.id) as id,
                    o.order_id,
										hex(o.id_empresa) as id_empresa,
										hex(o.id_tipo) as id_tipo,
										so.nombre as tipo_operacion_nombre,
										o.numero,
										o.nro_control,
										DATE_FORMAT(o.fecha,'%Y-%m-%dT%TZ') as fecha,
										o.fecha_registro,
										hex(o.id_doc_origen) as id_doc_origen,
										hex(o.id_cliente) as id_cliente,
										c.nombre as cliente_nombre,
										c.rif as cliente_rif,
										o.monto_bruto,
										o.pct_descuento,
										o.pct_adicional,
										o.monto_neto,
										o.monto_iva,
										o.monto_total,
										o.id_status,
										o.id_estado,
										hex(o.id_usuario) as id_usuario,
										u.nombre as usuario_nombre,
										o.observacion,
										HEX(o.id_vendedor) as id_vendedor, 
										HEX(o.id_vehiculo) as id_vehiculo, 
										HEX(o.id_chofer) as id_chofer
        FROM ventas_operaciones as o
				INNER JOIN sistema_operaciones as so ON o.id_tipo=so.id
				LEFT JOIN ventas_clientes as c ON o.id_cliente=c.id
				LEFT JOIN sistema_usuarios as u ON o.id_usuario=u.id
				WHERE o.id_empresa=UNHEX(_id_empresa) AND o.id_tipo=UNHEX(_id_tipo)
				ORDER BY o.order_id DESC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for ventas_operaciones_UPD
-- ----------------------------
DROP PROCEDURE IF EXISTS `ventas_operaciones_UPD`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ventas_operaciones_UPD`(IN `_id` VARCHAR(36),  IN `_id_empresa` VARCHAR(36), IN `_id_tipo` VARCHAR(36), IN `_numero` INT(10), IN `_nro_control` VARCHAR(100), IN `_fecha` DATETIME, IN `_fecha_registro` DATETIME, IN `_id_doc_origen` VARCHAR(36), IN `_id_cliente` VARCHAR(36), IN `_monto_bruto` DOUBLE(10,2), IN `_pct_descuento` DOUBLE(10,2), IN `_pct_adicional` DOUBLE(10,2), IN `_monto_neto` DOUBLE(10,2), IN `_monto_iva` DOUBLE(10,2), IN `_monto_total` DOUBLE(10,2), IN `_id_status` VARCHAR(36), IN `_id_estado` VARCHAR(36),IN `_id_usuario` VARCHAR(36), IN `_observacion` VARCHAR(255),IN `_id_vendedor` VARCHAR(36),IN `_id_vehiculo` VARCHAR(36),IN `_id_chofer` VARCHAR(36))
    NO SQL
BEGIN
SET @my_uuid=_id;

UPDATE ventas_operaciones as o
SET 
o.id_empresa= UNHEX(_id_empresa),
o.id_tipo= UNHEX(_id_tipo), 
o.numero= _numero, 
o.nro_control= _nro_control, 
o.fecha= _fecha, 
o.fecha_registro= _fecha_registro,
o.id_doc_origen= UNHEX(_id_doc_origen),
o.id_cliente= UNHEX(_id_cliente),
o.monto_bruto=_monto_bruto,
o.pct_descuento=_pct_descuento,
o.pct_adicional=_pct_adicional,
o.monto_neto=_monto_neto,
o.monto_iva=_monto_iva,
o.monto_total=_monto_total, 
o.id_usuario= UNHEX(_id_usuario), 
o.id_status= _id_status, 
o.observacion= _observacion,
o.id_vendedor= UNHEX(_id_vendedor),
o.id_vehiculo= UNHEX(_id_vehiculo),
o.id_chofer= UNHEX(_id_chofer)
WHERE o.id=UNHEX(_id);

SELECT @my_uuid as id;   
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for ordered_uuid
-- ----------------------------
DROP FUNCTION IF EXISTS `ordered_uuid`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ordered_uuid`(uuid BINARY(36)) RETURNS binary(16)
    DETERMINISTIC
RETURN UNHEX(CONCAT(SUBSTR(uuid, 15, 4),SUBSTR(uuid, 10, 4),SUBSTR(uuid, 1, 8),SUBSTR(uuid, 20, 4),SUBSTR(uuid, 25)))
;;
DELIMITER ;
