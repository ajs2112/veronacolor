(function (){
	angular.module('myApp',[
		'angular-jwt',
	    'ui.router',
	    'oc.lazyLoad',
	    'ct.ui.router.extras',
	    'ngAnimate',
	    'ngMessages',
	    'ngIdle',
	    'ngMaterial',
	    'ui.grid',
	    'ui.grid.selection',
	    'ui.grid.expandable',
	    'ui.grid.autoResize',
		'ui.grid.edit',
		'ui.grid.rowEdit',
		'ui.grid.cellNav',	    	    
		])

	.constant('CONFIG',{
		'appName':'Verona Color',
		/* env:dev *#/
    		'APIURL':'http://localhost/veronacolor/rest/',		
			'URL':'http://localhost/veronacolor/front/',		
    	/* env:dev:end */
    	
    	/* env:prod */
     		'APIURL':'/rest/',
			'URL':'/front/',	
     	/* env:prod:end */
		
		'logUser':{},
		'logEmpresa':{},
		'cotizacion_dolar':0,
	})

	.run(['$rootScope', '$state', 'CONFIG', 'jwtHelper', 'Idle', '$mdDialog', '$mdToast', function ($rootScope, $state, CONFIG, jwtHelper, Idle, $mdDialog, $mdToast) {
		$rootScope.appSeccion="";
		$rootScope.formColor="background-50";
		$rootScope.formBcolor="background-400";
		
		/*********** IDLE *********************/
		function closeWarning(){
			$mdDialog.hide();
		}
		function openWarning(){
          	$mdDialog.show({
	          	templateUrl: 'front/app/common/templates/idle-warning.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        });
		}

        $rootScope.$on('IdleStart', function() {
        	openWarning();
        });        

        $rootScope.$on('IdleEnd', function() {
       		closeWarning();
        });        

        $rootScope.$on('IdleTimeout', function() {
        	closeWarning();
		   	$state.go('login');
        });

        /*
		$rootScope.$on('IdleWarn', function() {
        });*/


        /************** ROUTE CHANGE ************/
		$rootScope.$on('$stateChangeStart', function(event, toState) {
			$rootScope.preloader = false;

		   	if (toState.name === "login") {
		      	return; // already going to login
		   	}

		   	if (toState.name === "signup") {
		      	return; // already going to signup
		   	}

		   	var token=localStorage.getItem("token");

		   	if (!token) {
		   		event.preventDefault();
		   		$state.go('login');

		   	} else {
		        //VERIFICA PERMISOS 
		   	}

		   	var bool = jwtHelper.isTokenExpired(token);
        	if(bool === true){     
        		//console.log('token expirado')
		   		event.preventDefault();        	       
        		$state.go('login');
        	} else {
        		CONFIG.logUser=jwtHelper.decodeToken(token);
        	}
		});

		$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams,CONFIG) {
			$rootScope.preloader = true;

			if (from.name){
				localStorage.setItem("prevState",from.name);
			}
			var obj={	
						sectionName:'nada'
					};
			if (to.params){

				obj=to.params;
			}
			$rootScope.appSeccion=obj.sectionName;
		});

		$rootScope.$state=$state;

		$rootScope.showToast =  function(texto) {
	      $mdToast.show(
	        $mdToast.simple()
	          .textContent(texto)
	          .action('CERRAR')
	          .highlightAction(true)
	          .highlightClass('md-accent')
	          .position('top right')
	      ).then(function(response) {
	          if ( response == 'ok' ) {
	            $mdToast.hide();
	          }
	      });
	    };


	}])

	.filter('optionGridDropdownFilter', function () {
		return function (input, context) {
			if (context.constructor.name!='c'){
		        var fieldLevel = (context.editDropdownOptionsArray === undefined) ? context.col.colDef : context;
		        var map = fieldLevel.editDropdownOptionsArray;
		        var idField = fieldLevel.editDropdownIdLabel;
		        var valueField = fieldLevel.editDropdownValueLabel;
		        var initial = context.row.entity[context.col.field];
		        if (typeof map !== "undefined") {
		            for (var i = 0; i < map.length; i++) {
		                if (map[i][idField] == input) {
		                    return map[i][valueField];
		                }
		            }
		        }
		        else if (initial) {
		            return initial;
		        }
		        return input;
		    }
				
		};
	})

	/* ********* DIRECTIVAS ******************* */
	/* FORMATO NUMERICO */

	.directive('formatoNumerico', ['$filter', function ($filter)  {
	  	var decimalCases = 2,
        whatToSet = function (str) {
          /**
           * TODO:
           * don't allow any non digits character, except decimal seperator character
           */
          return str ? Number(str) : '';
        },
        whatToShow = function (num) {
          return $filter('number')(num, decimalCases);
        };

        return {
          restrict: 'A',
          require: 'ngModel',
          link: function (scope, element, attr, ngModel) {
            ngModel.$parsers.push(whatToSet);
            ngModel.$formatters.push(whatToShow);

            element.bind('blur', function() {
              element.val(whatToShow(ngModel.$modelValue));
            });
            element.bind('focus', function () {
              element.val(ngModel.$modelValue);
            });
          }
        };
	}])
	
	/* ********* FILTRO ******************* */
	/* MULTI FILTRO */
	.filter('multiFiltro', ['$filter', function($filter) {
	  	return function multiFiltro(items, predicates, group) {
	      predicates = predicates.split(' ');
	      if (group){
	        items=$filter('filter')(items, group, true);
	      }

	      angular.forEach(predicates, function(predicate) {
	        items = $filter('filter')(items, predicate.trim());
	      });
	      return items;
	  			
	    };
	}]);

})();
(function (){
	'use strict';
	
	angular.module('myApp')

	.config(["$provide", "$stateProvider","$urlRouterProvider","$stickyStateProvider", "jwtInterceptorProvider", '$httpProvider','IdleProvider', 'TitleProvider','$ocLazyLoadProvider','$mdDateLocaleProvider', '$mdThemingProvider', function($provide, $stateProvider,$urlRouterProvider,$stickyStateProvider,jwtInterceptorProvider,$httpProvider,IdleProvider,TitleProvider,$ocLazyLoadProvider,$mdDateLocaleProvider,$mdThemingProvider){
		

		$mdThemingProvider.theme('verona')
		    .primaryPalette('deep-purple')
		    .accentPalette('amber')
		    .warnPalette('red')
		    .backgroundPalette('grey');
		$mdThemingProvider.setDefaultTheme('verona');

		/************** GRID OPTIONS *************/
		$provide.decorator('GridOptions', [ '$delegate', function($delegate){
		    var gridOptions;
		    gridOptions = angular.copy($delegate);
		    gridOptions.initialize = function(options) {
		      var initOptions;
		      initOptions = $delegate.initialize(options);
		      initOptions.enableColumnMenus = false;
		      initOptions.enableSorting= false;
		      initOptions.showColumnFooter= false;
	  		  initOptions.showGridFooter= false;      
	  		  initOptions.noUnselect=true;
		      initOptions.enableRowSelection= true;
		      initOptions.enableRowHeaderSelection= false;
		      initOptions.enableAutoResizing= true;
		      initOptions.enableexpandAll= false;
		      initOptions.selectionRowHeaderWidth= 50;
		      initOptions.multiSelect= false;
		      initOptions.rowHeight=50;
		      initOptions.enableHorizontalScrollbar=0;
		      initOptions.enableVerticalScrollbar=1;

		      return initOptions;
		    };
		    return gridOptions;
		}]);

		/************** $mdDateLocaleProvider *************/
		$mdDateLocaleProvider.formatDate = function(date) {
			var d = new Date(date),
			    month = '' + (d.getMonth() + 1),
			    day = '' + d.getDate(),
			    year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			var miD=[day, month, year].join('/');
			return miD;
		};
		
        //$ocLazyLoadProvider.config({debug:true});
		/******** IDLE PROVIDER *********/
        IdleProvider.idle(60 * 10);
        IdleProvider.timeout(15);
        TitleProvider.enabled(false);


        /*********** HTTP PROVIDER ***************/
		$httpProvider.defaults.headers.common["X-Requested-With"]="XMLHttpRequest";
		jwtInterceptorProvider.tokenGetter=function(){
			return localStorage.getItem("token");
		};
		$httpProvider.interceptors.push('jwtInterceptor');


        /*********** STATE PROVIDER ***************/
		$urlRouterProvider.otherwise("/login");
		//$stickyStateProvider.enableDebug(true);

		$stateProvider
		/******** LOGIN *******/
	    .state('login', {
	      url: "/login",
	      authorization: false,
	      onEnter: [ '$stateParams', '$state', '$mdDialog', function($stateParams, $state, $mdDialog) {
	        $mdDialog.show({
	        	controller: 'loginController',
	          	controllerAs: 'ctrl',
	          	templateUrl: 'front/app/common/login/login.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        });
	      }],
	      resolve: {
	        loginLoad: ['$ocLazyLoad', function($ocLazyLoad){
	          return $ocLazyLoad.load([
						            {
						              name: "loginModule",
						              files: [
						              			"front/app/common/modules/login.mdl.js",
						              			"front/app/common/services/login.srv.js",
						              			"front/app/common/login/login.ctrl.js"
						              			]
						            },
				  					{
				  						name: 'usuariosModule',
				  						serie:true,
				  						files:[	
						              			"front/app/common/modules/usuarios.mdl.js",
						              			"front/app/common/services/usuarios.srv.js",
											   ]
									}
	          	]

	          );
	        }]
	      }
	    })
	
	    .state('profile', {
	      url: "/profile",
	      authorization: true,
	      onEnter: [ '$stateParams', '$state', '$mdDialog',function($stateParams, $state, $mdDialog) {
	        $mdDialog.show({
	        	controller: 'usuariosDetailController',
	          	controllerAs: 'ctrl',
	          	templateUrl: 'front/app/core/listas/usuarios/usuarios.detail.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        })
	        .then(function(){
		       	$state.go('login');
		   	}, function(){
		       	$state.go('login');
		   	});
	      }],
	      resolve: {
	        signupLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
	          return $ocLazyLoad.load([
			          					{
			          						name: 'usuariosModule',
			          						serie:true,
			          						files:[	
							              			"front/app/common/modules/usuarios.mdl.js",
							              			"front/app/common/services/usuarios.srv.js",
												    "front/app/core/listas/usuarios/usuarios.detail.ctrl.js"
												   ]
										}
									]
	          );
	        }]
	      }
	    })	    


	    /************ CONFIG ******************/
	   	.state('config',{
			abstract:true,
			url:"/config",
			templateUrl:"front/app/common/config/config.tpl.html",
			controller:"configController",
			controllerAs:"ctrl",
			resolve:{
		        menuLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
			          						{
			          							name: 'configModule',
				          						serie:true,
				          						files:[	"front/app/common/config/config.mdl.js",
												    	"front/app/common/config/config.ctrl.js"]
					          				}

					]);
		        }]
			}
		})		


		.state('config.listas-menu-lista',{
			url:'menu',
			templateUrl:'front/app/core/listas/menu/menu.lista.tpl.html',
			controller:"menuListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Configuración de Menú'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'menuModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/menu.mdl.js",
											    	"front/app/common/services/menu.srv.js",
				          							"front/app/core/listas/menu/menu.lista.ctrl.js",			    
													"front/app/core/listas/menu/menu.detail.ctrl.js"
													]
											}		              	
					]);
		        }]
			}
		})

		.state('config.listas-operaciones-lista',{
			url:'operaciones',
			templateUrl:'front/app/core/listas/operaciones/operaciones.lista.tpl.html',
			controller:"operacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Configuración de operaciones'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		          	
				          					{
				          						name: 'menuModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/operaciones.mdl.js",
											    	"front/app/common/services/operaciones.srv.js",
				          							"front/app/core/listas/operaciones/operaciones.lista.ctrl.js",			    
													"front/app/core/listas/operaciones/operaciones.detail.ctrl.js"
													]
											}		              	
					]);
		        }]
			}
		})

	    /************ APP ******************/
	   	.state('menu',{
			abstract:true,
			url:"/",
			templateUrl:"front/app/common/menu/menu.tpl.html",
			controller:"menuController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Verona Color'
	        },			
			resolve:{
		        menuLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
			          						{
			          							name: 'menuModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/menu.mdl.js",
												    	"front/app/common/services/menu.srv.js",
												    	"front/app/common/menu/menu.ctrl.js"]
					          				},
			          						{
			          							name: 'usuariosModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/usuarios.mdl.js",
												    	"front/app/common/services/usuarios.srv.js",												    	
														]
					          				},
					          				

					]);
		        }]
			}
		})		
		/******** DASHBOARD *******/
	    .state('menu.dashboard', {
	      	url: "inicio",
			templateUrl:'front/app/common/dashboard/dashboard.tpl.html',
			controller:"dashboardController",
			controllerAs:"ctrl",			
			params: {
	            sectionName: 'Verona'
	        },						
	      	resolve: {
		        loginLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
  					{
  						name: 'cotizacionesModule',
  						serie:true,
  						files:[	
		              			"front/app/common/modules/cotizaciones.mdl.js",
		              			"front/app/common/services/cotizaciones.srv.js",
							   ]
					},
		            {
		              	name: "dashboardModule",
  						serie:true,		              
		              	files: [
								"front/app/common/modules/dashboard.mdl.js",
						    	"front/app/common/services/dashboard.srv.js",
				              	"front/app/common/dashboard/dashboard.ctrl.js"
		              	]
		            }

		          ]);
	        	}]
	      	}
	    })

		/*****************************************************/	    	    
	    /**************** INVENTARIO ************************/	    
	    /*****************************************************/

		/******** CATEGORIAS *******/	    	    
		.state('menu.inventario-categorias-lista',{
			url:'categorias',
			templateUrl:'front/app/core/inventario/categorias/categorias.lista.tpl.html',
			controller:"categoriasListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Categorias'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'categoriasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/categorias.mdl.js",
								              			"front/app/common/services/categorias.srv.js",
													    "front/app/core/inventario/categorias/categorias.lista.ctrl.js",
													    "front/app/core/inventario/categorias/categorias.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/******** ALMACENES *******/		
		.state('menu.inventario-almacenes-lista',{
			url:'almacenes',
			templateUrl:'front/app/core/inventario/almacenes/almacenes.lista.tpl.html',
			controller:"almacenesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Almacenes'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'almacenesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/almacenes.mdl.js",
								              			"front/app/common/services/almacenes.srv.js",
													    "front/app/core/inventario/almacenes/almacenes.lista.ctrl.js",
													    "front/app/core/inventario/almacenes/almacenes.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})


		/******** PRODUCTOS MAIN *******/
		.state('menu.inventario-productos-lista',{
			url:'productos',
			templateUrl:'front/app/core/inventario/productos/productos.lista.tpl.html',
			controller:"productosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Productos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													   ]
											},
				          					{
				          						name: 'categoriasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/categorias.mdl.js",
								              			"front/app/common/services/categorias.srv.js",
													   ]
											},
				          					{
				          						name: 'impuestosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/impuestos.mdl.js",
								              			"front/app/common/services/impuestos.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
								              			"front/app/core/inventario/productos/productos.lista.ctrl.js",
													   ]
											},																																	
					]);
		        }]
			}
		})
		/******** PRODUCTOS DETAIL *******/
		.state('config.inventario-productos-detail',{
			url:'producto',
			templateUrl:'front/app/core/inventario/productos/productos.detail.tpl.html',
			controller:"productosDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Producto',
	            listaTipos: null,
	            listaCategorias: null,
	            listaSistemas: null,
	            listaUnidades: null,
	            listaImpuestos: null,
	            listaProductos: null
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							{
				          						name: 'atributosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/atributos.mdl.js",
								              			"front/app/common/services/atributos.srv.js",
													   ]
											},
		          							{
				          						name: 'existenciasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/existencias.mdl.js",
								              			"front/app/common/services/existencias.srv.js",
													   ]
											},
		          							{
				          						name: 'formulasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/formulas.mdl.js",
								              			"front/app/common/services/formulas.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
								              			"front/app/core/inventario/productos/productos.detail.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.costos.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.atributos.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.formulas.ctrl.js",
													   ]
											},																																	
					]);
		        }]
			}
		})		



		/******** OPERACIONES MAIN*******/	    	    
		.state('menu.inventario-operaciones-lista',{
			url:'invoperaciones',
			templateUrl:'front/app/core/inventario/operaciones/invoperaciones.lista.tpl.html',
			controller:"invoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Inventario'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'operacionesModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/operaciones.mdl.js",
											    	"front/app/common/services/operaciones.srv.js",
													]
											},
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'invoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/invoperaciones.mdl.js",
								              			"front/app/common/services/invoperaciones.srv.js",
													    "front/app/core/inventario/operaciones/invoperaciones.lista.ctrl.js",
													    //"front/app/core/movimientos/cargosdescargos/cargosdescargos.detail.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/******** OPERACIONES DETAIL*******/	    	    
		.state('config.inventario-operaciones-detail',{
			url:'invoperaciones-detalle',
			templateUrl:'front/app/core/inventario/operaciones/invoperaciones.detail.tpl.html',
			controller:"invoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'DETALLE',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
													   ]
											},
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													   ]
											},											
				          					{
				          						name: 'almacenesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/almacenes.mdl.js",
								              			"front/app/common/services/almacenes.srv.js",
													   ]
											},
		          							{
				          						name: 'formulasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/formulas.mdl.js",
								              			"front/app/common/services/formulas.srv.js",
													   ]
											},																										          					
				          					{
				          						name: 'movimientosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/movimientos.mdl.js",
								              			"front/app/common/services/movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													   ]
											},

				          					{
				          						name: 'invoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/invoperaciones.mdl.js",
								              			"front/app/common/services/invoperaciones.srv.js",
													    "front/app/core/inventario/operaciones/invoperaciones.detail.ctrl.js",
													    "front/app/core/inventario/operaciones/invoperaciones.modal.carga.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})



		/******** ORDEN DE PRODUCCION MAIN*******/	    	    
		.state('menu.inventario-produccion-lista',{
			url:'invproduccion',
			templateUrl:'front/app/core/inventario/produccion/invproduccion.lista.tpl.html',
			controller:"invproduccionListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Orden de Produccion'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'operacionesModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/operaciones.mdl.js",
											    	"front/app/common/services/operaciones.srv.js",
													]
											},
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'invproduccionModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/invproduccion.mdl.js",
								              			"front/app/common/services/invproduccion.srv.js",
													    "front/app/core/inventario/produccion/invproduccion.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/******** OPERACIONES DETAIL*******/	    	    
		.state('config.inventario-produccion-detail',{
			url:'invproduccion-detalle',
			templateUrl:'front/app/core/inventario/produccion/invproduccion.detail.tpl.html',
			controller:"invproduccionDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Orden de Producción',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
													   ]
											},
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													   ]
											},											
				          					{
				          						name: 'almacenesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/almacenes.mdl.js",
								              			"front/app/common/services/almacenes.srv.js",
													   ]
											},															          					
				          					{
				          						name: 'movimientosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/movimientos.mdl.js",
								              			"front/app/common/services/movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													   ]
											},

				          					{
				          						name: 'invproduccionModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/invproduccion.mdl.js",
								              			"front/app/common/services/invproduccion.srv.js",
													    "front/app/core/inventario/produccion/invproduccion.detail.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

	    /*****************************************************/	    	    
	    /**************** LISTAS ************************/	    
	    /*****************************************************/

		.state('menu.listas-usuarios-lista',{
			url:'usuarios',
			templateUrl:'front/app/core/listas/usuarios/usuarios.lista.tpl.html',
			controller:"usuariosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Usuarios'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'usuariosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/usuarios.mdl.js",
								              			"front/app/common/services/usuarios.srv.js",
													    "front/app/core/listas/usuarios/usuarios.lista.ctrl.js",
													    "front/app/core/listas/usuarios/usuarios.permisos.ctrl.js",													    			    
													    "front/app/core/listas/usuarios/usuarios.detail.ctrl.js",
													    "front/app/core/listas/usuarios/usuarios.signup.ctrl.js"
													   ]
											},
											{
			          							name: 'menuModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/menu.mdl.js",
												    	"front/app/common/services/menu.srv.js",
													   ]											
											}		              	
					]);
		        }]
			}
		})
	   
		.state('menu.listas-listas-lista',{
			url:'listas',
			templateUrl:'front/app/core/listas/listas/listas.lista.tpl.html',
			controller:"listasListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'listas'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													    "front/app/core/listas/listas/listas.lista.ctrl.js",
													    "front/app/core/listas/listas/listas.detail.ctrl.js",
													   ]
											},

					]);
		        }]
			}
		})

		.state('menu.listas-impuestos-lista',{
			url:'impuestos',
			templateUrl:'front/app/core/listas/impuestos/impuestos.lista.tpl.html',
			controller:"impuestosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Impuestos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'impuestosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/impuestos.mdl.js",
								              			"front/app/common/services/impuestos.srv.js",
													    "front/app/core/listas/impuestos/impuestos.lista.ctrl.js",
													    "front/app/core/listas/impuestos/impuestos.detail.ctrl.js",
													   ]
											}

					]);
		        }]
			}
		})

		.state('menu.listas-cotizaciones-lista',{
			url:'Cotizaciones',
			templateUrl:'front/app/core/listas/cotizaciones/cotizaciones.lista.tpl.html',
			controller:"cotizacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'cotizaciones'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'cotizacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/cotizaciones.mdl.js",
								              			"front/app/common/services/cotizaciones.srv.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.lista.ctrl.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.detail.ctrl.js",
													   ]
											}

					]);
		        }]
			}
		})		
	   

		/******** UNIDADES *******/		
		.state('menu.listas-unidades-lista',{
			url:'unidades',
			templateUrl:'front/app/core/inventario/unidades/unidades.lista.tpl.html',
			controller:"unidadesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Unidades'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													    "front/app/core/inventario/unidades/unidades.lista.ctrl.js",
													    "front/app/core/inventario/unidades/unidades.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})	    


	    /*****************************************************/	    	    
	    /**************** COMPRAS ************************/	    
	    /*****************************************************/

		/******** PROVEEDORES *******/		
		.state('menu.compras-proveedores-lista',{
			url:'proveedores',
			templateUrl:'front/app/core/compras/proveedores/proveedores.lista.tpl.html',
			controller:"proveedoresListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Proveedores'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'proveedoresModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/proveedores.mdl.js",
								              			"front/app/common/services/proveedores.srv.js",
													    "front/app/core/compras/proveedores/proveedores.lista.ctrl.js",
													    "front/app/core/compras/proveedores/proveedores.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/******** COMPRA OPERACIONES MAIN*******/	    	    
		.state('menu.compras-operaciones-lista',{
			url:'cmpoperaciones',
			templateUrl:'front/app/core/compras/operaciones/cmpoperaciones.lista.tpl.html',
			controller:"cmpoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Compra'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'operacionesModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/operaciones.mdl.js",
											    	"front/app/common/services/operaciones.srv.js",
													]
											},
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'cmpoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/cmpoperaciones.mdl.js",
								              			"front/app/common/services/cmpoperaciones.srv.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/******** OPERACIONES DETAIL*******/	    	    
		.state('config.compras-operaciones-detail',{
			url:'cmpoperaciones-detalle',
			templateUrl:'front/app/core/compras/operaciones/cmpoperaciones.detail.tpl.html',
			controller:"cmpoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operación de Compra',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
													   ]
											},
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													   ]
											},											
				          					{
				          						name: 'almacenesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/almacenes.mdl.js",
								              			"front/app/common/services/almacenes.srv.js",
													   ]
											},
				          					{
				          						name: 'proveedoresModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/proveedores.mdl.js",
								              			"front/app/common/services/proveedores.srv.js",
													    "front/app/core/compras/proveedores/proveedores.detail.ctrl.js",
													   ]
											},																										          					
				          					{
				          						name: 'movimientosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/movimientos.mdl.js",
								              			"front/app/common/services/movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													   ]
											},

				          					{
				          						name: 'cmpoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/cmpoperaciones.mdl.js",
								              			"front/app/common/services/cmpoperaciones.srv.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.detail.ctrl.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.modal.carga.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})



	    /*****************************************************/	    	    
	    /**************** VENTAS ************************/	    
	    /*****************************************************/

		/******** CLIENTES *******/		
		.state('menu.ventas-clientes-lista',{
			url:'clientes',
			templateUrl:'front/app/core/ventas/clientes/clientes.lista.tpl.html',
			controller:"clientesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Clientes'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'clientesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/clientes.mdl.js",
								              			"front/app/common/services/clientes.srv.js",
													    "front/app/core/ventas/clientes/clientes.lista.ctrl.js",
													    "front/app/core/ventas/clientes/clientes.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/******** VENTAS OPERACIONES MAIN*******/	    	    
		.state('menu.ventas-operaciones-lista',{
			url:'vntoperaciones',
			templateUrl:'front/app/core/ventas/operaciones/vntoperaciones.lista.tpl.html',
			controller:"vntoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Venta'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'operacionesModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/operaciones.mdl.js",
											    	"front/app/common/services/operaciones.srv.js",
													]
											},
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'vntoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/vntoperaciones.mdl.js",
								              			"front/app/common/services/vntoperaciones.srv.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/******** OPERACIONES DETAIL*******/	    	    
		.state('config.ventas-operaciones-detail',{
			url:'vntoperaciones-detalle',
			templateUrl:'front/app/core/ventas/operaciones/vntoperaciones.detail.tpl.html',
			controller:"vntoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operación de Venta',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/listas.srv.js",
													   ]
											},
				          					{
				          						name: 'productosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/productos.mdl.js",
								              			"front/app/common/services/productos.srv.js",
													   ]
											},
				          					{
				          						name: 'unidadesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/unidades.mdl.js",
								              			"front/app/common/services/unidades.srv.js",
													   ]
											},											
				          					{
				          						name: 'almacenesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/almacenes.mdl.js",
								              			"front/app/common/services/almacenes.srv.js",
													   ]
											},
				          					{
				          						name: 'clientesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/clientes.mdl.js",
								              			"front/app/common/services/clientes.srv.js",
													    "front/app/core/ventas/clientes/clientes.detail.ctrl.js",
													   ]
											},																										          					
				          					{
				          						name: 'movimientosModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/movimientos.mdl.js",
								              			"front/app/common/services/movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													   ]
											},

				          					{
				          						name: 'vntoperacionesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/vntoperaciones.mdl.js",
								              			"front/app/common/services/vntoperaciones.srv.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.detail.ctrl.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.modal.carga.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})


	    /*****************************************************/	    	    
	    /**************** REPORTES ************************/	    
	    /*****************************************************/

		/******** INVENTARIO *******/		
		.state('menu.reportes-inventario-lista',{
			url:'rep-inventario',
			templateUrl:'front/app/core/reportes/inventario/inventario.reportes.tpl.html',
			controller:"inventarioReportesController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Reportes de Inventario'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'listasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/listas.mdl.js",
												    	"front/app/common/services/listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'reportesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/reportes.mdl.js",
								              			"front/app/common/services/repInventarios.srv.js",
													    "front/app/core/reportes/inventario/inventario.reportes.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})



	}]);

})();