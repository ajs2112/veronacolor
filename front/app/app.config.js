(function (){
	'use strict';
	
	angular.module('myApp')

	.config(["$provide", "$stateProvider","$urlRouterProvider","$stickyStateProvider", "jwtInterceptorProvider", '$httpProvider','IdleProvider', 'TitleProvider','$ocLazyLoadProvider','$mdDateLocaleProvider', '$mdThemingProvider', function($provide, $stateProvider,$urlRouterProvider,$stickyStateProvider,jwtInterceptorProvider,$httpProvider,IdleProvider,TitleProvider,$ocLazyLoadProvider,$mdDateLocaleProvider,$mdThemingProvider){
		

		$mdThemingProvider.theme('verona')
		    .primaryPalette('deep-purple')
		    .accentPalette('amber')
		    .warnPalette('red')
		    .backgroundPalette('grey');
		$mdThemingProvider.setDefaultTheme('verona');

		/************** GRID OPTIONS *************/
		$provide.decorator('GridOptions', [ '$delegate', function($delegate){
		    var gridOptions;
		    gridOptions = angular.copy($delegate);
		    gridOptions.initialize = function(options) {
		      var initOptions;
		      initOptions = $delegate.initialize(options);
		      initOptions.enableColumnMenus = false;
		      initOptions.enableSorting= false;
		      initOptions.showColumnFooter= false;
	  		  initOptions.showGridFooter= false;      
	  		  initOptions.noUnselect=true;
		      initOptions.enableRowSelection= true;
		      initOptions.enableRowHeaderSelection= false;
		      initOptions.enableAutoResizing= true;
		      initOptions.enableexpandAll= false;
		      initOptions.selectionRowHeaderWidth= 50;
		      initOptions.multiSelect= false;
		      initOptions.rowHeight=50;
		      initOptions.enableHorizontalScrollbar=0;
		      initOptions.enableVerticalScrollbar=1;

		      return initOptions;
		    };
		    return gridOptions;
		}]);

		/************** $mdDateLocaleProvider *************/
		$mdDateLocaleProvider.formatDate = function(date) {
			var d = new Date(date),
			    month = '' + (d.getMonth() + 1),
			    day = '' + d.getDate(),
			    year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			var miD=[day, month, year].join('/');
			return miD;
		};
		
        //$ocLazyLoadProvider.config({debug:true});
		/******** IDLE PROVIDER *********/
        IdleProvider.idle(60 * 10);
        IdleProvider.timeout(15);
        TitleProvider.enabled(false);


        /*********** HTTP PROVIDER ***************/
		$httpProvider.defaults.headers.common["X-Requested-With"]="XMLHttpRequest";
		jwtInterceptorProvider.tokenGetter=function(){
			return localStorage.getItem("token");
		};
		$httpProvider.interceptors.push('jwtInterceptor');


        /*********** STATE PROVIDER ***************/
		$urlRouterProvider.otherwise("/login");
		//$stickyStateProvider.enableDebug(true);

		$stateProvider
		/******** LOGIN *******/
	    .state('login', {
	      url: "/login",
	      authorization: false,
	      onEnter: [ '$stateParams', '$state', '$mdDialog', function($stateParams, $state, $mdDialog) {
	        $mdDialog.show({
	        	controller: 'loginController',
	          	controllerAs: 'ctrl',
	          	templateUrl: 'front/app/common/login/login.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        });
	      }],
	      resolve: {
	        loginLoad: ['$ocLazyLoad', function($ocLazyLoad){
	          return $ocLazyLoad.load([
		          							{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/lst-cotizaciones.srv.js",
													   ]
											},	          	
								            {
								              name: "sistemaModule",
								              serie:true,
								              files: [
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-login.srv.js",
								              			"front/app/common/services/sis-usuarios.srv.js",
								              			"front/app/common/login/login.ctrl.js"
								              			]
								            },
	          	]

	          );
	        }]
	      }
	    })
	
	    .state('profile', {
	      url: "/profile",
	      authorization: true,
	      onEnter: [ '$stateParams', '$state', '$mdDialog',function($stateParams, $state, $mdDialog) {
	        $mdDialog.show({
	        	controller: 'usuariosDetailController',
	          	controllerAs: 'ctrl',
	          	templateUrl: 'front/app/core/listas/usuarios/usuarios.detail.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        })
	        .then(function(){
		       	$state.go('login');
		   	}, function(){
		       	$state.go('login');
		   	});
	      }],
	      resolve: {
	        signupLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
	          return $ocLazyLoad.load([
			          					{
			          						name: 'sistemaModule',
			          						serie:true,
			          						files:[	
							              			"front/app/common/modules/sistema.mdl.js",
							              			"front/app/common/services/sis-usuarios.srv.js",
												    "front/app/core/listas/usuarios/usuarios.detail.ctrl.js"
												   ]
										}
									]
	          );
	        }]
	      }
	    })	    


	    /*************************************************** CONFIG CONTAINER **********************************************************/
	   	.state('config',{
			abstract:true,
			url:"/config",
			templateUrl:"front/app/common/config/config.tpl.html",
			controller:"configController",
			controllerAs:"ctrl",
			resolve:{
		        menuLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
			          						{
			          							name: 'configModule',
				          						serie:true,
				          						files:[	"front/app/common/config/config.mdl.js",
												    	"front/app/common/config/config.ctrl.js"]
					          				}

					]);
		        }]
			}
		})		


		.state('config.listas-menu-lista',{
			url:'menu',
			templateUrl:'front/app/core/listas/menu/menu.lista.tpl.html',
			controller:"menuListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Configuración de Menú'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'menuModule',
				          						serie:true,
				          						files:[	
			          								"front/app/common/modules/sistema.mdl.js",
											    	"front/app/common/services/sis-menu.srv.js",
				          							"front/app/core/listas/menu/menu.lista.ctrl.js",			    
													"front/app/core/listas/menu/menu.detail.ctrl.js"
													]
											}		              	
					]);
		        }]
			}
		})

		.state('config.listas-operaciones-lista',{
			url:'operaciones',
			templateUrl:'front/app/core/listas/operaciones/operaciones.lista.tpl.html',
			controller:"operacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Configuración de operaciones'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
					          							"front/app/core/listas/operaciones/operaciones.lista.ctrl.js",			    
														"front/app/core/listas/operaciones/operaciones.detail.ctrl.js"
													   ]											
											},		          	

					]);
		        }]
			}
		})

	    /*************************************************** APP MENU CONTAINER **********************************************************/
	   	.state('menu',{
			abstract:true,
			url:"/",
			templateUrl:"front/app/common/menu/menu.tpl.html",
			controller:"menuController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Verona Color'
	        },			
			resolve:{
		        menuLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/lst-cotizaciones.srv.js",
													   ]
											},		          	
			          						{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-menu.srv.js",
												    	"front/app/common/services/sis-usuarios.srv.js",												    	
												    	"front/app/common/menu/menu.ctrl.js"]
					          				},
					]);
		        }]
			}
		})		
		
		/*************************************************** DASHBOARD **********************************************************/
	    .state('menu.dashboard', {
	      	url: "inicio",
			templateUrl:'front/app/common/dashboard/dashboard.tpl.html',
			controller:"dashboardController",
			controllerAs:"ctrl",			
			params: {
	            sectionName: 'Verona'
	        },						
	      	resolve: {
		        loginLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
  					{
  						name: 'sistemaModule',
  						serie:true,
  						files:[	
		              			"front/app/common/modules/sistema.mdl.js",
		              			"front/app/common/services/sis-cotizaciones.srv.js",
		              			"front/app/common/services/sis-empresas.srv.js",
						    	"front/app/common/services/sis-dashboard.srv.js",
				              	"front/app/common/dashboard/dashboard.ctrl.js"
							   ]
					},
		          ]);
	        	}]
	      	}
	    })

		/****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /*************************************************** INVENTARIO *************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/

		/*************************************************** CATEGORIAS **********************************************************/
		.state('menu.inventario-categorias-lista',{
			url:'categorias',
			templateUrl:'front/app/core/inventario/categorias/categorias.lista.tpl.html',
			controller:"categoriasListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Categorias'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-categorias.srv.js",
													    "front/app/core/inventario/categorias/categorias.lista.ctrl.js",
													    "front/app/core/inventario/categorias/categorias.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/*************************************************** ALMACENES **********************************************************/
		.state('menu.inventario-almacenes-lista',{
			url:'almacenes',
			templateUrl:'front/app/core/inventario/almacenes/almacenes.lista.tpl.html',
			controller:"almacenesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Almacenes'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-almacenes.srv.js",
													    "front/app/core/inventario/almacenes/almacenes.lista.ctrl.js",
													    "front/app/core/inventario/almacenes/almacenes.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})


		/*************************************************** PRODUCTOS MAIN **********************************************************/
		.state('menu.inventario-productos-lista',{
			url:'productos',
			templateUrl:'front/app/core/inventario/productos/productos.lista.tpl.html',
			controller:"productosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Productos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-impuestos.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-categorias.srv.js",
								              			"front/app/common/services/inv-productos.srv.js",
								              			"front/app/core/inventario/productos/productos.lista.ctrl.js",
													   ]
											},
																																	
					]);
		        }]
			}
		})
		/*************************************************** PRODUCTOS DETAIL **********************************************************/
		.state('config.inventario-productos-detail',{
			url:'producto',
			templateUrl:'front/app/core/inventario/productos/productos.detail.tpl.html',
			controller:"productosDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Producto',
	            listaTipos: null,
	            listaCategorias: null,
	            listaSistemas: null,
	            listaUnidades: null,
	            listaImpuestos: null,
	            listaProductos: null
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							/*{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-productos.srv.js",
													   ]											
											},*/	

											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-productos.srv.js",
												    	"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
												    	"front/app/common/services/sis-empresas.srv.js",
													    "front/app/core/listas/productos/productos.detail.ctrl.js",
													   ]											
											},	
		          							{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-atributos.srv.js",
								              			"front/app/common/services/inv-existencias.srv.js",
								              			"front/app/common/services/inv-formulas.srv.js",
								              			"front/app/common/services/inv-productos.srv.js",
								              			"front/app/core/inventario/productos/productos.detail.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.costos.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.atributos.ctrl.js",
								              			"front/app/core/inventario/productos/productos.modal.formulas.ctrl.js",
													   ]
											},
																																	
					]);
		        }]
			}
		})		



		/*************************************************** INVENTARIO OPERACIONES MAIN **********************************************************/
		.state('menu.inventario-operaciones-lista',{
			url:'invoperaciones',
			templateUrl:'front/app/core/inventario/operaciones/invoperaciones.lista.tpl.html',
			controller:"invoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Inventario'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
									              		"front/app/common/services/sis-listas.srv.js",
													]
											},
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-operaciones.srv.js",
													    "front/app/core/inventario/operaciones/invoperaciones.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/*************************************************** INVENTARIO OPERACIONES DETAIL **********************************************************/
		.state('config.inventario-operaciones-detail',{
			url:'invoperaciones-detalle',
			templateUrl:'front/app/core/inventario/operaciones/invoperaciones.detail.tpl.html',
			controller:"invoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'DETALLE',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
													   ]
											},
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-productos.srv.js",
								              			"front/app/common/services/inv-almacenes.srv.js",
								              			"front/app/common/services/inv-formulas.srv.js",
								              			"front/app/common/services/inv-movimientos.srv.js",
								              			"front/app/common/services/inv-operaciones.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													    "front/app/core/inventario/operaciones/invoperaciones.detail.ctrl.js",
													    "front/app/core/inventario/operaciones/invoperaciones.modal.carga.ctrl.js",
													   ]
											},

					]);
		        }]
			}
		})



		

	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /****************************************************** LISTAS **************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/

		
		/****************************************************** USUARIOS **************************************************************/
		.state('menu.listas-usuarios-lista',{
			url:'usuarios',
			templateUrl:'front/app/core/listas/usuarios/usuarios.lista.tpl.html',
			controller:"usuariosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Usuarios'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-usuarios.srv.js",
												    	"front/app/common/services/sis-menu.srv.js",
												    	"front/app/common/services/sis-empresas.srv.js",
													    "front/app/core/listas/usuarios/usuarios.lista.ctrl.js",
													    "front/app/core/listas/usuarios/usuarios.permisos.ctrl.js",													    			    
													    "front/app/core/listas/usuarios/usuarios.detail.ctrl.js",
													    "front/app/core/listas/usuarios/usuarios.signup.ctrl.js"
													   ]
											},

					]);
		        }]
			}
		})
	   
	   /****************************************************** LISTAS **************************************************************/
		.state('menu.listas-listas-lista',{
			url:'listas',
			templateUrl:'front/app/core/listas/listas/listas.lista.tpl.html',
			controller:"listasListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'listas'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-listas.srv.js",
													    "front/app/core/listas/listas/listas.lista.ctrl.js",
													    "front/app/core/listas/listas/listas.detail.ctrl.js",
													   ]
											},

					]);
		        }]
			}
		})

		/****************************************************** IMPUESTOS **************************************************************/
		.state('menu.listas-impuestos-lista',{
			url:'impuestos',
			templateUrl:'front/app/core/listas/impuestos/impuestos.lista.tpl.html',
			controller:"impuestosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Impuestos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-impuestos.srv.js",
													    "front/app/core/listas/impuestos/impuestos.lista.ctrl.js",
													    "front/app/core/listas/impuestos/impuestos.detail.ctrl.js",
													   ]											
											},		              	


					]);
		        }]
			}
		})

		/****************************************************** COTIZACIONES **************************************************************/
		/*.state('menu.listas-cotizaciones-lista',{
			url:'Cotizaciones',
			templateUrl:'front/app/core/listas/cotizaciones/cotizaciones.lista.tpl.html',
			controller:"cotizacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Cotizaciones'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-cotizaciones.srv.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.lista.ctrl.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.detail.ctrl.js",
													   ]
											}

					]);
		        }]
			}
		})*/	

		/***** COTIZACIONES ********************************************************************************************/	    	    
		.state('menu.listas-cotizaciones-list',{
			url:'Cotizaciones',
			templateUrl:'front/app/core/listas/cotizaciones/cotizaciones.list.tpl.html',
			controller:"cotizacionesListController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Cotizaciones'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]
											},	
				          					{
				          						name: 'listasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/listas.mdl.js",
								              			"front/app/common/services/lst-cotizaciones.srv.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.list.ctrl.js",
													    "front/app/core/listas/cotizaciones/cotizaciones.detail.ctrl.js",
													   ]
											}

					]);
		        }]
			}
		})	
	   

		/****************************************************** UNIDADES **************************************************************/
		.state('menu.listas-unidades-lista',{
			url:'unidades',
			templateUrl:'front/app/core/inventario/unidades/unidades.lista.tpl.html',
			controller:"unidadesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Unidades'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
													   ]											
											},
											{
			          							name: 'inventarioModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/inventario.mdl.js",
													    "front/app/core/inventario/unidades/unidades.lista.ctrl.js",
													    "front/app/core/inventario/unidades/unidades.detail.ctrl.js",
													   ]											
											},		              	


					]);
		        }]
			}
		})	    

		


		/****************************************************** EMPRESAS **************************************************************/
		.state('menu.listas-empresas-lista',{
			url:'empresas',
			templateUrl:'front/app/core/listas/empresas/empresas.lista.tpl.html',
			controller:"empresasListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Empresas'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'empresasModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-empresas.srv.js",
													    "front/app/core/listas/empresas/empresas.lista.ctrl.js",
													    "front/app/core/listas/empresas/empresas.detail.ctrl.js",
													   ]											
											},		              	

					]);
		        }]
			}
		})


		/****************************************************** SIS PRODUCTOS **************************************************************/
		.state('menu.listas-productos-lista',{
			url:'sisproductos',
			templateUrl:'front/app/core/listas/productos/productos.lista.tpl.html',
			controller:"sisProductosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Productos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-productos.srv.js",
												    	"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
												    	"front/app/common/services/sis-empresas.srv.js",
													    "front/app/core/listas/productos/productos.lista.ctrl.js",
													    "front/app/core/listas/productos/productos.detail.ctrl.js",
													   ]											
											},		              	

					]);
		        }]
			}
		})


	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /*************************************************** COMPRAS ****************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/

		/******** PROVEEDORES *******/		
		.state('menu.compras-proveedores-lista',{
			url:'proveedores',
			templateUrl:'front/app/core/compras/proveedores/proveedores.lista.tpl.html',
			controller:"proveedoresListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Proveedores'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'comprasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/compras.mdl.js",
								              			"front/app/common/services/cmp-proveedores.srv.js",
													    "front/app/core/compras/proveedores/proveedores.lista.ctrl.js",
													    "front/app/core/compras/proveedores/proveedores.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/*************************************************** COMPRAS OPERACIONES MAIN **********************************************************/    
		.state('menu.compras-operaciones-lista',{
			url:'cmpoperaciones',
			templateUrl:'front/app/core/compras/operaciones/cmpoperaciones.lista.tpl.html',
			controller:"cmpoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Compra'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
								              			"front/app/common/services/sis-listas.srv.js",
													]
											},
				          					{
				          						name: 'comprasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/compras.mdl.js",
								              			"front/app/common/services/cmp-operaciones.srv.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/*************************************************** COMPRAS OPERACIONES DETAIL **********************************************************/
		.state('config.compras-operaciones-detail',{
			url:'cmpoperaciones-detalle',
			templateUrl:'front/app/core/compras/operaciones/cmpoperaciones.detail.tpl.html',
			controller:"cmpoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operación de Compra',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
													   ]
											},
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-productos.srv.js",
								              			"front/app/common/services/inv-almacenes.srv.js",
								              			"front/app/common/services/inv-formulas.srv.js",
								              			"front/app/common/services/inv-movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.inventario.ctrl.js",
													   ]
											},
				          					{
				          						name: 'comprasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/compras.mdl.js",
								              			"front/app/common/services/cmp-proveedores.srv.js",
													    "front/app/core/compras/proveedores/proveedores.detail.ctrl.js",
								              			"front/app/common/services/cmp-operaciones.srv.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.detail.ctrl.js",
													    "front/app/core/compras/operaciones/cmpoperaciones.modal.carga.ctrl.js",
													   ]
											},																										          					

					]);
		        }]
			}
		})



	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /*************************************************** VENTAS *****************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/

		/*************************************************** CLIENTES **********************************************************/
		.state('menu.ventas-clientes-lista',{
			url:'clientes',
			templateUrl:'front/app/core/ventas/clientes/clientes.lista.tpl.html',
			controller:"clientesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Clientes'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'ventasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/ventas.mdl.js",
								              			"front/app/common/services/vnt-clientes.srv.js",
													    "front/app/core/ventas/clientes/clientes.lista.ctrl.js",
													    "front/app/core/ventas/clientes/clientes.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/*************************************************** VENTAS OPERACIONES MAIN **********************************************************/
		.state('menu.ventas-operaciones-lista',{
			url:'vntoperaciones',
			templateUrl:'front/app/core/ventas/operaciones/vntoperaciones.lista.tpl.html',
			controller:"vntoperacionesListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operaciones de Venta'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
								              			"front/app/common/services/sis-listas.srv.js",
													]
											},
				          					{
				          						name: 'ventasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/ventas.mdl.js",
								              			"front/app/common/services/vnt-operaciones.srv.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.lista.ctrl.js",
													   ]
											},


					]);
		        }]
			}
		})

		/*************************************************** OPERACIONES DETAIL **********************************************************/
		.state('config.ventas-operaciones-detail',{
			url:'vntoperaciones-detalle',
			templateUrl:'front/app/core/ventas/operaciones/vntoperaciones.detail.tpl.html',
			controller:"vntoperacionesDetailController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Operación de Venta',
	            listaOperaciones: null,
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad', function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'sistemaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/sistema.mdl.js",
								              			"front/app/common/services/sis-listas.srv.js",
								              			"front/app/common/services/sis-operaciones.srv.js",
								              			"front/app/common/services/sis-unidades.srv.js",
													   ]
											},
				          					{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-productos.srv.js",
								              			"front/app/common/services/inv-almacenes.srv.js",
								              			"front/app/common/services/inv-formulas.srv.js",
								              			"front/app/common/services/inv-movimientos.srv.js",
														"front/app/core/movimientos/modals/modal.ventas.ctrl.js",
														"front/app/core/movimientos/modals/modal.cargasel.ctrl.js",
													   ]
											},
											{
				          						name: 'cajaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/caja.mdl.js",
								              			"front/app/common/services/caj-bancos.srv.js",
								              			"front/app/common/services/caj-instrumentos.srv.js",
								              			"front/app/common/services/caj-movimientos.srv.js",
													    "front/app/core/caja/movimientos/caj-movimientos.modal.ctrl.js",
													   ]
											},
											{
				          						name: 'empresaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/empresa.mdl.js",
								              			"front/app/common/services/emp-empleados.srv.js",
								              			"front/app/common/services/emp-vehiculos.srv.js",
													    "front/app/core/empresa/empleados/empleados.detail.ctrl.js",
													    "front/app/core/empresa/vehiculos/vehiculos.detail.ctrl.js",
													   ]
											},
				          					{
				          						name: 'ventasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/ventas.mdl.js",
								              			"front/app/common/services/vnt-clientes.srv.js",
													    "front/app/core/ventas/clientes/clientes.detail.ctrl.js",
								              			"front/app/common/services/vnt-operaciones.srv.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.detail.ctrl.js",
													    "front/app/core/ventas/operaciones/vntoperaciones.modal.carga.ctrl.js",
													   ]
											},																										          					


					]);
		        }]
			}
		})


	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /*************************************************** REPORTES ***************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/

		/***************************************************** INVENTARIO ***********************************************************/	
		.state('menu.reportes-inventario-lista',{
			url:'rep-inventario',
			templateUrl:'front/app/core/reportes/inventario/inventario.reportes.tpl.html',
			controller:"inventarioReportesController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Reportes de Inventario'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							/*
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},
											*/
											{
				          						name: 'inventarioModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/inventario.mdl.js",
								              			"front/app/common/services/inv-categorias.srv.js",
								              			"front/app/common/services/inv-productos.srv.js",
													   ]
											},		              	
				          					{
				          						name: 'reportesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/reportes.mdl.js",
								              			"front/app/common/services/rep-inventarios.srv.js",
													    "front/app/core/reportes/inventario/inventario.reportes.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})


		/******************************************************* VENTAS **************************************************************/
		.state('menu.reportes-ventas-lista',{
			url:'rep-ventas',
			templateUrl:'front/app/core/reportes/ventas/ventas.reportes.tpl.html',
			controller:"ventasReportesController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Reportes de Ventas'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
												    	"front/app/common/services/sis-usuarios.srv.js",
													   ]											
											},
											{
			          							name: 'empresaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/empresa.mdl.js",
												    	"front/app/common/services/emp-empleados.srv.js",
													   ]											
											},
											{
				          						name: 'ventasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/ventas.mdl.js",
								              			"front/app/common/services/vnt-clientes.srv.js",
								              			"front/app/common/services/vnt-operaciones.srv.js",
													   ]
											},		              	
				          					{
				          						name: 'reportesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/reportes.mdl.js",
								              			"front/app/common/services/rep-ventas.srv.js",
													    "front/app/core/reportes/ventas/ventas.reportes.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/******************************************************* CAJA **************************************************************/
		.state('menu.reportes-caja-lista',{
			url:'rep-caja',
			templateUrl:'front/app/core/reportes/caja/caja.reportes.tpl.html',
			controller:"cajaReportesController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Reportes de Caja'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							
																						{
				          						name: 'cajaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/caja.mdl.js",
								              			"front/app/common/services/caj-bancos.srv.js",
								              			"front/app/common/services/caj-instrumentos.srv.js",
													   ]
											},		              	
				          					{
				          						name: 'reportesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/reportes.mdl.js",
								              			"front/app/common/services/rep-caja.srv.js",
													    "front/app/core/reportes/caja/caja.reportes.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})

		/******************************************************* COMPRAS **************************************************************/
		.state('menu.reportes-compras-lista',{
			url:'rep-compras',
			templateUrl:'front/app/core/reportes/compras/compras.reportes.tpl.html',
			controller:"comprasReportesController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Reportes de Compras'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
												    	"front/app/common/services/sis-operaciones.srv.js",
												    	"front/app/common/services/sis-usuarios.srv.js",
													   ]											
											},
											{
				          						name: 'comprasModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/compras.mdl.js",
								              			"front/app/common/services/cmp-proveedores.srv.js",
								              			"front/app/common/services/cmp-operaciones.srv.js",
													   ]
											},		              	
				          					{
				          						name: 'reportesModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/reportes.mdl.js",
								              			"front/app/common/services/rep-compras.srv.js",
													    "front/app/core/reportes/compras/compras.reportes.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})


		/****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /******************************************************* CAJA ***************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/


		/*************************************************** BANCOS **********************************************************/
		.state('menu.caja-bancos-lista',{
			url:'caja_bancos',
			templateUrl:'front/app/core/caja/bancos/bancos.list.tpl.html',
			controller:"bancosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Cuentas Bancarias'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},		              	
				          					{
				          						name: 'cajaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/caja.mdl.js",
								              			"front/app/common/services/caj-bancos.srv.js",
													    "front/app/core/caja/bancos/bancos.list.ctrl.js",
													    "front/app/core/caja/bancos/bancos.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})	    

		/*************************************************** INSTRUMENTOS **********************************************************/
		.state('menu.caja-instrumentos-list',{
			url:'caja-instrumentos',
			templateUrl:'front/app/core/caja/instrumentos/instrumentos.list.tpl.html',
			controller:"instrumentosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Instrumentos de Pago'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
											{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},
				          					{
				          						name: 'cajaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/caja.mdl.js",
								              			"front/app/common/services/caj-bancos.srv.js",
								              			"front/app/common/services/caj-instrumentos.srv.js",
													    "front/app/core/caja/instrumentos/instrumentos.list.ctrl.js",
													    "front/app/core/caja/instrumentos/instrumentos.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})


		/****************************************************************************************************************************/
		/****************************************************************************************************************************/	    	    
	    /******************************************************* EMPRESA ***************************************************************/	    
	    /****************************************************************************************************************************/
		/****************************************************************************************************************************/


		/*************************************************** VEHICULOS **********************************************************/
		.state('menu.empresa-vehiculos-lista',{
			url:'empresa-vehiculos',
			templateUrl:'front/app/core/empresa/vehiculos/vehiculos.list.tpl.html',
			controller:"vehiculosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Vehículos'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
				          					{
				          						name: 'empresaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/empresa.mdl.js",
								              			"front/app/common/services/emp-vehiculos.srv.js",
													    "front/app/core/empresa/vehiculos/vehiculos.list.ctrl.js",
													    "front/app/core/empresa/vehiculos/vehiculos.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})	    

		/*************************************************** EMPLEADOS **********************************************************/
		.state('menu.empresa-empleados-lista',{
			url:'empresa-empleados',
			templateUrl:'front/app/core/empresa/empleados/empleados.list.tpl.html',
			controller:"empleadosListaController",
			controllerAs:"ctrl",
			params: {
	            sectionName: 'Empleados'
	        },			
			resolve:{
		        usuariosLoad: ['$ocLazyLoad' ,function($ocLazyLoad){
		          return $ocLazyLoad.load([
		          							{
			          							name: 'sistemaModule',
				          						serie:true,
				          						files:[	
				          								"front/app/common/modules/sistema.mdl.js",
												    	"front/app/common/services/sis-listas.srv.js",
													   ]											
											},
				          					{
				          						name: 'empresaModule',
				          						serie:true,
				          						files:[	
								              			"front/app/common/modules/empresa.mdl.js",
								              			"front/app/common/services/emp-empleados.srv.js",
													    "front/app/core/empresa/empleados/empleados.list.ctrl.js",
													    "front/app/core/empresa/empleados/empleados.detail.ctrl.js",
													   ]
											},
					]);
		        }]
			}
		})	    


	}]);

})();