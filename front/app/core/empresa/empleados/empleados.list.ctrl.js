(function(){
  'use strict';

  angular.module("empresaModule")
  .controller("empleadosListaController", empleadosListaController);
  empleadosListaController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'empEmpleadosService', 'sisListasService', 'CONFIG'];

  function empleadosListaController($rootScope, $filter, $mdDialog, uiGridConstants, empEmpleadosService, sisListasService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaTipos=[];
    var listaBancos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'tipo_nombre', displayName: 'TIPO', sort: { direction: uiGridConstants.ASC }, },
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'cedula', displayName: 'CEDULA',},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/empresa/empleados/empleados.list.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      //TIPOS EMPLEADO
      sisListasService.getByCampo('tipo_empleado').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaTipos=res.data.response.datos;
        }
      })


      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return empEmpleadosService.getList(logEmpresa.id).then(function(res){
          //console.log(res)
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return empEmpleadosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          empEmpleadosService.one={};
          angular.copy(res.data.response.datos,empEmpleadosService.one)
          empEmpleadosService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return empEmpleadosService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          empEmpleadosService.one={};
          angular.copy(res.data.response.datos,empEmpleadosService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'empleadosDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/empresa/empleados/empleados.detail.tpl.html',
          locals: {
            tipos: listaTipos
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Empleado guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();