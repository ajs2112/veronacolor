(function(){
  'use strict';
  
  angular.module("empresaModule")
  .controller("empleadosDetailController", empleadosDetailController);
  empleadosDetailController.$inject=['$mdDialog', '$scope', 'empEmpleadosService', 'tipos'];

  function empleadosDetailController($mdDialog, $scope, empEmpleadosService, tipos){
    var vm=this;
    vm.listaTipos=tipos;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=empEmpleadosService.one;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      empEmpleadosService.setOne(vm.one)
      .then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 
  }

})();