(function(){
  'use strict';

  angular.module("empresaModule")
  .controller("vehiculosListaController", vehiculosListaController);
  vehiculosListaController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'empVehiculosService', 'CONFIG'];

  function vehiculosListaController($rootScope, $filter, $mdDialog, uiGridConstants, empVehiculosService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaTipos=[];
    var listaBancos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'vehiculo', displayName: 'VEHICULO', sort: { direction: uiGridConstants.ASC }, },
          { field: 'marca', displayName: 'MARCA',},
          { field: 'modelo', displayName: 'MODELO',},
          { field: 'placa', displayName: 'PLACA',},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/empresa/vehiculos/vehiculos.list.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();

      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return empVehiculosService.getList(logEmpresa.id).then(function(res){
          //console.log(res)
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return empVehiculosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          empVehiculosService.one={};
          angular.copy(res.data.response.datos,empVehiculosService.one)
          empVehiculosService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return empVehiculosService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          empVehiculosService.one={};
          angular.copy(res.data.response.datos,empVehiculosService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'vehiculosDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/empresa/vehiculos/vehiculos.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Vehículo guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();