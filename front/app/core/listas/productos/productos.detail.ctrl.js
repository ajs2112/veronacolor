(function(){
  'use strict';
  
  angular.module("sistemaModule")
  .controller("sisProductosDetailController", sisProductosDetailController);
  sisProductosDetailController.$inject=['$mdDialog', '$scope', '$filter', 'sisProductosService', 'sisListasService', 'sisUnidadesService', 'sisEmpresasService', 'CONFIG'];

  function sisProductosDetailController($mdDialog, $scope, $filter, sisProductosService, sisListasService, sisUnidadesService, sisEmpresasService, CONFIG){
    var vm=this;
    vm.one = {};
    vm.error="";
    vm.id_sistema="";
    vm.btnEliminar=false;
    vm.full=true;

    vm.listaSistemas= [];
    vm.listaUnidades= [];
    vm.empresasSeleccionadas = [];

    var _listaUnidades=[];

    activate();

    function selectByTrigger (lista, trigger) {
      if (trigger){
        var esta=false;

        for (var i = 0; i < lista.length; i++) {
          if (lista[i].trigger==trigger){
            esta= true;
          }
        }

      } else {
        esta=false;
      }
      return esta;
    }

    function activate(){
      Pace.restart();
      vm.btnEliminar= selectByTrigger(CONFIG.menuItems,'sistema-productos-btnEliminar')
      //SISTEMA UNIDADES
      sisListasService.getByCampo('sistema_unidades').then(function(res){
          vm.listaSistemas=res.data.response.datos;
    
          //UNIDADES
          sisUnidadesService.getList().then(function(res){
              _listaUnidades=res.data.response.datos;

              //ONE
              vm.one=sisProductosService.one;
              
              //OBTENER SISTEMA DE UNIDADES
              if (vm.one.id_unidad_venta){
                var filtro={};
                filtro.id=vm.one.id_unidad_venta;   
                var result = $filter('filter')(_listaUnidades, filtro, true);
                if (result.length){
                  vm.id_sistema=result[0].id_tipo;
                  vm.listaUnidades = $filter('filter')(_listaUnidades, vm.id_sistema);
                }
              }

              //EMPRESAS
              var myLogUser=JSON.parse(sessionStorage.getItem("logUser"));
              var permisos={permisos:myLogUser.empresas};
              console.log('permisos',permisos)
              sisEmpresasService.getListByUser(permisos).then(function(res){
                  vm.listaEmpresas=res.data.response.datos;
                  console.log('vm.listaEmpresas',res)
                  //EMPRESAS
                  //console.log(vm.one.empresas)
                  if (vm.one.empresas.length){
                    vm.empresasSeleccionadas=JSON.parse(vm.one.empresas);        
                  } else {
                    vm.empresasSeleccionadas=[];
                  }
                  vm.one.empresas=vm.empresasSeleccionadas;
              })
          })
    
      })


      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.filtrarUnidades = function() {
      var filtro={};
      filtro.id_tipo=vm.id_sistema;
      vm.listaUnidades = $filter('filter')(_listaUnidades, filtro, true);
     }

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      vm.one.empresas=JSON.stringify(vm.empresasSeleccionadas);
      console.log('setOne',vm.one)
      sisProductosService.setOne(vm.one)
      .then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide(res);
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 


    vm.eliminar = function() {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
            .title('¿Realmente desea eliminar el producto?')
            .multiple(true)
            .textContent('Esta acción no se podrá deshacer!.')
            .ariaLabel('Eliminar')
            .ok('Si, deseo eliminarlo!')
            .cancel('No');

      $mdDialog.show(confirm).then(function() {

        vm.full=false;
        Pace.restart();
        sisProductosService.delOne(vm.one.id).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            $mdDialog.hide(res);
            Pace.stop();
          } else if (res.data && res.data.code!==0){
            localStorage.setItem("token",res.data.response.token);
            vm.error=res.data.response.token;
            Pace.stop();
            vm.full=true;
          } 
        })

      }, function() {
        $mdDialog.cancel();
      });


      
    } 


  }

})();