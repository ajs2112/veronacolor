(function(){
  'use strict';
  
  angular.module("sistemaModule")
  .controller("empresasDetailController", empresasDetailController);
  empresasDetailController.$inject=['$mdDialog', '$scope', 'sisEmpresasService'];

  function empresasDetailController($mdDialog, $scope, sisEmpresasService){
    var vm=this;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=sisEmpresasService.one;
      vm.one.es_inactivo=!!+vm.one.es_inactivo;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      vm.full=false;
      vm.one.es_inactivo=+vm.one.es_inactivo;
      Pace.restart();
      sisEmpresasService.setOne(vm.one)
      .then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 
  }

})();