(function(){
  'use strict';

  angular.module("sistemaModule")
  .controller("cotizacionesListaController", cotizacionesListaController);
  cotizacionesListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'sisCotizacionesService', 'CONFIG'];

  function cotizacionesListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, sisCotizacionesService, CONFIG){
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'fecha', displayName: 'FECHA', sort: { direction: uiGridConstants.DESC }, },
          { field: 'valor', displayName: 'VALOR', maxWidth: 200, cellFilter: 'currency:""',cellClass: 'cell-align-right',},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/listas/cotizaciones/cotizaciones.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      return getList()
      .then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return sisCotizacionesService.getList()
        .then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return sisCotizacionesService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          sisCotizacionesService.one={};
          angular.copy(res.data.response.datos,sisCotizacionesService.one)
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return sisCotizacionesService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          sisCotizacionesService.one={};
          angular.copy(res.data.response.datos,sisCotizacionesService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'cotizacionesDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/listas/cotizaciones/cotizaciones.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Cotización guardada con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();