(function(){
  'use strict';
  
  angular.module("sistemaModule")
  .controller("cotizacionesDetailController", cotizacionesDetailController);
  cotizacionesDetailController.$inject=['$mdDialog', '$scope', 'sisCotizacionesService'];

  function cotizacionesDetailController($mdDialog, $scope, sisCotizacionesService){
    var vm=this;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=sisCotizacionesService.one;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.miFecha = function() {
      console.log(vm.one.fecha)
    };

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      sisCotizacionesService.setOne(vm.one)
      .then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=false;
        } 
      })
    } 
  }

})();