(function(){
  'use strict';

  angular.module("cajaModule")
  .controller("bancosListaController", bancosListaController);
  bancosListaController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'cajBancosService', 'sisListasService', 'CONFIG'];

  function bancosListaController($rootScope, $filter, $mdDialog, uiGridConstants, cajBancosService, sisListasService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaTipos=[];
    var listaBancos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'banco_nombre', displayName: 'BANCO', sort: { direction: uiGridConstants.ASC }, },
          { field: 'tipo_nombre', displayName: 'TIPO', sort: { direction: uiGridConstants.ASC }, },
          { field: 'numero', displayName: 'NUMERO',},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/caja/bancos/bancos.list.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      console.log('CajaCuentasBancariasList')
      //BANCOS
      sisListasService.getByCampo('bancos').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaBancos=res.data.response.datos;
        }
      })

      //TIPOS
      sisListasService.getByCampo('tipo_cuenta_bancaria').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaTipos=res.data.response.datos;
        }
      })


      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return cajBancosService.getList(logEmpresa.id).then(function(res){
          //console.log(res)
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return cajBancosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cajBancosService.one={};
          angular.copy(res.data.response.datos,cajBancosService.one)
          cajBancosService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return cajBancosService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cajBancosService.one={};
          angular.copy(res.data.response.datos,cajBancosService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'bancosDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/caja/bancos/bancos.detail.tpl.html',
          locals: {
            bancos: listaBancos,
            tipos: listaTipos
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Cuenta bancaria guardada con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();