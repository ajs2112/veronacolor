(function(){
  'use strict';

  angular.module("cajaModule")
  .controller("instrumentosListaController", instrumentosListaController);
  instrumentosListaController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'cajInstrumentosService', 'CONFIG'];

  function instrumentosListaController($rootScope, $filter, $mdDialog, uiGridConstants, cajInstrumentosService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));

    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaInstrumentos=[];
    var listaCuentas=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'nombre', displayName: 'INSTRUMENTO', sort: { direction: uiGridConstants.ASC }, },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/caja/instrumentos/instrumentos.list.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return cajInstrumentosService.getList(logEmpresa.id)
        .then(function(res){
          //console.log(res)
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return cajInstrumentosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cajInstrumentosService.one={};
          angular.copy(res.data.response.datos,cajInstrumentosService.one)
          cajInstrumentosService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return cajInstrumentosService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cajInstrumentosService.one={};
          angular.copy(res.data.response.datos,cajInstrumentosService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'instrumentosDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/caja/instrumentos/instrumentos.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Instrumento guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();