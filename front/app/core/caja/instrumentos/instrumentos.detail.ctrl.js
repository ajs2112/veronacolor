(function(){
  'use strict';
  
  angular.module("cajaModule")
  .controller("instrumentosDetailController", instrumentosDetailController);
  instrumentosDetailController.$inject=['$mdDialog', '$scope', 'cajInstrumentosService'];

  function instrumentosDetailController($mdDialog, $scope, cajInstrumentosService){
    var vm=this;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=cajInstrumentosService.one;
      vm.one.es_inactivo=!!+vm.one.es_inactivo;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      vm.one.es_inactivo=+vm.one.es_inactivo;
      cajInstrumentosService.setOne(vm.one)
      .then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 
  }

})();