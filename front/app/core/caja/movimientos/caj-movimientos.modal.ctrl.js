(function(){
  'use strict';

  angular.module("cajaModule")
  .controller("cajMovimientosModalController", cajMovimientosModalController);
  cajMovimientosModalController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'sisListasService',  'cajBancosService', 'cajMovimientosService', 'cajInstrumentosService', 'tipoOperacion', 'totalDocumento', 'instrumentosPago', 'CONFIG'];

  function cajMovimientosModalController($rootScope, $filter, $mdDialog, uiGridConstants, sisListasService, cajBancosService, cajMovimientosService, cajInstrumentosService, tipoOperacion, totalDocumento, instrumentosPago, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var monedaSeleccionada=JSON.parse(sessionStorage.getItem("oneCotizacion"));
    var vm=this;
    vm.filtro="";
    vm.full=true;
    vm.list=[];
    vm.listaInstrumentos=[];
    vm.totales={
      documento:totalDocumento,
      seleccionado:0,
      resta:0
    };
    vm.id_instrumento=0;

    vm.id_banco=0;
    vm.id_cuenta=0;
    vm.nro_operacion="";


    vm.bancos_visible=false;
    vm.cuentas_visible=false;
    vm.nro_operacion_visible=false;

    vm.monto=totalDocumento;
    vm.seleccionPago=[];
    console.log(instrumentosPago)
    vm.seleccionPago=instrumentosPago;

    var lista=[];
    var listaInstrumentos=[];

    vm.calcularTotales=function(){
        vm.pagoTotal = false
        vm.totales.seleccionado=0;

        if (vm.seleccionPago){
          for (var i = 0; i < vm.seleccionPago.length; i++) {
            vm.totales.seleccionado += Number(vm.seleccionPago[i].monto);
          }
          
          vm.totales.resta=vm.totales.documento-vm.totales.seleccionado;
          vm.monto=vm.totales.resta;
          //vm.one.monto=vm.totalResta;
          if (vm.totales.seleccionado >= totalDocumento){
            vm.pagoTotal = true
          }
        }
    }

    activate();

    function activate(){
      Pace.restart();
      vm.calcularTotales();

      //INSTRUMENTOS
      cajInstrumentosService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaInstrumentos=res.data.response.datos;

          var objInst={};
          objInst.es_inactivo="0";
          vm.listaInstrumentos= $filter('filter')(listaInstrumentos,objInst, true);

        }
      })    

      //BANCOS
      sisListasService.getByCampo('bancos').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaBancos=res.data.response.datos;
        }
      })

      //CUENTAS
      cajBancosService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaCuentas=res.data.response.datos;
        }
      })

    }

    /****************************************************************************/

    function dbl(numero){
      return parseFloat(numero).toFixed(2);
    }

    vm.addInstrumento = function(){
      var objPago={
        id:"",
        order_id:0,
        last_update:0,
        id_empresa:logEmpresa.id,
        id_instrumento:"",        
        instrumento_nombre:"",
        monto:0,
        id_banco:"",
        id_cuenta:"",
        numero_operacion:"",
        id_moneda:monedaSeleccionada.id_moneda,
        factor:1
      };

      objPago.id_instrumento=vm.id_instrumento;
      var instSeleccionado= $filter('filter')(listaInstrumentos,vm.id_instrumento, undefined);
      objPago.instrumento_nombre=instSeleccionado[0].nombre;;
      objPago.monto=Number(vm.monto);

      if(vm.bancos_visible)objPago.id_banco=vm.id_banco;
      if(vm.cuentas_visible)objPago.id_cuenta=vm.id_cuenta;
      if (vm.nro_operacion_visible)objPago.numero_operacion=vm.nro_operacion;

      vm.seleccionPago.push(objPago);
      console.log(vm.seleccionPago)
      vm.nro_operacion="";
      vm.calcularTotales();
    }

    vm.delInstrumento = function(id){
      var indice = vm.seleccionPago.indexOf(id);
      vm.seleccionPago.splice(indice,1);   
       vm.calcularTotales();    
    }

    vm.selInstrumento = function(){
      var x={};
      x.id=vm.id_instrumento;
      var miIntrumento= $filter('filter')(listaInstrumentos,x, true);
      console.log(miIntrumento[0].id_tipo)
      if(miIntrumento[0]){
        switch(miIntrumento[0].id_tipo.toString()) {
            case "1":
                vm.bancos_visible=false;
                vm.cuentas_visible=false;
                vm.nro_operacion_visible=false;
                break;
            case "2":
                vm.bancos_visible=true;
                vm.cuentas_visible=false;
                vm.nro_operacion_visible=true;
                break;
            case "3":
                vm.bancos_visible=false;
                vm.cuentas_visible=true;
                vm.nro_operacion_visible=true;
                break;
            case "4":
                vm.bancos_visible=false;
                vm.cuentas_visible=true;
                vm.nro_operacion_visible=false;
                break;                
            default:
                vm.bancos_visible=false;
                vm.cuentas_visible=false;
                vm.nro_operacion_visible=false;
        }      
      }

      
    }

    


    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };


    vm.guardar = function(ev){
      Pace.restart();
      $mdDialog.hide(vm.seleccionPago);
      Pace.stop();
      /*
      */
    }

  }

})();