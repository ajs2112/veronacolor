(function(){
  'use strict';
  
  angular.module("ventasModule")
  .controller("vntoperacionesDetailController", vntoperacionesDetailController);

  vntoperacionesDetailController.$inject=['$state', '$scope', '$mdDialog', '$rootScope', '$filter', 'sisOperacionesService', 'sisListasService', 'invAlmacenesService', 'invProductosService', 'sisUnidadesService', 'invMovimientosService', 'vntClientesService', 'cajMovimientosService', 'empEmpleadosService', 'empVehiculosService', 'vntOperacionesService', 'CONFIG'];
  function vntoperacionesDetailController($state, $scope, $mdDialog, $rootScope, $filter, sisOperacionesService, sisListasService, invAlmacenesService, invProductosService, sisUnidadesService, invMovimientosService, vntClientesService, cajMovimientosService, empEmpleadosService, empVehiculosService, vntOperacionesService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.one = {};
    vm.doc_origen={};
    vm.full=true;
    vm.es_flete=false;
    vm.pct_flete=0;

    vm.error="";
    vm.id_sistema="";
    vm.totales={
      items: 0,
      subtotal:  0,
      pct_descuento: 0,
      monto_descuento: 0,
      base_exenta: 0,
      base_imponible : 0,
      monto_impuesto : 0,
      total : 0,      
    };

    vm.listaOperaciones=[];
    vm.listaAlmacenes=[];
    vm.listaVehiculos=[];
    vm.listaEmpleados=[];

    vm.cntProductos=0;

    vm.searchText=null;
    vm.selectedCliente=null;    
    
    var lista=[];
    var _listaProductos=[];
    var _listaUnidades=[];
    var _listaClientes=[];
    var _listaInstrumentos=[];
    var instrumentosPago=[];
    var monedaSeleccionada={};

    console.log('monedaSeleccionada',monedaSeleccionada)
    vm.tipoOperacion={};

    vm.gridDetail = {
        columnDefs: [
          { field: 'producto_codigo', displayName: 'CÓDIGO', maxWidth: 140, enableCellEdit: false},
          { field: 'producto_nombre', displayName: 'DESCRIPCION', enableCellEdit: false},
          { field: 'producto_unidad', displayName: 'UNIDAD', maxWidth: 140, enableCellEdit: false},
          { field: 'cantidad', displayName: 'CANTIDAD', cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth: 140 },
          { field: 'precio_moneda', displayName: 'PRECIO', cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.edit.on.beginCellEdit(null,function(rowEntity, colDef){
            vm.cntSel=1;
            vm.rowSel=rowEntity;
          });
          gridApi.edit.on.afterCellEdit(null,function(rowEntity, colDef){
            calcularTotales();
          });
        },
        enableCellEditOnFocus: true,
    };


    activate();

    $scope.$watch('$root.factor', function() {
        calcularTotales();
    });

    function calcularMoneda(){
      monedaSeleccionada=JSON.parse(sessionStorage.getItem("oneCotizacion"));
      var factor=$rootScope.factor;
      angular.forEach(lista, function(item, index) {
        item.precio_moneda=item.precio*factor;
      });
      vm.totales.total_moneda=vm.totales.total*factor;
    }

    function activate(){
      Pace.restart();

      //PRODUCTOS
      invProductosService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaProductos=res.data.response.datos;
        }
      })

      //UNIDADES
      sisUnidadesService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaUnidades=res.data.response.datos;
        }
      })

      //ALMACENES
      invAlmacenesService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaAlmacenes=res.data.response.datos;
        }
      })

      //CLIENTES
      vntClientesService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaClientes=res.data.response.datos;
          vm.selectedCliente=selectById(_listaClientes, vm.one.id_cliente);
        }
      })

      //EMPLEADOS
      empEmpleadosService.getList(logEmpresa.id).then(function(res){
          vm.listaEmpleados = res.data.response.datos;

      })      

      //VEHICULOS
      empVehiculosService.getList(logEmpresa.id).then(function(res){
          vm.listaVehiculos=res.data.response.datos;
      })            

              

      //ONE 
      vm.one=JSON.parse(sessionStorage.getItem("one"));
      var oneCotizacion=JSON.parse(sessionStorage.getItem("oneCotizacion"));
      vm.one.fecha=new Date(vm.one.fecha);
      vm.one.id_cotizacion=oneCotizacion.id;
      console.log('vm.one onActivate',vm.one)
      //OPERACION
      sisOperacionesService.getOne(vm.one.id_tipo).then(function(res){
          vm.tipoOperacion=res.data.response.datos;
          $rootScope.appSeccion= vm.tipoOperacion.nombre + " (" + vm.one.nro_control + ")";
      })

      if (vm.one.id_doc_origen){
        var one={
          id:vm.one.id_doc_origen,
          id_empresa:logEmpresa.id,
          id_tipo:0
        }
        vntOperacionesService.getOne(one).then(function(res){
          vm.doc_origen=res.data.response.datos;
          if (vm.doc_origen){
            vm.doc_origen.fecha=new Date(vm.doc_origen.fecha);
            vm.doc_origen.fecha=$filter('date')(vm.doc_origen.fecha, 'dd/MM/yyyy');
          }
        })
      }

      return getList().then(function(){
        if (lista){
          vm.gridDetail.data = lista;
          vm.cntProductos=lista.length;    
          calcularTotales();     
          calcularPctFlete(); 
        } else {
          lista=[];
        }

          Pace.stop();
      })
    }

    function getList (){        
      var obj={
        id_tipo:vm.one.id_tipo,
        id_operacion:vm.one.id 
      }
      //PAGO
      cajMovimientosService.getList(obj).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          instrumentosPago=res.data.response.datos;
        }
      })
      //PRODUCTOS
      return invMovimientosService.getList(obj).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          return lista;
        }
      })
    }


    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      var factor=$rootScope.factor;
      console.log('factor al guardar',factor)
      vm.one.id_usuario=CONFIG.logUser.id;

      vm.one.monto_bruto = Number(vm.totales.subtotal)/Number(factor);
      vm.one.monto_neto = Number(vm.totales.total)/Number(factor);
      vm.one.monto_iva = Number(vm.totales.monto_impuesto)/Number(factor);
      vm.one.monto_total = (Number(vm.totales.total) + Number(vm.totales.monto_impuesto))/Number(factor);

      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        detail:lista,
        pago: instrumentosPago
      }
      console.log('obj al guardar')
      console.log(obj)
      vntOperacionesService.setOne(obj).then(function(res){ 
        console.log(res)
        vm.one.id=res.data.response.datos;

        var one={
          id:vm.one.id,
          id_tipo:vm.one.id_tipo
        }
        vntOperacionesService.getOne(one).then(function(res){
          vm.one=res.data.response.datos;
          vm.one.fecha=new Date(vm.one.fecha);
          vm.imprimir();
        })

        $rootScope.showToast('Operación registrada con exito');
        vm.full=true;
      })
    }

    vm.anular = function() {
      vm.full=false;
      Pace.restart();
      vm.one.id_usuario=CONFIG.logUser.id;
      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        //detail:lista
      }
      console.log('send',obj)
      vntOperacionesService.anularOne(obj).then(function(res){ 
        console.log('res del anular',res)
          $rootScope.showToast('Operación anulada con exito');
          vm.full=true;
      })
    }     

    vm.calcularTotales=function(){
      calcularTotales();
    }

    vm.calcularFlete=function(){
      calcularFlete();
    }

    /************************* PRODUCTOS *******************************/
    function calcularPctFlete(){
      var pctFlete=0;
      var _monto=0;
      var valorFlete=0;
      
      for (var i = 0; i < lista.length; i++) {
        if (lista[i].producto_codigo!="SF001") _monto+=Number(lista[i].cantidad*lista[i].precio);
      }

      var filtro={};
      filtro.producto_codigo="SF001";   
      var result = $filter('filter')(lista, filtro, true);
      if (result.length){
        var valorFlete=result[0].precio;
      }
      
      pctFlete=(valorFlete*100)/_monto;
      vm.pct_flete=pctFlete;
    }

    function calcularFlete(){
      console.log('calcularFlete')
      var pctFlete=Number(vm.pct_flete)/100;
      var _monto=0;
      for (var i = 0; i < lista.length; i++) {
        if (lista[i].producto_codigo!="SF001") _monto+=Number(lista[i].cantidad*lista[i].precio);
      }
      var valorFlete=Number(_monto*pctFlete);
      var filtro={};
      filtro.producto_codigo="SF001";   
      var result = $filter('filter')(lista, filtro, true);
      if (result.length){
        result[0].cantidad=1;
        result[0].precio=valorFlete;
        result[0].monto=valorFlete;
        result[0].valor_impuesto=0;
      }
      calcularTotales();
    }
    
    function calcularTotales(){
      calcularMoneda();
      //items: 0,
      vm.totales.subtotal=  0;
      vm.totales.pct_descuento= 0;
      vm.totales.monto_descuento= 0;
      vm.totales.base_exenta= 0;
      vm.totales.base_imponible=  0;
      vm.totales.monto_impuesto= 0;
      vm.totales.total= 0;

      var filtro={};
      filtro.producto_codigo="SF001";   
      var result = $filter('filter')(lista, filtro, true);
      if (result.length){
        vm.es_flete=true;
      }

      var pctDcto=Number(vm.one.pct_descuento)/100;
      vm.totales.pct_descuento=vm.one.pct_descuento;

      for (var i = 0; i < lista.length; i++) {
        var pctIva=Number(lista[i].valor_impuesto/100)+1;
        var iMonto=Number(lista[i].cantidad*lista[i].precio_moneda);
        var iDcto=0;
        var iIva=0;
        vm.totales.subtotal+=Number(iMonto);

        //DESCUENTO
        iDcto = (iMonto * pctDcto);
        iMonto = iMonto - iDcto;

        //iMonto=Number(iMonto)-Number(iMonto*pctDcto);
        iIva=Number(pctIva*iMonto)-Number(iMonto);

        /*
        if (Number(lista[i].valor_impuesto)>0){
          vm.totales.base_imponible+=Number(iMonto);
          vm.totales.monto_impuesto+=Number(iIva);
        } else {
          vm.totales.base_exenta+=Number(iMonto);
        }
        */
        if (vm.tipoOperacion.es_fiscal!=0){
          console.log('documento fiscal')
            if (lista[i].valor_impuesto > 0)
            {
                vm.totales.base_imponible += iMonto;
                vm.totales.monto_impuesto += iIva;
            }
            else
            {
                vm.totales.base_exenta += iMonto;
            }
        } else {
          console.log('documento NOO fiscal')
        }

        vm.totales.monto_descuento+=Number(iDcto);
        vm.totales.total+=Number(iMonto);
      }      

      if(lista.length){
        vm.totales.items=lista.length;
      }
    }

    function agregar(obj){
      if (obj.id_producto){
        //VERIFICAR SI SE ENCUENTRA EN LA LISTA
        var filtro={};
        filtro.id_producto=obj.id_producto;   
        var result = $filter('filter')(lista, filtro, true);
        if (result.length){
          $rootScope.showToast('Producto en lista');
        } else {
          lista.push(obj);
        }
        //VERFICA SI ES FLETE
        if(obj.producto_codigo==="SF001") vm.es_flete=true;

      }
      vm.gridDetail.data = lista;
      vm.cntProductos=lista.length;
      calcularTotales();
    }

    vm.delProducto = function(ev){
      var sel=vm.rowSel;
      var indice = lista.indexOf(sel);
      lista.splice(indice,1);        
      if (sel.producto_codigo="SF001"){
        vm.es_flete=false;
        vm.pct_flete=0;
      } 
      vm.cntSel=0;
      if (!lista){
        lista=[];        
      } else {
        vm.gridDetail.data = lista;
        vm.cntProductos=lista.length;
      }
      calcularTotales();
    }    

  /************************* CLIENTE *******************************/
    vm.querySearch = function( item ) {
      var result= $filter('filter')(_listaClientes, item);
      if (!result){
        result=[];
      }
      return result;
    }

    vm.selCliente=function(item){
      if (item){
        vm.one.id_cliente=item.id;
      }
    }

    function selectById (lista, id) {
      var filtro={};
      filtro.id=id;   
      var result = $filter('filter')(lista, filtro, true);
      if (result){
        if (result.length){
          return result[0];
        } else {
          return null;
        }
      }
    }

    /************************* DIALOGS *******************************/
    vm.cargar=function(ev){
        $mdDialog.show({
          controller: 'modalCargaVentasController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/ventas/operaciones/vntoperaciones.modal.carga.tpl.html',
          parent: angular.element(document.body),
          locals:{
            listaOperaciones:vm.listaOperaciones
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(documento){
            /*
            var obj={
              id_tipo:documento.id_tipo,
              id_operacion:documento.id 
            }
            invMovimientosService.getList(obj).then(function(res){
              if (res.data && res.data.code==0){
                localStorage.setItem("token",res.data.response.token);
                lista=res.data.response.datos;
                if (lista){
                  vm.gridDetail.data = lista;
                  vm.cntProductos=lista.length;          
                } else {
                  lista=[];
                }
              }
            })

            */
            vm.one.id_doc_origen=documento.head.id,
            vm.doc_origen=documento.head,
            vm.doc_origen.fecha=$filter('date')(vm.doc_origen.fecha, 'dd/MM/yyyy');
            vm.selectedCliente=selectById(_listaClientes, vm.doc_origen.id_cliente);

            lista = documento.lista;
            vm.gridDetail.data = lista;
            vm.cntProductos=documento.lista.length;  
            calcularTotales();
            

          $rootScope.showToast('Documento cargado con exito');
        });
    }

    vm.openDialogProductos=function(ev){
        $mdDialog.show({
          controller: 'modalVentasController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/movimientos/modals/modal.ventas.tpl.html',
          parent: angular.element(document.body),
          locals:{
            tipoOperacion: vm.tipoOperacion.id,
            listaProductos:_listaProductos,
            listaUnidades:_listaUnidades,
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(producto){
          console.log('producto[0]',producto[0])
          agregar(producto[0]);
        });
    }

    vm.openDialogCaja=function(ev){
        $mdDialog.show({
          controller: 'cajMovimientosModalController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/caja/movimientos/caj-movimientos.modal.tpl.html',
          parent: angular.element(document.body),
          locals:{
            tipoOperacion: vm.tipoOperacion.id,
            instrumentosPago: instrumentosPago,
            totalDocumento: Number(vm.totales.total)+Number(vm.totales.monto_impuesto)
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(listaInstrumentosPago){
          //console.log(listaInstrumentosPago)
          instrumentosPago=listaInstrumentosPago;
        });
    }

    vm.modalCliente=function(ev,idCliente){
      vntClientesService.getOne(idCliente).then(function(res){
        vntClientesService.one={};
        angular.copy(res.data.response.datos,vntClientesService.one)

        $mdDialog.show({
          controller: 'clientesDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/ventas/clientes/clientes.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(id){
          $rootScope.showToast('Cliente guardado con exito');
          vntClientesService.getList(logEmpresa.id).then(function(res){
            _listaClientes=res.data.response.datos;
            vm.selectedCliente=selectById(_listaClientes, id.toUpperCase());
          })
        });

      })
    }

    /************************* VALIDACIONES *******************************/    
    vm.seGuarda=function(formValid){
      var valido=false;
      if (formValid && vm.full && vm.cntProductos){
        valido= true;
      }

      if (vm.tipoOperacion.signo_caja!='N'){
        if(instrumentosPago.length){
          valido=true;
        } else {
          valido=false;
        }
      }

      if (vm.one.id_status!=0){
        valido=false;
      }
      
      return valido;
    }

    /************************* IMPRIMIR *******************************/
    vm.imprimir=function(ev){
        function alinearDerecha(texto){
          var margen=doc.internal.pageSize.width-20;
          return margen - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize());
        }

        var totales={
          subtotal : 0,
          descuento: 0,
          base_exenta:0,
          base_imponible : 0,
          monto_impuesto : 0,
          total : 0,
          total_sin_iva:0
        };

        vm.full=false;
        //DO ROWS
        var doRows = function(data){
          var numero=0;
          var _datos=[];
          //console.log('data a imprimir')
          //console.log(data)
          var pctDcto=Number(vm.one.pct_descuento)/100;
          var pctAdic=Number(vm.one.pct_adicional)/100;

          for (var i = 0; i < data.length; i++) {
            var _fila={};
            numero+=1;
            //PARA EL MULTIMONEDA
            var _factor=1;
            var _valor=data[i].precio_moneda;
            var _monto=(data[i].cantidad*_valor)

            _fila.numero=numero;
            _fila.producto_codigo=data[i].producto_codigo;
            _fila.producto_nombre=data[i].producto_nombre;
            _fila.producto_unidad=data[i].producto_unidad;
            _fila.cantidad=$filter('currency')(Number(data[i].cantidad),'');
            _fila.valor=$filter('currency')(Number(_valor),'');
            _fila.monto=$filter('currency')(Number(_monto),'');

            totales.subtotal+=Number(_monto);
            totales.descuento+=Number(_monto)*pctDcto;

            console.log('data[i].valor_impuesto',data[i].valor_impuesto)
            if (Number(data[i].valor_impuesto)>0){
              var pctIva=Number(data[i].valor_impuesto/100)+1;
              var base=(Number(_monto) - (Number(_monto)*pctDcto));
              totales.monto_impuesto+=Number(base*pctIva) - (Number(base));
              totales.base_imponible+=base;
            } else {
              totales.base_exenta+=(Number(_monto) - (Number(_monto)*pctDcto));
            }
            _datos.push(_fila);
          }

            totales.total+=Number(totales.base_imponible)+Number(totales.monto_impuesto)+Number(totales.base_exenta);
            totales.total_sin_iva+=Number(totales.base_imponible)+Number(totales.base_exenta);

            
          return _datos;
        }

        var logoA = new Image();

        if (vm.tipoOperacion.es_transporte!='0'){
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "producto_codigo"},
              {title: "DESCRIPCION", dataKey: "producto_nombre"}, 
              {title: "UND", dataKey: "producto_unidad"},
              {title: "CANT", dataKey: "cantidad"},
          ];
        } else {
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "producto_codigo"},
              {title: "DESCRIPCION", dataKey: "producto_nombre"}, 
              {title: "UND", dataKey: "producto_unidad"},
              {title: "CANT", dataKey: "cantidad"},
              {title: "PRECIO", dataKey: "valor"},
              {title: "MONTO", dataKey: "monto"},            
          ];
        }

        var rows = doRows(lista);


            var centerText = function(texto, alto){
              var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
              doc.text(texto, xOffset, alto);        
            }

            var doc = new jsPDF('p', 'pt', 'letter');
            var totalPagesExp = "{total_pages_count_string}";

            var pageContent = function (data) {
                // HEADER ***************************************************************************************************
                var topH=80;

                doc.setFontStyle('normal');
                /*EMPRESA*/
                if (vm.one.tipo_operacion_nombre!="Factura"){
                  doc.addImage(logoA , 'png', 20, 15, 90, 50);
                  doc.setFontSize(10);
                  doc.text(logEmpresa.nombre,32,75);
                  doc.setFontSize(9);
                  doc.text(logEmpresa.rif,32,88);
                }                

                var miFecha=$filter('date')(vm.one.fecha, 'dd/MM/yyyy');

                doc.text("FECHA: "+ miFecha,440,topH+10);
                doc.text("Nº "+vm.one.tipo_operacion_nombre+" : "+ vm.one.nro_control,440,topH+25);

                doc.setFontSize(16);
                centerText(vm.one.tipo_operacion_nombre,topH+50);                                                

                doc.setFontSize(10);
                doc.text("CLIENTE: "+ vm.selectedCliente.nombre,30,topH+80);
                doc.text("RIF: "+ vm.selectedCliente.rif,460,topH+80);
                //doc.text("DIRECCIÓN: "+ vm.selectedCliente.direccion.substr(0, 60),30,topH+95);
                doc.text("DIRECCIÓN: ",30,topH+95);
                doc.text("TELÉFONO: "+ vm.selectedCliente.telefono,460,topH+95);

                var splitTitle = doc.splitTextToSize(vm.selectedCliente.direccion, 360);
                console.log(splitTitle)
                doc.text(90, topH+95, splitTitle);

                /* DOCUMENTO AFECTADO*/
                if (vm.tipoOperacion.es_derivado!='0'){
                    doc.text("DOCUMENTO: "+ vm.doc_origen.tipo_operacion_nombre,30,topH+120);
                    doc.text("NÚMERO: "+ vm.doc_origen.nro_control,200,topH+120);
                    doc.text("FECHA: "+ vm.doc_origen.fecha,360,topH+120);
                }


                // FOOTER ***************************************************************************************************
                var topF=doc.internal.pageSize.height - 120;
                var left=doc.internal.pageSize.width -260;
                
                /* TRANSPORTE*/
                if (vm.tipoOperacion.es_transporte!='0'){
                  var _listaVehiculos=vm.listaVehiculos;
                  var _vehiculo=selectById(_listaVehiculos,vm.id_vehiculo);

                  var _listaEmpleados=vm.listaEmpleados;
                  var _empleado=selectById(_listaEmpleados,vm.one.id_chofer);

                  console.log('_listaEmpleados',_listaEmpleados)
                  console.log('vm.id_chofer',vm.one.id_chofer)                  

                  doc.text("VEHICULO: "+ _vehiculo.vehiculo,30,topF);
                  doc.text("MODELO: "+ _vehiculo.modelo,200,topF);
                  doc.text("MARCA: "+ _vehiculo.marca,360,topF);
                  doc.text("COLOR: "+ _vehiculo.color,30,topF+15);
                  doc.text("PLACA: "+ _vehiculo.placa,200,topF+15);
                  doc.text("RACDA: "+ _vehiculo.racda,360,topF+15);

                  doc.text("CHOFER: "+ _empleado.nombre+', C.I: '+_empleado.cedula,30,topF+30);

                } else {

                  
                  //TOTALES
                  var printDescuento=false;
                  var printExento=false;

                  /*                  
                  totales.subtotal=$filter('currency')(Number(totales.subtotal), '');
                  totales.descuento=$filter('currency')(Number(totales.descuento), '');
                  totales.base_exenta=$filter('currency')(Number(totales.base_exenta), '');
                  totales.base_imponible=$filter('currency')(Number(totales.base_imponible), '');
                  totales.monto_impuesto=$filter('currency')(Number(totales.monto_impuesto), '');
                  totales.total=$filter('currency')(Number(totales.total), '');
                  totales.total_sin_iva=$filter('currency')(Number(totales.total_sin_iva), '');
                  */
                  console.log('vm.totales',vm.totales)
                  totales.subtotal=$filter('currency')(Number(vm.totales.subtotal), '');
                  totales.descuento=$filter('currency')(Number(vm.totales.monto_descuento), '');
                  totales.base_exenta=$filter('currency')(Number(vm.totales.base_exenta), '');
                  totales.base_imponible=$filter('currency')(Number(vm.totales.base_imponible), '');
                  totales.monto_impuesto=$filter('currency')(Number(vm.totales.monto_impuesto), '');
                  totales.total=$filter('currency')(Number(vm.totales.total)+Number(vm.totales.monto_impuesto), '');
                  totales.total_sin_iva=$filter('currency')(Number(vm.totales.total), '');


                  if (Number(vm.totales.monto_descuento)>0) printDescuento=true;
                  if (Number(vm.totales.base_exenta)>0) printExento=true;

                  doc.text("SUBTOTAL: ",left,topF);
                  doc.text(totales.subtotal,alinearDerecha(totales.subtotal) ,topF);

                  if (printDescuento){ 
                    topF+=15;
                    doc.text("DESCUENTO ("+ vm.one.pct_descuento.toString()+ "%): ",left,topF);
                    doc.text(totales.descuento,alinearDerecha(totales.descuento) ,topF);
                  }


                  if (vm.tipoOperacion.id=="11E7C39A3423F2A88F1A00E04C6F7E24"){
                    topF+=15;
                    doc.text("TOTAL ("+monedaSeleccionada.moneda_descrip+"): ",left,topF);             
                    doc.text(totales.total_sin_iva,alinearDerecha(totales.total_sin_iva) ,topF);                                  
                  } else {
                    if (printExento){
                      topF+=15;
                      doc.text("EXENTO: ",left,topF);
                      doc.text(totales.base_exenta,alinearDerecha(totales.base_exenta) ,topF);
                    }
                    topF+=15;
                    doc.text("BASE IMPONIBLE: ",left,topF);
                    doc.text(totales.base_imponible,alinearDerecha(totales.base_imponible) ,topF);
                    topF+=15;
                    doc.text("IVA: ",left,topF);
                    doc.text(totales.monto_impuesto,alinearDerecha(totales.monto_impuesto) ,topF);
                    topF+=15;
                    doc.text("TOTAL ("+monedaSeleccionada.moneda_descrip+"): ",left,topF);             
                    doc.text(totales.total,alinearDerecha(totales.total) ,topF);                                
                  }


                }



                var str = "Page " + data.pageCount;
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }

            };


          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){
                doc.setFontSize(10);
                
                var topT=210;

                doc.autoTable(columns, rows, {
                    addPageContent: pageContent,
                    margin: {top: topT, bottom: 140, left:20, right:30}, 
                    theme: 'grid',
                    headerStyles: {
                      fillColor: 255,
                      textColor: 0,
                      lineWidth: 1,
                      fontSize: 8,
                    },
                    bodyStyles: {fontSize: 8},
                    columnStyles: {
                        numero: {columnWidth: 25, halign: 'right'},
                        producto_codigo: {columnWidth: 50, halign: 'left'},
                        producto_unidad: {columnWidth: 40, halign: 'left'},
                        cantidad: {columnWidth: 40, halign: 'right'},
                        valor: {columnWidth: 75, halign: 'right'},
                        monto: {columnWidth: 90, halign: 'right'},
                    },
                });

                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp);
                }

                var blob= doc.output("blob");
                window.open(URL.createObjectURL(blob));
                vm.full=true;

          }
    }

  
  }

})();