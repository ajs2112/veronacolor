(function(){
  'use strict';

  angular.module("ventasModule")
  .controller("modalCargaVentasController", modalCargaVentasController);
  modalCargaVentasController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'listaOperaciones', 'vntOperacionesService', 'CONFIG'];

  function modalCargaVentasController($rootScope, $filter, $mdDialog, uiGridConstants, listaOperaciones, vntOperacionesService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));

    var vm=this;
    vm.filtro="";
    vm.full=true;
    vm.list=[];
    vm.listaOperaciones=listaOperaciones;

    vm.cntSel=0;
    vm.id_tipo=0;
    vm.rowSel=null;


    var lista=[];

    vm.gridModal = {
        columnDefs: [
          { field: 'nro_control', displayName: 'Nº CONTROL', sort: { direction: uiGridConstants.DESC }, maxWidth: 140},
          { field: 'fecha', displayName: 'FECHA', maxWidth: 180, type:'Date', cellFilter: 'date:"dd/MM/yyyy"'},
          { field: 'cliente_nombre', displayName: 'CLIENTE', maxWidth: 140},
          { field: 'cliente_rif', displayName: 'RIF' },
          { field: 'observacion', displayName: 'OBSERVACION'},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null,function(row){
            vm.rowSel = row.entity;
          });
        },
        enableCellEditOnFocus: true,
        selectionRowHeaderWidth: 40,
        rowHeight: 40,
        rowStyle: function(row){
                if(row.entity.id_status > 1){
                  return 'row-null';
                }
              },
            rowTemplate : `<div ng-class="grid.options.rowStyle(row)"><div ng-mouseover="rowStyle={'background-color': (rowRenderIndex%2 == 1) ? '#F5F5F5' : '#F5F5F5','cursor':'pointer' };" 
               ng-mouseleave="rowStyle={}" ng-style="selectedStyle={'background-color': (row.isSelected) ? '#2196F3' : '','color': (row.isSelected) ? '#E6E6E6' : ''}">
                <div  ng-style="(!row.isSelected) ? rowStyle : selectedStyle" 
                    ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" 
                    ui-grid-one-bind-id-grid="rowRenderIndex + '-' + col.uid + '-cell'"
                    class="ui-grid-cell"   
                    ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" role="{{col.isRowHeader ? 'rowheader' : 'gridcell'}}" 
                    ui-grid-cell>
                </div>
            </div></div>`
    };

    activate();

    function activate(){
      //Pace.restart();

    }

    vm.getList = function(){        
      Pace.restart();
      vm.rowSel=null;
      if (vm.gridModal.data){
        vm.gridModal.data.length=0;
      }
      return vntOperacionesService.getList(logEmpresa.id, vm.id_tipo).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          vm.filtrar();
          return lista;
        }
      })
      Pace.stop();
    }

    vm.filtrar = function() {
      vm.gridModal.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.seleccionar = function(ev){
      Pace.restart();
        $mdDialog.hide(vm.rowSel);
      Pace.stop();
    }

    vm.cargarProductos=function(ev){
        $mdDialog.show({
          controller: 'modalCargaSelController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/movimientos/modals/modal.cargasel.tpl.html',
          parent: angular.element(document.body),
          multiple: true,
          locals:{
            headOperacion:vm.rowSel
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(listaSeleccionada){
            var obj={
              head:vm.rowSel,
              lista:listaSeleccionada 
            }
            $mdDialog.hide(obj);
            //$mdDialog.hide(listaSeleccionada);

        });
    }

  }

})();