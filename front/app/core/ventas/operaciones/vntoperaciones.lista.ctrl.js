(function(){
  'use strict';

  angular.module("ventasModule")
  .controller("vntoperacionesListaController", vntoperacionesListaController);
  vntoperacionesListaController.$inject=['$rootScope', '$scope', '$filter', '$state', '$mdDialog', 'uiGridConstants', 'sisOperacionesService', 'sisListasService', 'vntOperacionesService', 'CONFIG'];

  function vntoperacionesListaController($rootScope, $scope, $filter, $state, $mdDialog, uiGridConstants, sisOperacionesService, sisListasService, vntOperacionesService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));

    var vm=this;
    vm.filtro="";
    vm.id_tipo=0;
    vm.list=[];
    vm.full=true;
    vm.listaOperaciones=[];

    var lista=[];
    var _listaOperaciones=[];
    var _listaTipos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'nro_control', displayName: 'Nº CONTROL', sort: { direction: uiGridConstants.DESC }, maxWidth: 150},
          { field: 'fecha', displayName: 'FECHA', maxWidth: 140, type:'Date', cellFilter: 'date:"dd/MM/yyyy"'},
          { field: 'cliente_nombre', displayName: 'CLIENTE'},
          { field: 'cliente_rif', displayName: 'RIF', maxWidth: 140 },
          { field: 'observacion', displayName: 'OBSERVACION'},
          { field: 'monto_total_moneda', displayName: 'MONTO', cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/ventas/operaciones/vntoperaciones.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          
        rowStyle: function(row){
                if(row.entity.id_status > 1){
                  return 'row-null';
                }
              },
            rowTemplate : `<div ng-class="grid.options.rowStyle(row)"><div ng-mouseover="rowStyle={'background-color': (rowRenderIndex%2 == 1) ? '#F5F5F5' : '#F5F5F5','cursor':'pointer' };" 
               ng-mouseleave="rowStyle={}" ng-style="selectedStyle={'background-color': (row.isSelected) ? '#2196F3' : '','color': (row.isSelected) ? '#E6E6E6' : ''}">
                <div  ng-style="(!row.isSelected) ? rowStyle : selectedStyle" 
                    ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" 
                    ui-grid-one-bind-id-grid="rowRenderIndex + '-' + col.uid + '-cell'"
                    class="ui-grid-cell"   
                    ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" role="{{col.isRowHeader ? 'rowheader' : 'gridcell'}}" 
                    ui-grid-cell>
                </div>
            </div></div>`

    };

    vm.getList = function(){        
      Pace.restart();
      if (vm.gridOptions.data){
        vm.gridOptions.data.length=0;
      }
      return vntOperacionesService.getList(logEmpresa.id, vm.id_tipo).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          vm.filtrar();
          return lista;
        }
      })
      Pace.stop();
    }

    activate();

    $scope.$watch('$root.factor', function() {
        calcularMoneda();
    });

    function calcularMoneda(){
      console.log('calcularMoneda',$rootScope.factor)
      var factor=$rootScope.factor;
      angular.forEach(lista, function(item, index) {
        item.monto_total_moneda=item.monto_total*factor;
      });
    }

    function activate(){
      Pace.restart();
      //OPERACIONES
      sisOperacionesService.getList('11E7C39A0EACD9A08F1A00E04C6F7E24').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaOperaciones=res.data.response.datos;
          vm.listaOperaciones=_listaOperaciones;
        }
      })

      vm.getList();
    }




    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      var one={
        id:0,
        id_empresa:logEmpresa.id,
        id_tipo:vm.id_tipo
      }
      return vntOperacionesService.getOne(one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          /*vntOperacionesService.one={};
          angular.copy(res.data.response.datos,vntOperacionesService.one)*/
          var obj=res.data.response.datos;
          sessionStorage.setItem("one",JSON.stringify(obj));
          $state.go('config.ventas-operaciones-detail');            
        }
      })
    }

    vm.selOne = function(item,ev){
      var one={
        id:item.id,
        id_empresa:logEmpresa.id,
        id_tipo:vm.id_tipo
      }
      return vntOperacionesService.getOne(one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          /*vntOperacionesService.one={};
          angular.copy(res.data.response.datos,vntOperacionesService.one)*/
          var obj=res.data.response.datos;
          sessionStorage.setItem("one",JSON.stringify(obj));
          $state.go('config.ventas-operaciones-detail');            
        }
      })
    }

  }

})();