(function(){
  'use strict';

  angular.module("ventasModule")
  .controller("clientesListaController", clientesListaController);
  clientesListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'vntClientesService', 'sisListasService', 'CONFIG'];

  function clientesListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, vntClientesService, sisListasService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaBancos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'rif', displayName: 'RIF', maxWidth: 200},
          { field: 'deuda', displayName: 'DEUDA', cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/ventas/clientes/clientes.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      //STATUS
      /*
      sisListasService.getByCampo('bancos').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaBancos=res.data.response.datos;
        }
      })
      */

      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return vntClientesService.getList(logEmpresa.id)
        .then(function(res){
          console.log('res',res)
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return vntClientesService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vntClientesService.one={};
          angular.copy(res.data.response.datos,vntClientesService.one)
          vntClientesService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return vntClientesService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vntClientesService.one={};
          angular.copy(res.data.response.datos,vntClientesService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'clientesDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/ventas/clientes/clientes.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Cliente guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();