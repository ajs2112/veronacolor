(function(){
  'use strict';

  angular.module("reportesModule")
  .controller("inventarioReportesController", inventarioReportesController);
  inventarioReportesController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'repInventariosService', 'invCategoriasService', 'CONFIG'];

  function inventarioReportesController($rootScope, $filter, $state, $mdDialog, uiGridConstants, repInventariosService, invCategoriasService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.idReporte="1";
    vm.idCategoria="";
    vm.listaCategorias=[];
    vm.listaProductos=[];
    vm.activeItem={};
    vm.usa_fecha=false;
    vm.usa_productos=false;

    var d = new Date();
    d.setMonth(d.getMonth() - 1);
    vm.desde= d;
    vm.hasta= new Date();


    var lista=[];
    var _listaCategorias=[];
    var _listaProductos=[];


    activate();

    function activate(){
      Pace.restart();
      //CATEGORIAS
      invCategoriasService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaCategorias=res.data.response.datos;
          vm.listaCategorias=_listaCategorias;
        }
      })

    }

    vm.filtrar = function() {
      vm.listaProductos = $filter('multiFiltro')(_listaProductos, vm.filtro);
    };
    
    vm.selItem=function(item,event){
      vm.activeItem=item;
    }

    vm.getProductos=function(){
      var idCategoria='X';
      if (vm.idCategoria!="TODAS"){
        idCategoria=vm.idCategoria;
      }

      repInventariosService.getList(logEmpresa.id,idCategoria).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaProductos=res.data.response.datos;
          vm.filtrar();
        }
      })

    }

    vm.selOptReport=function(){
      switch (vm.idReporte){
        case '1':
          //LISTA EXISTENCIAS
          vm.usa_fecha=false;
          vm.usa_productos=false;
          break;
        case '2':
          //LISTA PRECIOS    
          vm.usa_fecha=false;
          vm.usa_productos=false;
          break;
        case '3':
          //MOVIMIENTOS RESUMEN
          vm.usa_fecha=true;
          vm.usa_productos=false;
          break;
        case '4':
          //MOVIMIENTOS DETALLE
          vm.usa_fecha=true;
          vm.usa_productos=true;
          break;
        case '5':
          //LISTA PRECIOS    
          vm.usa_fecha=false;
          vm.usa_productos=false;
          break;                  
      }
    }

    vm.selectReport = function() {
      //vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
      switch (vm.idReporte){
        case '1':
          repExistencia();
          break;
        case '2':
          repPrecios();        
          break;
        case '3':
          repMovimientosResumen();
          break;        
        case '4':
          repMovimientosDetalle();
          break;                
        case '5':
          repInventarioMonetario();
          break;                
      }  
    };

    /*********************************************************************************************************************************/
    /********************************************************** EXISTENCIAS **********************************************************/
    /*********************************************************************************************************************************/
    function repExistencia(){
      vm.full=false;
      var idCat="";
      if (vm.idCategoria!='x') idCat=vm.idCategoria;

      repInventariosService.getList(logEmpresa.id,idCat).then(function(res){
        console.log('res repInventariosService')
        console.log(res)
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;


          //DO ROWS
          var doRows = function(data){
            var miCat=0;
            for (var i = 0; i < data.length; i++) {

              if (miCat!==data[i]["id_categoria"]){
                miCat=data[i]["id_categoria"];
                var numero=0;
              }

                numero+=1;
                data[i].numero=numero;
                data[i].total_existencia=$filter('currency')(Number(data[i].total_existencia), '');
            }

            return data;
          }

          var logoA = new Image();

          //CONFIGURAR COLUMNAS
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "codigo"},
              {title: "DESCRIPCION", dataKey: "nombre"}, 
              {title: "UNIDAD", dataKey: "unidad_venta_nombre"},
              {title: "EXISTENCIA", dataKey: "total_existencia"},
          ];

          //ASIGNACION
          var rows = doRows(lista);
          var doc = new jsPDF('p', 'pt', 'letter');

          var centerText = function(texto, alto){
            var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
            doc.text(texto, xOffset, alto);        
          }

          var doc = new jsPDF('l', 'pt', 'letter');
          //console.log(doc)
          var totalPagesExp = "{total_pages_count_string}";

          
          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){


              // HEADER ***************************************************************************************************
              doc.setFontStyle('normal');
              /*EMPRESA*/                
              doc.addImage(logoA , 'png', 20, 15, 90, 50);
              doc.setFontSize(10);
              doc.text(logEmpresa.nombre,32,75);
              doc.setFontSize(9);
              doc.text(logEmpresa.rif,32,88);

              doc.setFontSize(10);

              var laFecha=new Date();
              var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

              doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

              doc.setFontSize(16);
              centerText('INVENTARIO DE PRODUCTOS',110);                                                

              doc.setFontSize(10);


              // FOOTER ***************************************************************************************************
              var pageContent = function (data) {
                  var str = "Page " + data.pageCount;
                  if (typeof doc.putTotalPages === 'function') {
                      str = str + " of " + totalPagesExp;
                  }

              };

              // CONTENIDO ***************************************************************************************************
              var miCategoria=0;
              var miY=0;
              doc.autoTable.previous=false;
              for (var i = 0; i < rows.length; i++) {
                if (miCategoria!==rows[i]["id_categoria"]){
                  miCategoria=rows[i]["id_categoria"];

                  var filtroLista={};
                  filtroLista.id_categoria=rows[i]["id_categoria"];           
                  var filas = $filter('filter')(rows, filtroLista, true);

                  //SEPARADOR DE TABLA CATEGORIA
                  doc.setFontSize(12);
                  if (!doc.autoTable.previous){
                    miY=150;
                    doc.text(40, miY, rows[i]["categoria_nombre"])
                  } else {
                    miY=doc.autoTable.previous.finalY + 30;
                    doc.text(40, miY, rows[i]["categoria_nombre"])                  
                  }

                  doc.autoTable(columns, filas, {
                      addPageContent: pageContent,
                      startY: miY + 20,
                      theme: 'grid',                  
                      margin: {top: 170, bottom: 40}, 
                      theme: 'grid',
                      headerStyles: {
                        fillColor: 255,
                        textColor: 0,
                        lineWidth: 1,
                        fontSize: 9,
                      },
                      bodyStyles: {fontSize: 9},
                      columnStyles: {
                          numero: {columnWidth: 50, halign: 'right'},
                          codigo: {columnWidth: 70, halign: 'left'},
                          unidad_venta_nombre: {columnWidth: 70, halign: 'left'},
                          total_existencia: {columnWidth: 70, halign: 'right'},
                      },
                  });
                }   
              }

              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === 'function') {
                  doc.putTotalPages(totalPagesExp);
              }


              // CIERRE ***************************************************************************************************
              var blob= doc.output("blob");
              window.open(URL.createObjectURL(blob));
              vm.full=true;
          }
        }
      })          
    }


    /*********************************************************************************************************************************/
    /************************************************************ PRECIOS ************************************************************/
    /*********************************************************************************************************************************/
    function repPrecios(){
      vm.full=false;
      var idCat="";
      if (vm.idCategoria!='x') idCat=vm.idCategoria;

      repInventariosService.getList(logEmpresa.id,idCat).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          //DO ROWS
          var doRows = function(data){
            var miCat=0;
            for (var i = 0; i < data.length; i++) {

              if (miCat!==data[i]["id_categoria"]){
                miCat=data[i]["id_categoria"];
                var numero=0;
              }

                numero+=1;
                data[i].numero=numero;
                data[i].precio_venta=$filter('currency')(Number(data[i].precio_venta), '');
            }

            return data;
          }

          var logoA = new Image();

          //CONFIGURAR COLUMNAS
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "codigo"},
              {title: "DESCRIPCION", dataKey: "nombre"}, 
              {title: "UNIDAD", dataKey: "unidad_venta_nombre"},
              {title: "PRECIO", dataKey: "precio_venta"},
          ];

          //ASIGNACION
          var rows = doRows(lista);
          var doc = new jsPDF('p', 'pt', 'letter');


          var centerText = function(texto, alto){
            var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
            doc.text(texto, xOffset, alto);        
          }

          var doc = new jsPDF('l', 'pt', 'letter');
          //console.log(doc)
          var totalPagesExp = "{total_pages_count_string}";

          
          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){


              // HEADER ***************************************************************************************************
              doc.setFontStyle('normal');
              /*EMPRESA*/                
              doc.addImage(logoA , 'png', 20, 15, 90, 50);
              doc.setFontSize(10);
              doc.text(logEmpresa.nombre,32,75);
              doc.setFontSize(9);
              doc.text(logEmpresa.rif,32,88);

              doc.setFontSize(10);

              var laFecha=new Date();
              var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

              doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

              doc.setFontSize(16);
              centerText('LISTA DE PRECIOS',110);                                                

              doc.setFontSize(10);


              // FOOTER ***************************************************************************************************
              var pageContent = function (data) {
                  var str = "Page " + data.pageCount;
                  if (typeof doc.putTotalPages === 'function') {
                      str = str + " of " + totalPagesExp;
                  }

              };

              // CONTENIDO ***************************************************************************************************
              var miCategoria=0;
              var miY=0;
              doc.autoTable.previous=false;
              for (var i = 0; i < rows.length; i++) {
                if (miCategoria!==rows[i]["id_categoria"]){
                  miCategoria=rows[i]["id_categoria"];

                  var filtroLista={};
                  filtroLista.id_categoria=rows[i]["id_categoria"];           
                  var filas = $filter('filter')(rows, filtroLista, true);

                  //SEPARADOR DE TABLA CATEGORIA
                  doc.setFontSize(12);
                  if (!doc.autoTable.previous){
                    miY=150;
                    doc.text(40, miY, rows[i]["categoria_nombre"])
                  } else {
                    miY=doc.autoTable.previous.finalY + 30;
                    doc.text(40, miY, rows[i]["categoria_nombre"])                  
                  }

                  doc.autoTable(columns, filas, {
                      addPageContent: pageContent,
                      startY: miY + 20,
                      theme: 'grid',                  
                      margin: {top: 170, bottom: 40}, 
                      theme: 'grid',
                      headerStyles: {
                        fillColor: 255,
                        textColor: 0,
                        lineWidth: 1,
                        fontSize: 9,
                      },
                      bodyStyles: {fontSize: 9},
                      columnStyles: {
                          numero: {columnWidth: 50, halign: 'right'},
                          codigo: {columnWidth: 70, halign: 'left'},
                          unidad_venta_nombre: {columnWidth: 70, halign: 'left'},
                          precio_venta: {columnWidth: 80, halign: 'right'},
                      },
                  });
                }   
              }

              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === 'function') {
                  doc.putTotalPages(totalPagesExp);
              }


              // CIERRE ***************************************************************************************************
              var blob= doc.output("blob");
              window.open(URL.createObjectURL(blob));
              vm.full=true;
          }
        }
      })          
    }    
    

    /*********************************************************************************************************************************/
    /********************************************** DISPONIBILIDAD FORMULAS **********************************************************/
    /*********************************************************************************************************************************/
    function repDisponibilidadFormulas(){
        vm.full=false;

        repInventariosService.getDisponibilidadFormulas(logEmpresa.id).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;

            //DO ROWS
            var doRows = function(data){
              var miCat=0;
              for (var i = 0; i < data.length; i++) {

                if (miCat!==data[i]["id_categoria"]){
                  miCat=data[i]["id_categoria"];
                  var numero=0;
                }

                  numero+=1;
                  data[i].numero=numero;
                  data[i].total_existencia=$filter('currency')(Number(data[i].total_existencia), '');
                  data[i].cantidad_disponible=$filter('currency')(Number(data[i].cantidad_disponible), '');
              }

              return data;
            }

            var logoA = new Image();

            //CONFIGURAR COLUMNAS
            var columns = [
                {title: "Nº", dataKey: "numero"},
                {title: "CODIGO", dataKey: "codigo"},
                {title: "DESCRIPCION", dataKey: "nombre"}, 
                {title: "UNIDAD", dataKey: "unidad_nombre"},
                {title: "EXISTENCIA", dataKey: "total_existencia"},
                {title: "DISPONIBILIDAD", dataKey: "cantidad_disponible"},
            ];

            //ASIGNACION
            var rows = doRows(lista);
            var doc = new jsPDF('p', 'pt', 'letter');


            var centerText = function(texto, alto){
              var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
              doc.text(texto, xOffset, alto);        
            }

            var doc = new jsPDF('l', 'pt', 'letter');
            //console.log(doc)
            var totalPagesExp = "{total_pages_count_string}";

            
            logoA.src = 'front/assets/img/logo2.png';
            logoA.onload = function(){


                // HEADER ***************************************************************************************************
                doc.setFontStyle('normal');
                doc.addImage(logoA , 'png', 50, 20, 80, 50);
                doc.setFontSize(10);
                doc.text("RIF: J407889257",50,80);

                doc.setFontSize(10);

                var laFecha=new Date();
                var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

                doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

                doc.setFontSize(16);
                centerText('DISPONIBILIDAD DE FABRICACION',110);                                                

                doc.setFontSize(10);


                // FOOTER ***************************************************************************************************
                var pageContent = function (data) {
                    var str = "Page " + data.pageCount;
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + " of " + totalPagesExp;
                    }

                };

                // CONTENIDO ***************************************************************************************************
                var miCategoria=0;
                var miY=0;
                doc.autoTable.previous=false;
                for (var i = 0; i < rows.length; i++) {
                  if (miCategoria!==rows[i]["id_categoria"]){
                    miCategoria=rows[i]["id_categoria"];

                    var filtroLista={};
                    filtroLista.id_categoria=rows[i]["id_categoria"];           
                    var filas = $filter('filter')(rows, filtroLista, true);

                    //SEPARADOR DE TABLA CATEGORIA
                    doc.setFontSize(12);
                    if (!doc.autoTable.previous){
                      miY=150;
                      doc.text(40, miY, rows[i]["categoria_nombre"])
                    } else {
                      miY=doc.autoTable.previous.finalY + 30;
                      doc.text(40, miY, rows[i]["categoria_nombre"])                  
                    }

                    doc.autoTable(columns, filas, {
                        addPageContent: pageContent,
                        startY: miY + 20,
                        margin: {top: 170, bottom: 40}, 
                        theme: 'grid',
                        headerStyles: {
                          fillColor: 255,
                          textColor: 0,
                          lineWidth: 1,
                          fontSize: 9,
                        },
                        bodyStyles: {fontSize: 9},
                        columnStyles: {
                            numero: {columnWidth: 50, halign: 'right'},
                            codigo: {columnWidth: 70, halign: 'left'},
                            unidad_nombre: {columnWidth: 70, halign: 'left'},
                            total_existencia: {columnWidth: 80, halign: 'right'},
                            cantidad_disponible: {columnWidth: 80, halign: 'right'},
                        },
                    });
                  }   
                }

                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp);
                }


                // CIERRE ***************************************************************************************************
                var blob= doc.output("blob");
                window.open(URL.createObjectURL(blob));
                vm.full=true;
            }

          }
        })
    }


    /*********************************************************************************************************************************/
    /**************************************************** MOVIMIENTO RESUMEN *********************************************************/
    /*********************************************************************************************************************************/
    function repMovimientosResumen(){
      var idCat="";
      if (vm.idCategoria!='x') idCat=vm.idCategoria;
      var obj={
        idEmpresa:logEmpresa.id,
        idCategoria:idCat,
        desde:vm.desde,
        hasta:vm.hasta
      }

      vm.full=false;

      repInventariosService.getMovimientosResumen(obj).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          //DO ROWS
          var doRows = function(data){
            var miCat=0;
            for (var i = 0; i < data.length; i++) {

              if (miCat!==data[i]["id_categoria"]){
                miCat=data[i]["id_categoria"];
                var numero=0;
              }

                numero+=1;
                data[i].numero=numero;
                //data[i].precio_venta_final=$filter('currency')(Number(data[i].precio_venta_final), '');
                data[i].total_entrada=$filter('number')(data[i].total_entrada, 2);
                data[i].total_neutro=$filter('number')(data[i].total_neutro, 2);
                data[i].total_salida=$filter('number')(data[i].total_salida, 2);
            }

            return data;
          }

          var logoA = new Image();

          //CONFIGURAR COLUMNAS
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "producto_codigo"},
              {title: "DESCRIPCION", dataKey: "producto_nombre"}, 
              {title: "UNIDAD", dataKey: "producto_unidad_venta"},
              {title: "ENTRADA", dataKey: "total_entrada"},
              {title: "NEUTRO", dataKey: "total_neutro"},
              {title: "SALIDA", dataKey: "total_salida"},
          ];

          //ASIGNACION
          var rows = doRows(lista);
          var doc = new jsPDF('p', 'pt', 'letter');


          var centerText = function(texto, alto){
            var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
            doc.text(texto, xOffset, alto);        
          }

          var doc = new jsPDF('l', 'pt', 'letter');
          //console.log(doc)
          var totalPagesExp = "{total_pages_count_string}";

          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){


              // HEADER ***************************************************************************************************
              doc.setFontStyle('normal');
              /*EMPRESA*/                
              doc.addImage(logoA , 'png', 20, 15, 90, 50);
              doc.setFontSize(10);
              doc.text(logEmpresa.nombre,32,75);
              doc.setFontSize(9);
              doc.text(logEmpresa.rif,32,88);

              doc.setFontSize(10);

              var laFecha=new Date();
              var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

              doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

              doc.setFontSize(16);
              centerText('RESUMEN DE MOVIMIENTOS',110);                                                

              doc.setFontSize(10);


              // FOOTER ***************************************************************************************************
              var pageContent = function (data) {
                  var str = "Page " + data.pageCount;
                  if (typeof doc.putTotalPages === 'function') {
                      str = str + " of " + totalPagesExp;
                  }

              };

              // CONTENIDO ***************************************************************************************************
              var miCategoria=0;
              var miY=0;
              doc.autoTable.previous=false;
              for (var i = 0; i < rows.length; i++) {
                if (miCategoria!==rows[i]["id_categoria"]){
                  miCategoria=rows[i]["id_categoria"];

                  var filtroLista={};
                  filtroLista.id_categoria=rows[i]["id_categoria"];           
                  var filas = $filter('filter')(rows, filtroLista, true);

                  //SEPARADOR DE TABLA CATEGORIA
                  doc.setFontSize(12);
                  if (!doc.autoTable.previous){
                    miY=150;
                    doc.text(40, miY, rows[i]["categoria_nombre"])
                  } else {
                    miY=doc.autoTable.previous.finalY + 30;
                    doc.text(40, miY, rows[i]["categoria_nombre"])                  
                  }

                  doc.autoTable(columns, filas, {
                      addPageContent: pageContent,
                      startY: miY + 20,
                      theme: 'grid',                  
                      margin: {top: 170, bottom: 40}, 
                      theme: 'grid',
                      headerStyles: {
                        fillColor: 255,
                        textColor: 0,
                        lineWidth: 1,
                        fontSize: 9,
                      },
                      bodyStyles: {fontSize: 9},
                      columnStyles: {
                          numero: {columnWidth: 30, halign: 'right'},
                          producto_codigo: {columnWidth: 70, halign: 'left'},
                          producto_unidad_venta: {columnWidth: 60, halign: 'left'},
                          total_entrada: {columnWidth: 70, halign: 'right'},
                          total_neutro: {columnWidth: 70, halign: 'right'},
                          total_salida: {columnWidth: 70, halign: 'right'},
                      },
                  });
                }   
              }

              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === 'function') {
                  doc.putTotalPages(totalPagesExp);
              }


              // CIERRE ***************************************************************************************************
              var blob= doc.output("blob");
              window.open(URL.createObjectURL(blob));
              vm.full=true;
          }


        }
      })

    }


    /*********************************************************************************************************************************/
    /****************************************************** MOVIMIENTO DETALLADO *****************************************************/
    /*********************************************************************************************************************************/
    function repMovimientosDetalle(){
      var obj={
        idEmpresa:logEmpresa.id,
        idProducto:vm.activeItem.id,
        desde:vm.desde,
        hasta:vm.hasta
      }

      vm.full=false;

      repInventariosService.getMovimientosDetalle(obj).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;


          //DO ROWS
          var doRows = function(data){
            var numero=0;
            for (var i = 0; i < data.length; i++) {
                numero+=1;
                data[i].numero=numero;
                data[i].fecha=new Date(data[i].fecha);
                data[i].fecha=$filter('date')(data[i].fecha, 'dd/MM/yyyy');

                
                data[i].total_entrada=$filter('number')(data[i].total_entrada, 2);
                data[i].total_neutro=$filter('number')(data[i].total_neutro, 2);
                data[i].total_salida=$filter('number')(data[i].total_salida, 2);
            }

            return data;
          }

          var logoA = new Image();

          //CONFIGURAR COLUMNAS
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "FECHA", dataKey: "fecha"},
              {title: "TIPO", dataKey: "tipo_nombre"},
              {title: "OPERACION", dataKey: "operacion_nombre"}, 
              {title: "Nº CONTROL", dataKey: "nro_control"},
              {title: "ENTRADA", dataKey: "total_entrada"},
              {title: "NEUTRO", dataKey: "total_neutro"},
              {title: "SALIDA", dataKey: "total_salida"},
          ];

          //ASIGNACION
          var rows = doRows(lista);
          var doc = new jsPDF('p', 'pt', 'letter');


          var centerText = function(texto, alto){
            var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
            doc.text(texto, xOffset, alto);        
          }

          var doc = new jsPDF('l', 'pt', 'letter');
          //console.log(doc)
          var totalPagesExp = "{total_pages_count_string}";

          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){


              // HEADER ***************************************************************************************************
              doc.setFontStyle('normal');
              doc.setTextColor(0,0,0);
              /*EMPRESA*/                
              doc.addImage(logoA , 'png', 20, 15, 90, 50);
              doc.setFontSize(10);
              doc.text(logEmpresa.nombre,32,75);
              doc.setFontSize(9);
              doc.text(logEmpresa.rif,32,88);

              doc.setFontSize(10);

              var laFecha=new Date();
              var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

              doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

              doc.setFontSize(16);

              centerText('RESUMEN DE MOVIMIENTOS',110);

              doc.setFontSize(12);
              doc.text(vm.activeItem.nombre,40,136);                                                
              doc.setFontSize(9);
              doc.text(vm.activeItem.unidad_venta_nombre,40,148);
              doc.text(vm.activeItem.codigo,40,160);


              // FOOTER ***************************************************************************************************
              var pageContent = function (data) {
                  var str = "Page " + data.pageCount;
                  if (typeof doc.putTotalPages === 'function') {
                      str = str + " of " + totalPagesExp;
                  }

              };

              // CONTENIDO ***************************************************************************************************
              var miY=160;
              doc.autoTable.previous=false;
              for (var i = 0; i < rows.length; i++) {
                  //SEPARADOR DE TABLA CATEGORIA
                  doc.setFontSize(12);

                  doc.autoTable(columns, rows, {
                      addPageContent: pageContent,
                      startY: miY + 20,
                      theme: 'grid',                  
                      margin: {top: 170, bottom: 40}, 
                      theme: 'grid',
                      headerStyles: {
                        fillColor: 255,
                        textColor: 0,
                        lineWidth: 1,
                        fontSize: 9,
                      },
                      bodyStyles: {fontSize: 9},
                      columnStyles: {
                          numero: {columnWidth: 30, halign: 'right'},
                          fecha: {columnWidth: 70, halign: 'left'},
                          tipo: {columnWidth: 70, halign: 'left'},
                          operacion: {columnWidth: 70, halign: 'left'},
                          nro_control: {columnWidth: 70, halign: 'left'},
                          total_entrada: {columnWidth: 70, halign: 'right'},
                          total_neutro: {columnWidth: 70, halign: 'right'},
                          total_salida: {columnWidth: 70, halign: 'right'},
                      },
                  });
              }

              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === 'function') {
                  doc.putTotalPages(totalPagesExp);
              }


              // CIERRE ***************************************************************************************************
              var blob= doc.output("blob");
              window.open(URL.createObjectURL(blob));
              vm.full=true;
          }




        }
      })
    }


    /*********************************************************************************************************************************/
    /***************************************************** INVENTARIO MONETARIO ******************************************************/
    /*********************************************************************************************************************************/
    function repInventarioMonetario(){
      vm.full=false;
      var idCat="";
      if (vm.idCategoria!='x') idCat=vm.idCategoria;

      repInventariosService.getList(logEmpresa.id,idCat).then(function(res){
        console.log('res repInventariosService')
        console.log(res)
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;


          //DO ROWS
          var doRows = function(data){
            var miCat=0;
            for (var i = 0; i < data.length; i++) {

              if (miCat!==data[i]["id_categoria"]){
                miCat=data[i]["id_categoria"];
                var numero=0;
              }

                numero+=1;
                data[i].numero=numero;
                var _monto_producto=Number(data[i].total_existencia)*Number(data[i].dolar_costo_dolar);
                data[i].total_existencia=$filter('currency')(Number(data[i].total_existencia), '');
                data[i].monto_producto=$filter('currency')(Number(_monto_producto),'');
            }

            return data;
          }

          var logoA = new Image();

          //CONFIGURAR COLUMNAS
          var columns = [
              {title: "Nº", dataKey: "numero"},
              {title: "CODIGO", dataKey: "codigo"},
              {title: "DESCRIPCION", dataKey: "nombre"}, 
              {title: "UNIDAD", dataKey: "unidad_venta_nombre"},
              {title: "EXISTENCIA", dataKey: "total_existencia"},
              {title: "MONTO", dataKey: "monto_producto"},
          ];

          //ASIGNACION
          var rows = doRows(lista);
          var doc = new jsPDF('p', 'pt', 'letter');

          var centerText = function(texto, alto){
            var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
            doc.text(texto, xOffset, alto);        
          }

          var doc = new jsPDF('l', 'pt', 'letter');
          //console.log(doc)
          var totalPagesExp = "{total_pages_count_string}";

          
          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){


              // HEADER ***************************************************************************************************
              doc.setFontStyle('normal');
              /*EMPRESA*/                
              doc.addImage(logoA , 'png', 20, 15, 90, 50);
              doc.setFontSize(10);
              doc.text(logEmpresa.nombre,32,75);
              doc.setFontSize(9);
              doc.text(logEmpresa.rif,32,88);

              doc.setFontSize(10);

              var laFecha=new Date();
              var miFecha=$filter('date')(laFecha, 'dd/MM/yyyy');

              doc.text("FECHA: "+ miFecha,doc.internal.pageSize.width -150,60);

              doc.setFontSize(16);
              centerText('INVENTARIO DE PRODUCTOS',110);                                                

              doc.setFontSize(10);


              // FOOTER ***************************************************************************************************
              var pageContent = function (data) {
                  var str = "Page " + data.pageCount;
                  if (typeof doc.putTotalPages === 'function') {
                      str = str + " of " + totalPagesExp;
                  }

              };

              // CONTENIDO ***************************************************************************************************
              var miCategoria=0;
              var miY=0;
              doc.autoTable.previous=false;
              for (var i = 0; i < rows.length; i++) {
                if (miCategoria!==rows[i]["id_categoria"]){
                  miCategoria=rows[i]["id_categoria"];

                  var filtroLista={};
                  filtroLista.id_categoria=rows[i]["id_categoria"];           
                  var filas = $filter('filter')(rows, filtroLista, true);

                  //SEPARADOR DE TABLA CATEGORIA
                  doc.setFontSize(12);
                  if (!doc.autoTable.previous){
                    miY=150;
                    doc.text(40, miY, rows[i]["categoria_nombre"])
                  } else {
                    miY=doc.autoTable.previous.finalY + 30;
                    doc.text(40, miY, rows[i]["categoria_nombre"])                  
                  }

                  doc.autoTable(columns, filas, {
                      addPageContent: pageContent,
                      startY: miY + 20,
                      theme: 'grid',                  
                      margin: {top: 170, bottom: 40}, 
                      theme: 'grid',
                      headerStyles: {
                        fillColor: 255,
                        textColor: 0,
                        lineWidth: 1,
                        fontSize: 9,
                      },
                      bodyStyles: {fontSize: 9},
                      columnStyles: {
                          numero: {columnWidth: 30, halign: 'right'},
                          codigo: {columnWidth: 60, halign: 'left'},
                          unidad_venta_nombre: {columnWidth: 70, halign: 'left'},
                          total_existencia: {columnWidth: 70, halign: 'right'},
                          monto_producto: {columnWidth: 100, halign: 'right'},
                      },
                  });
                }   
              }

              // Total page number plugin only available in jspdf v1.0+
              if (typeof doc.putTotalPages === 'function') {
                  doc.putTotalPages(totalPagesExp);
              }


              // CIERRE ***************************************************************************************************
              var blob= doc.output("blob");
              window.open(URL.createObjectURL(blob));
              vm.full=true;
          }
        }
      })          
    }


  }
})();