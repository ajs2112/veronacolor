(function(){
  'use strict';
  
  angular.module("comprasModule")
  .controller("proveedoresDetailController", proveedoresDetailController);
  proveedoresDetailController.$inject=['$mdDialog', '$scope', 'cmpProveedoresService', 'bancos'];

  function proveedoresDetailController($mdDialog, $scope, cmpProveedoresService, bancos){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.listaBancos=bancos;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=cmpProveedoresService.one;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      Pace.restart();
      vm.full=false;
      vm.one.id_empresa=logEmpresa.id;
      cmpProveedoresService.setOne(vm.one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide(res.data.response.datos);
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 
  }

})();