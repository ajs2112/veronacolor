(function(){
  'use strict';

  angular.module("comprasModule")
  .controller("proveedoresListaController", proveedoresListaController);
  proveedoresListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'cmpProveedoresService', 'sisListasService', 'CONFIG'];

  function proveedoresListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, cmpProveedoresService, sisListasService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaBancos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'rif', displayName: 'RIF', maxWidth: 200},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/compras/proveedores/proveedores.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      //BANCOS
      sisListasService.getByCampo('bancos').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaBancos=res.data.response.datos;
        }
      })

      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return cmpProveedoresService.getList(logEmpresa.id)
        .then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return cmpProveedoresService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cmpProveedoresService.one={};
          angular.copy(res.data.response.datos,cmpProveedoresService.one)
          cmpProveedoresService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return cmpProveedoresService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          cmpProveedoresService.one={};
          angular.copy(res.data.response.datos,cmpProveedoresService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'proveedoresDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/compras/proveedores/proveedores.detail.tpl.html',
          locals: {
            bancos: listaBancos
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Proveedor guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();