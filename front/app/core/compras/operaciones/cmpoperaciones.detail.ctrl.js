(function(){
  'use strict';
  
  angular.module("comprasModule")
  .controller("cmpoperacionesDetailController", cmpoperacionesDetailController);
  cmpoperacionesDetailController.$inject=['$state', '$scope', '$mdDialog', '$rootScope', '$filter', 'sisListasService', 'invAlmacenesService', 'invProductosService', 'sisUnidadesService', 'invMovimientosService', 'cmpProveedoresService', 'cmpOperacionesService', 'CONFIG'];

  function cmpoperacionesDetailController($state, $scope, $mdDialog, $rootScope, $filter,  sisListasService, invAlmacenesService, invProductosService, sisUnidadesService, invMovimientosService, cmpProveedoresService, cmpOperacionesService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));

    var vm=this;
    vm.one = {};
    vm.full=true;
    vm.error="";
    vm.id_sistema="";

    vm.listaOperaciones=[];
    vm.listaAlmacenes=[];

    vm.cntProductos=0;

    vm.searchText=null;
    vm.selectedProveedor=null;    
    
    var lista=[];
    var _listaProductos=[];
    var _listaUnidades=[];
    var _listaProveedores=[];
    var _listaBancos=[];
    vm.tipoOperacion={};

    vm.gridDetail = {
        columnDefs: [
          { field: 'producto_codigo', displayName: 'CÓDIGO', maxWidth: 140, enableCellEdit: false},
          { field: 'producto_nombre', displayName: 'DESCRIPCION', enableCellEdit: false},
          { field: 'producto_unidad', displayName: 'UNIDAD', maxWidth: 140, enableCellEdit: false},
          { field: 'cantidad', displayName: 'CANTIDAD', maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.edit.on.beginCellEdit(null,function(rowEntity, colDef){
            vm.cntSel=1;
            vm.rowSel=rowEntity;
          });
        },
        enableCellEditOnFocus: true,
    };

    activate();

    function activate(){
      Pace.restart();
      if ($state.params.listaOperaciones){
        vm.listaOperaciones=$state.params.listaOperaciones;
      }

      //PRODUCTOS
      invProductosService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaProductos=res.data.response.datos;
        }
      })

      //UNIDADES
      sisUnidadesService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaUnidades=res.data.response.datos;
        }
      })

      //ALMACENES
      invAlmacenesService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaAlmacenes=res.data.response.datos;
        }
      })

      //PROVEEDORES
      cmpProveedoresService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaProveedores=res.data.response.datos;
          vm.selectedProveedor=selectById(_listaProveedores, vm.one.id_proveedor);
        }
      })      

      //BANCOS
      sisListasService.getByCampo('bancos').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaBancos=res.data.response.datos;
        }
      })

      vm.one=cmpOperacionesService.one;
      vm.one.fecha=new Date(vm.one.fecha);

      selTipoOperacion();

      return getList().then(function(){
        if (lista){
          vm.gridDetail.data = lista;
          vm.cntProductos=lista.length;          
        } else {
          lista=[];
        }

          Pace.stop();
      })

    }

    function getList (){        
        var obj={
          id_tipo:vm.one.id_tipo,
          id_operacion:vm.one.id 
        }
        return invMovimientosService.getList(obj).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    function selTipoOperacion(){
      var filtro={};
      filtro.id=vm.one.id_tipo;   
      var result = $filter('filter')(vm.listaOperaciones, filtro, true);
      if (result.length){
        vm.tipoOperacion=result[0];
        $rootScope.appSeccion=result[0].nombre + " (" + vm.one.nro_control + ")";
      }
    }

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      //SELECCIONAR TIPO OPERACION
      vm.one.id_usuario=CONFIG.logUser.id;
      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        detail:lista
      }
      cmpOperacionesService.setOne(obj).then(function(res){ 
        console.log(res)
        vm.one.id=res.data.response.datos;

        var one={
          id:vm.one.id,
          id_tipo:vm.one.id_tipo
        }
        cmpOperacionesService.getOne(one).then(function(res){
          vm.one=res.data.response.datos;
          vm.one.fecha=new Date(vm.one.fecha);
          vm.imprimir();
        })
        $rootScope.showToast('Operación registrada con exito');
        vm.full=true;
      })
    }

    vm.anular = function() {
      vm.full=false;
      Pace.restart();
      //SELECCIONAR TIPO OPERACION
      vm.one.id_usuario=CONFIG.logUser.id;
      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        //detail:lista
      }
      cmpOperacionesService.anularOne(obj).then(function(res){ 
          $rootScope.showToast('Operación anulada con exito');
          vm.full=true;
      })
    }     

    function agregar(listaSeleccionada){
      for (var i = 0; i < listaSeleccionada.length; i++) {
        if (listaSeleccionada[i].id_producto){
          var filtro={};
          filtro.id_producto=listaSeleccionada[i].id_producto;   
          var result = $filter('filter')(lista, filtro, true);
          if (result.length){
            $rootScope.showToast('Producto en lista');
          } else {
            lista.push(listaSeleccionada[i]);
          }
        } else {
          //return null
        }
      }

      var filtro={};
      filtro.id_producto_padre="00000000000000000000000000000000";   
      var result = $filter('filter')(lista, filtro, true);

      vm.gridDetail.data = result;
      vm.cntProductos=result.length;

    }

    vm.delProducto = function(ev){
      var sel=vm.rowSel;
      var indice = lista.indexOf(sel);
      lista.splice(indice,1);        
      vm.cntSel=0;
      if (!lista){
        lista=[];        
      } else {
        vm.gridDetail.data = lista;
        vm.cntProductos=lista.length;
      }
    }    

  /************************* PROVEEDOR *******************************/
    vm.querySearch = function( item ) {
      var result= $filter('filter')(_listaProveedores, item);
      return result;
    }

    vm.selProveedor=function(item){
      if (item){
        vm.one.id_proveedor=item.id;
      }
    }

    function selectById (lista, id) {
      var filtro={};
      filtro.id=id;   
      var result = $filter('filter')(lista, filtro, true);
      if (result){
        if (result.length){
          return result[0];
        } else {
          return null;
        }
      }
    }

    vm.modalProveedor=function(ev, idProveedor){
      cmpProveedoresService.getOne(idProveedor).then(function(res){
        cmpProveedoresService.one={};
        angular.copy(res.data.response.datos,cmpProveedoresService.one)

        $mdDialog.show({
          controller: 'proveedoresDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/compras/proveedores/proveedores.detail.tpl.html',
          locals: {
            bancos: _listaBancos
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(id){
          cmpProveedoresService.getList().then(function(res){
            _listaProveedores=res.data.response.datos;
            vm.selectedProveedor=selectById(_listaProveedores, id.toUpperCase());
          })    
        });

      })
    }

    /************************* DIALOGS *******************************/
    vm.cargar=function(ev){
        $mdDialog.show({
          controller: 'modalCargaComprasController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/compras/operaciones/cmpoperaciones.modal.carga.tpl.html',
          parent: angular.element(document.body),
          locals:{
            listaOperaciones:vm.listaOperaciones
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(documento){
            var obj={
              id_tipo:documento.id_tipo,
              id_operacion:documento.id 
            }
            invMovimientosService.getList(obj).then(function(res){
              if (res.data && res.data.code==0){
                localStorage.setItem("token",res.data.response.token);
                lista=res.data.response.datos;
                if (lista){
                  vm.gridDetail.data = lista;
                  vm.cntProductos=lista.length;          
                } else {
                  lista=[];
                }
              }
            })


          $rootScope.showToast('Documento cargado con exito');
        });
    }

    vm.openDialogProductos=function(ev){
        $mdDialog.show({
          controller: 'modalInventarioController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/movimientos/modals/modal.inventario.tpl.html',
          parent: angular.element(document.body),
          locals:{
            tipoOperacion: vm.tipoOperacion.id,
            listaProductos:_listaProductos,
            listaUnidades:_listaUnidades,
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(listaProductos){
          agregar(listaProductos);
          //$rootScope.showToast('Formula generada con exito');
        });
    }


    vm.imprimir=function(ev){
      console.log(vm.one)


        vm.full=false;
        //DO ROWS
        var doRows = function(data){
          var numero=0;
          for (var i = 0; i < data.length; i++) {
            numero+=1;
            data[i].numero=numero;
          }
          //console.log(data)
          return data;
        }


        var logoA = new Image();

        var columns = [
            {title: "Nº", dataKey: "numero"},
            {title: "CODIGO", dataKey: "producto_codigo"},
            {title: "DESCRIPCION", dataKey: "producto_nombre"}, 
            {title: "UNIDAD", dataKey: "producto_unidad"},
            {title: "CANTIDAD", dataKey: "cantidad"},
        ];
        var rows = doRows(lista);


            var centerText = function(texto, alto){
              var xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(texto) * doc.internal.getFontSize() / 2); 
              doc.text(texto, xOffset, alto);        
            }

            var doc = new jsPDF('p', 'pt', 'letter');
            //console.log(doc)
            var totalPagesExp = "{total_pages_count_string}";

            var pageContent = function (data) {
                // HEADER ***************************************************************************************************
                doc.setFontStyle('normal');
                //doc.addImage(logoB , 'png', (doc.internal.pageSize.width-data.settings.margin.right)-50, 20, 50, 50);
                
                /*EMPRESA*/                
                doc.addImage(logoA , 'png', 20, 15, 90, 50);
                doc.setFontSize(10);
                doc.text(logEmpresa.nombre,32,75);
                doc.setFontSize(9);
                doc.text(logEmpresa.rif,32,88);
                
                doc.setFontSize(10);
                var miFecha=$filter('date')(vm.one.fecha, 'dd/MM/yyyy');

                doc.text("FECHA:         "+ miFecha,440,60);
                doc.text("Nº CONTROL: "+ vm.one.nro_control,440,80);

                doc.text("PROVEEDOR: "+ vm.selectedProveedor.nombre,40,140);
                doc.text("RIF: "+ vm.selectedProveedor.rif,500,140);

                doc.setFontSize(14);
                centerText(vm.one.tipo_operacion_nombre,110);                                                


                // FOOTER ***************************************************************************************************
                var str = "Page " + data.pageCount;
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }

            };


          logoA.src = 'front/assets/img/logo2.png';
          logoA.onload = function(){
                doc.setFontSize(10);

                doc.autoTable(columns, rows, {
                    addPageContent: pageContent,
                    margin: {top: 170, bottom: 40}, 
                    theme: 'grid',
                    headerStyles: {
                      fillColor: 255,
                      textColor: 0,
                      lineWidth: 1,
                      fontSize: 9,
                    },
                    bodyStyles: {fontSize: 9},
                    columnStyles: {
                        numero: {columnWidth: 30, halign: 'right'},
                        producto_codigo: {columnWidth: 50, halign: 'left'},
                        producto_unidad: {columnWidth: 50, halign: 'left'},
                        cantidad: {columnWidth: 70, halign: 'right'},
                    },
                });

                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    doc.putTotalPages(totalPagesExp);
                }

                var blob= doc.output("blob");
                window.open(URL.createObjectURL(blob));
                vm.full=true;

          }

      


    }
  
  }

})();