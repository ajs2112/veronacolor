(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("productosListaController", productosListaController);
  productosListaController.$inject=['$rootScope', '$scope', '$filter', '$state', '$mdDialog', 'uiGridConstants', 'invProductosService', 'sisListasService', 'invCategoriasService', 'sisUnidadesService', 'sisImpuestosService', 'CONFIG'];

  function productosListaController($rootScope, $scope, $filter, $state, $mdDialog, uiGridConstants, invProductosService, sisListasService, invCategoriasService, sisUnidadesService, sisImpuestosService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];
    vm.listaCategorias= [];
    vm.id_categoria="";
    vm.full=true;

    var lista=[];
    var _listaTipos=[];
    var _listaCategorias=[];
    var _listaSistemas=[];
    var _listaUnidades=[];
    var _listaImpuestos=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'tipo_nombre', displayName: 'TIPO', sort: { direction: uiGridConstants.ASC }, maxWidth: 140},
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'unidad_venta_nombre', displayName: 'UNIDAD', maxWidth: 120},
          { field: 'precio_venta_inicial', displayName: 'PRECIO', cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth:140},
          { field: 'total_existencia', displayName: 'EXISTENCIA', sort: { direction: uiGridConstants.ASC }, maxWidth: 120,cellFilter: 'currency:""',cellClass: 'cell-align-right',maxWidth:'140'},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/inventario/productos/productos.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },       
        rowStyle: function(row){
                if(Number(row.entity.total_existencia) < Number(row.entity.stock_minimo) ){
                  return 'row-null';
                }
              },
            rowTemplate : `<div ng-class="grid.options.rowStyle(row)"><div ng-mouseover="rowStyle={'background-color': (rowRenderIndex%2 == 1) ? '#F5F5F5' : '#F5F5F5','cursor':'pointer' };" 
               ng-mouseleave="rowStyle={}" ng-style="selectedStyle={'background-color': (row.isSelected) ? '#2196F3' : '','color': (row.isSelected) ? '#E6E6E6' : ''}">
                <div  ng-style="(!row.isSelected) ? rowStyle : selectedStyle" 
                    ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.uid" 
                    ui-grid-one-bind-id-grid="rowRenderIndex + '-' + col.uid + '-cell'"
                    class="ui-grid-cell"   
                    ng-class="{ 'ui-grid-row-header-cell': col.isRowHeader }" role="{{col.isRowHeader ? 'rowheader' : 'gridcell'}}" 
                    ui-grid-cell>
                </div>
            </div></div>`   

    };

    $scope.$watch('$root.factor', function() {
        calcularMoneda();
    });

    function calcularMoneda(){
      console.log('calcularMoneda',$rootScope.factor)
      var factor=$rootScope.factor;
      angular.forEach(lista, function(item, index) {
        item.precio_venta_inicial=item.dolar_precio_inicial*factor;
      });
    }

    activate();

    function activate(){
      Pace.restart();
      //TIPOS
      sisListasService.getByCampo('tipo_producto').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaTipos=res.data.response.datos;
        }
      })
      //CATEGORIAS
      invCategoriasService.getList(logEmpresa.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaCategorias=res.data.response.datos;
          vm.listaCategorias=_listaCategorias;
        }
      })
      //SISTEMA UNIDADES
      sisListasService.getByCampo('sistema_unidades').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaSistemas=res.data.response.datos;
        }
      })
      //UNIDADES
      sisUnidadesService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaUnidades=res.data.response.datos;
        }
      })
      //IMPUESTOS
      sisImpuestosService.getListByTipo("11E7A7016E303D9D9B0700270E383B06").then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaImpuestos=res.data.response.datos;
          //console.log('_listaImpuestos',_listaImpuestos)
        }
      })

    }

    vm.getList=function (){        
        invProductosService.getList(logEmpresa.id,vm.id_categoria).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            calcularMoneda();
            vm.filtrar();
            Pace.stop();
          }
        })
    }

    vm.filtrar = function() {
      /*var filtro={};
      filtro.id_categoria=vm.id_categoria;
      var miLista = $filter('filter')(lista, filtro, true);*/
      //vm.gridOptions.data = $filter('multiFiltro')(miLista, vm.filtro);
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return invProductosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invProductosService.one={};
          angular.copy(res.data.response.datos,invProductosService.one)
          invProductosService.one.id_empresa=logEmpresa.id;

          sessionStorage.setItem("one",JSON.stringify(invProductosService.one));
          $state.go('config.inventario-productos-detail', 
                    { 
                      sectionName: invProductosService.one.nombre,
                      listaTipos: _listaTipos,
                      listaCategorias: _listaCategorias,
                      listaSistemas: _listaSistemas,
                      listaUnidades: _listaUnidades,
                      listaImpuestos: _listaImpuestos,
                      //listaProductos: lista
                    });            
        }
      })
    }

    vm.selOne = function(item,ev){
      return invProductosService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invProductosService.one={};
          angular.copy(res.data.response.datos,invProductosService.one)
          sessionStorage.setItem("one",JSON.stringify(invProductosService.one));
          $state.go('config.inventario-productos-detail', 
                    { 
                      sectionName: invProductosService.one.nombre,
                      listaTipos: _listaTipos,
                      listaCategorias: _listaCategorias,
                      listaSistemas: _listaSistemas,
                      listaUnidades: _listaUnidades,
                      listaImpuestos: _listaImpuestos,
                      listaProductos: lista
                    });
        }
      })
    }

  }

})();