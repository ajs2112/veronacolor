(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("productosModalAtributosController", productosModalAtributosController);
  productosModalAtributosController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'invProductosService', 'invAtributosService', 'CONFIG'];

  function productosModalAtributosController($rootScope, $filter, $mdDialog, uiGridConstants, invProductosService, invAtributosService, CONFIG){
    var vm=this;
    vm.filtro="";
    vm.full=true;
    vm.list=[];

    vm.nombre="";
    vm.valor="";
    vm.cntSel=0;
    vm.rowSel=null;

    var lista=[];

    vm.gridModal = {
        columnDefs: [
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'valor', displayName: 'VALOR', sort: { direction: uiGridConstants.ASC }, },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.edit.on.beginCellEdit(null,function(rowEntity, colDef){
            vm.cntSel=1;
            vm.rowSel=rowEntity;
          });
        },
        enableCellEditOnFocus: true,
        selectionRowHeaderWidth: 40,
        rowHeight: 40
    };

    activate();

    function activate(){
      Pace.restart();
      return getList().then(function(){
          vm.list=lista;
          vm.gridModal.data = vm.list;          
          Pace.stop();
      })
    }

    function getList (){        
        return invAtributosService.getList(invProductosService.one.id)
        .then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.agregar = function(ev){
      var one={
        id:"",
        order_id:"",
        id_producto: invProductosService.one.id,
        nombre: vm.nombre,
        valor: vm.valor
      }
      if (!lista){
        lista=[];
      }
      lista.push(one);
      vm.nombre="";
      vm.valor="";
      vm.gridModal.data = lista;
    }

    vm.eliminar = function(ev){
      var sel=vm.rowSel;
      var indice = lista.indexOf(sel);
      lista.splice(indice,1);        
      /*
      */
      vm.cntSel=0;
      vm.gridModal.data = lista;
      console.log(vm.rowSel)
      console.log(indice)
    }    

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function(ev){
      Pace.restart();
      $mdDialog.hide(lista);
      Pace.stop();
    }

  }

})();