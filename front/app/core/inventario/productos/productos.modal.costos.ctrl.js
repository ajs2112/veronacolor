(function(){
  'use strict';
  
  angular.module("inventarioModule")
  .controller("productosModalCostosController", productosModalCostosController);
  productosModalCostosController.$inject=['$mdDialog', '$scope', 'invProductosService', 'CONFIG'];

  function productosModalCostosController($mdDialog, $scope, invProductosService, CONFIG){
    var vm=this;
    vm.one = {};
    vm.full=true;
    vm.error="";

    function dbl(numero){
      return parseFloat(numero).toFixed(2);
    }

    vm.calcular= function (){
      //INICIALIZAR
      var dolar= parseFloat(CONFIG.cotizacion_dolar).toFixed(2);
      var pctDcto=(parseFloat(vm.one.costo_pct_dcto)/100)+1;
      var pctAdic=(parseFloat(vm.one.costo_pct_adic)/100)+1;
      var utilDolar=(parseFloat(vm.one.dolar_pct_utilidad)/100)+1;
      var utilFactura=(parseFloat(vm.one.factura_pct_utilidad)/100)+1;
      var utilLista=(parseFloat(vm.one.lista_pct_utilidad)/100)+1;

      //DOLAR
      vm.one.dolar_costo_bruto=vm.one.dolar_costo_dolar * dolar;
      vm.one.dolar_costo_neto= dbl(vm.one.dolar_costo_bruto/pctDcto);
      vm.one.dolar_costo_neto= dbl(vm.one.dolar_costo_neto*pctAdic);
      vm.one.dolar_precio_inicial= dbl(vm.one.dolar_costo_neto*utilDolar);

      //NETOS
      vm.one.factura_costo_neto= dbl(vm.one.factura_costo_bruto/pctDcto);
      vm.one.factura_costo_neto= dbl(vm.one.factura_costo_neto*pctAdic);
      vm.one.factura_precio_final= dbl(vm.one.factura_costo_neto*utilFactura);
      vm.one.factura_costo_dolar= vm.one.factura_costo_neto / dolar;

      //CONVERTIR
      vm.one.lista_costo_neto= dbl(vm.one.lista_costo_bruto/pctDcto);
      vm.one.lista_costo_neto= dbl(vm.one.lista_costo_neto*pctAdic);
      vm.one.lista_precio_final= dbl(vm.one.lista_costo_neto*utilLista);
      vm.one.lista_costo_dolar= vm.one.lista_costo_neto / dolar;

    }

    activate();

    function activate(){
      console.log('invProductosService.one')
      console.log(invProductosService.one)
      Pace.restart();
      vm.one=invProductosService.one;
      vm.calcular();
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      Pace.restart();
      invProductosService.one=vm.one;
      $mdDialog.hide();
      Pace.stop();
    } 
  }

})();