(function(){
  'use strict';
  
  angular.module("inventarioModule")
  .controller("productosDetailController", productosDetailController);
  productosDetailController.$inject=['$state', '$scope', '$mdDialog', '$rootScope', '$filter', 'invProductosService', 'invExistenciasService', 'invAtributosService', 'invFormulasService', 'sisProductosService', 'CONFIG'];

  function productosDetailController($state, $scope, $mdDialog, $rootScope, $filter, invProductosService, invExistenciasService, invAtributosService, invFormulasService, sisProductosService, CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var one=JSON.parse(sessionStorage.getItem("one"));
    var vm=this;
    vm.one = {};
    vm.full=true;
    vm.error="";
    vm.id_sistema="";
    vm.btnFormulas=false;

    vm.listaTipos= [];
    vm.listaCategorias= [];
    vm.listaSistemas= [];
    vm.listaUnidades= [];
    vm.listaImpuestos= [];
    vm.listaExistencia=[];
    
    vm.searchText=null;
    vm.selectedProducto=null;


    var listaUnidades=[];
    var listaProductos=[];
    var listaAtributos=[];
    var listaFormulas=[];
    var _listaSisProductos=[];


    activate();


    function selectByTrigger (lista, trigger) {
      if (trigger){
        var esta=false;

        for (var i = 0; i < lista.length; i++) {
          if (lista[i].trigger==trigger){
            esta= true;
          }
        }

      } else {
        esta=false;
      }
      return esta;
    }

    function activate(){
      Pace.restart();

      vm.btnFormulas= selectByTrigger(CONFIG.menuItems,'inventario-productos-btnFormulas')
      //console.log(vm.btnFormulas)
      console.log('one', one)

      //vm.one=invProductosService.one;
      vm.one=one;

      //SIS-PRODUCTOS
      sisProductosService.getList(logEmpresa.id).then(function(res){
          _listaSisProductos=res.data.response.datos;
          vm.selectedProducto=selectById(_listaSisProductos, vm.one.id_producto);
      })

      if ($state.params.listaTipos){
        vm.listaTipos=$state.params.listaTipos;
      }

      if ($state.params.listaCategorias){
        vm.listaCategorias=$state.params.listaCategorias;
      }

      if ($state.params.listaSistemas){
        vm.listaSistemas=$state.params.listaSistemas;
      }

      if ($state.params.listaUnidades){
        listaUnidades=$state.params.listaUnidades;
      }

      if ($state.params.listaImpuestos){
        vm.listaImpuestos=$state.params.listaImpuestos;
      }
      /*
      if ($state.params.listaProductos){
        listaProductos=$state.params.listaProductos;
      }
      */                
      

      //OBTENER SISTEMA DE UNIDADES
      if (vm.one.id_unidad_venta){
        var filtro={};
        filtro.id=vm.one.id_unidad_venta;   
        var result = $filter('filter')(listaUnidades, filtro, true);
        if (result.length){
          vm.id_sistema=result[0].id_tipo;
          vm.listaUnidades = $filter('filter')(listaUnidades, vm.id_sistema);
        }
      }

      vm.one.es_inactivo=!!+vm.one.es_inactivo;
      vm.one.es_editable=!!+vm.one.es_editable;

      invExistenciasService.getOne(vm.one.id).then(function(res){
        console.log(vm.one.id)
        console.log(res)
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaExistencia=res.data.response.datos;
        }
      })
      Pace.stop();
    }

    /************************************************************************************************/
    vm.calcular= function (){
      console.log('calcula')
      //INICIALIZAR
      //var dolar= parseFloat(CONFIG.cotizacion_dolar).toFixed(2);
      var pctDcto=(parseFloat(vm.one.costo_pct_dcto)/100)+1;
      var pctAdic=(parseFloat(vm.one.costo_pct_adic)/100)+1;
      var utilDolar=(parseFloat(vm.one.dolar_pct_utilidad)/100)+1;

      //DOLAR
      vm.one.dolar_costo_neto= (vm.one.dolar_costo_dolar/pctDcto);
      vm.one.dolar_costo_neto= (vm.one.dolar_costo_dolar*pctAdic);
      vm.one.dolar_precio_inicial= (vm.one.dolar_costo_neto*utilDolar);

    }

    vm.selProducto=function(item){
      if (item){
        console.log(item)
        vm.one.id_producto=item.id;
        vm.one.nombre=item.nombre;
        vm.one.codigo=item.codigo;
        vm.one.unidad_compra_nombre=item.unidad_compra_nombre;
        vm.one.unidad_venta_nombre=item.unidad_venta_nombre;
        //OBTENER SISTEMA DE UNIDADES
        /*if (item.id_unidad_venta){
          var filtro={};
          filtro.id=item.id_unidad_venta;   
          var result = $filter('filter')(listaUnidades, filtro, true);
          if (result.length){
            var filtroUnd={};
            filtroUnd.id_tipo=result[0].id_tipo;
            vm.listaUnidades = $filter('filter')(listaUnidades, filtroUnd, true);
            vm.id_unidad=item.id_unidad_venta;
            vm.unidad=selUnidad(vm.id_unidad);
          }
        }*/
      }
    }


    vm.querySearch = function( item ) {
      var result= $filter('filter')(_listaSisProductos, item);
      return result;
    }

    function selectById (lista, id) {
      var filtro={};
      filtro.id=id;   
      var result = $filter('filter')(lista, filtro, true);

      if (result.length){
        return result[0];
      } else {
        return null;
      }
    }

    /************************************************************************************************/
    vm.filtrarUnidades = function() {
      var filtro={};
      filtro.id_tipo=vm.id_sistema;
      vm.listaUnidades = $filter('filter')(listaUnidades, filtro, true);
    }

    vm.guardarx = function() {
      console.log(vm.one)
    }
    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      vm.one.es_inactivo=+vm.one.es_inactivo;
      vm.one.es_editable=+vm.one.es_editable;
      invProductosService.setOne(vm.one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);

          vm.one.id=res.data.response.datos;
          //ATRIBUTOS *************************************************************/
          if (listaAtributos.length){
            var one={
              id_producto: vm.one.id,
              lista: listaAtributos
            }
            invAtributosService.setOne(one).then(function(res){
              console.log('res atributos')
              console.log(res)
              if (res.data && res.data.code==0){
                localStorage.setItem("token",res.data.response.token);
              } else if (res.data && res.data.code!==0){
                localStorage.setItem("token",res.data.response.token);
                vm.error=res.data.response.token;
              } 
            })
          }

          //FORMULAS *************************************************************/
          if (listaFormulas.length){
            var one={
              id_producto: vm.one.id,
              lista: listaFormulas
            }
            //console.log('envio formula')
            //console.log(one)
            invFormulasService.setOne(one).then(function(res){
              //console.log('res guardar formula')
              //console.log(res)
              if (res.data && res.data.code==0){
                localStorage.setItem("token",res.data.response.token);
              } else if (res.data && res.data.code!==0){
                localStorage.setItem("token",res.data.response.token);
                vm.error=res.data.response.token;
              } 
            })
          }

          Pace.stop();
          vm.full=true;
          $rootScope.showToast('Producto guardado con exito');
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 


    /************************* DIALOGS *******************************/
    vm.openDialogAtributos=function(ev){
        listaAtributos=[];
        $mdDialog.show({
          controller: 'productosModalAtributosController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/productos/productos.modal.atributos.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(atributos){
          listaAtributos=atributos;
          $rootScope.showToast('Atributos seleccionados con exito');
        });
    }

    vm.openDialogCostos=function(ev){
        $mdDialog.show({
          controller: 'productosModalCostosController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/productos/productos.modal.costos.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Costos seleccionados con exito');
        });
    }

    vm.openDialogFormulas=function(ev){
        $mdDialog.show({
          controller: 'productosModalFormulasController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/productos/productos.modal.formulas.tpl.html',
          parent: angular.element(document.body),
          locals:{
            //listaProductos:listaProductos,
            listaUnidades:listaUnidades,
            listaFormulas:listaFormulas  
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(formula){
          listaFormulas=formula;
          //console.log('asigno')
          //console.log(listaFormulas)
          $rootScope.showToast('Formula generada con exito');
        });
    }    

    vm.openDialogSisProductos=function(ev){
      sisProductosService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          sisProductosService.one={};
          angular.copy(res.data.response.datos,sisProductosService.one)

          $mdDialog.show({
            controller: 'sisProductosDetailController',
            controllerAs: 'ctrl',
            templateUrl: 'front/app/core/listas/productos/productos.detail.tpl.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true
          })
          .then(function(response){
            var idSisProducto=response.data.response.datos;
            console.log(idSisProducto)
            $rootScope.showToast('Producto guardado con exito');
            sisProductosService.getList(logEmpresa.id).then(function(res){
                _listaSisProductos=res.data.response.datos;
                vm.selectedProducto=selectById(_listaSisProductos, idSisProducto.toUpperCase());
                console.log(_listaSisProductos)
            })
          });

        }
      })

    }



  }

})();