(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("almacenesListaController", almacenesListaController);
  almacenesListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'invAlmacenesService', 'CONFIG'];

  function almacenesListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, invAlmacenesService,  CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];
    var lista=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'descrip', displayName: 'DESCRIPCION', },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/inventario/almacenes/almacenes.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      return getList()
      .then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){
        return invAlmacenesService.getList(logEmpresa.id).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return invAlmacenesService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invAlmacenesService.one={};
          angular.copy(res.data.response.datos,invAlmacenesService.one)
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return invAlmacenesService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invAlmacenesService.one={};
          angular.copy(res.data.response.datos,invAlmacenesService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'almacenesDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/almacenes/almacenes.detail.tpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Almacen guardado con exito');
          return getList().then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();