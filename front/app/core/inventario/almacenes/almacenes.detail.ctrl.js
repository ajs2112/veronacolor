(function(){
  'use strict';
  
  angular.module("inventarioModule")
  .controller("almacenesDetailController", almacenesDetailController);
  almacenesDetailController.$inject=['$mdDialog', '$scope', 'invAlmacenesService'];

  function almacenesDetailController($mdDialog, $scope, invAlmacenesService){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=invAlmacenesService.one;
      vm.one.es_default=!!+vm.one.es_default;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      vm.one.es_default=+vm.one.es_default;
      vm.one.id_empresa=logEmpresa.id;
      invAlmacenesService.setOne(vm.one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          Pace.stop();
          vm.full=true;
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
        } 
      })
    } 
  }

})();