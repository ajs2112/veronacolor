(function(){
  'use strict';
  
  angular.module("invproduccionModule")
  .controller("invproduccionDetailController", invproduccionDetailController);
  invproduccionDetailController.$inject=['$state', '$scope', '$mdDialog', '$rootScope', '$filter', 'listasService', 'almacenesService', 'productosService', 'unidadesService', 'movimientosService', 'invproduccionService', 'CONFIG'];

  function invproduccionDetailController($state, $scope, $mdDialog, $rootScope, $filter,  listasService, almacenesService, productosService, unidadesService, movimientosService, invproduccionService, CONFIG){
    var vm=this;
    vm.one = {};
    vm.full=true;
    vm.error="";
    vm.id_sistema="";

    vm.listaOperaciones=[];
    vm.listaAlmacenes=[];

    vm.cntProductos=0;
    
    var lista=[];
    var _listaProductos=[];
    var _listaUnidades=[];
    vm.tipoOperacion={};

    vm.gridDetail = {
        columnDefs: [
          { field: 'producto_codigo', displayName: 'CÓDIGO', maxWidth: 140, enableCellEdit: false},
          { field: 'producto_nombre', displayName: 'DESCRIPCION', enableCellEdit: false},
          { field: 'producto_unidad', displayName: 'UNIDAD', maxWidth: 140, enableCellEdit: false},
          { field: 'cantidad', displayName: 'CANTIDAD', maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.edit.on.beginCellEdit(null,function(rowEntity, colDef){
            vm.cntSel=1;
            vm.rowSel=rowEntity;
          });
        },
        enableCellEditOnFocus: true,
    };

    activate();

    function activate(){
      Pace.restart();
      if ($state.params.listaOperaciones){
        vm.listaOperaciones=$state.params.listaOperaciones;
      }

      //PRODUCTOS
      productosService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaProductos=res.data.response.datos;
        }
      })

      //UNIDADES
      unidadesService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          _listaUnidades=res.data.response.datos;
        }
      })

      //ALMACENES
      almacenesService.getList().then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          vm.listaAlmacenes=res.data.response.datos;
        }
      })      

      vm.one=invproduccionService.one;
      vm.one.fecha=new Date(vm.one.fecha);

      selTipoOperacion();

      return getList().then(function(){
        if (lista){
          vm.gridDetail.data = lista;
          vm.cntProductos=lista.length;          
        } else {
          lista=[];
        }

          Pace.stop();
      })

    }

    function getList (){        
        var obj={
          id_tipo:vm.one.id_tipo,
          id_operacion:vm.one.id 
        }
        return movimientosService.getList(obj).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    function selTipoOperacion(){
      var filtro={};
      filtro.id=vm.one.id_tipo;   
      var result = $filter('filter')(vm.listaOperaciones, filtro, true);
      if (result.length){
        vm.tipoOperacion=result[0];
      }
    }

    vm.guardar = function() {
      vm.full=false;
      Pace.restart();
      //SELECCIONAR TIPO OPERACION
      vm.one.id_usuario=CONFIG.logUser.id;
      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        detail:lista
      }
      invoperacionesService.setOne(obj).then(function(res){ 
        $rootScope.showToast('Operación registrada con exito');
        vm.full=true;
      })
    }

    vm.anular = function() {
      vm.full=false;
      Pace.restart();
      //SELECCIONAR TIPO OPERACION
      vm.one.id_usuario=CONFIG.logUser.id;
      var obj={
        tipo:vm.tipoOperacion,
        head:vm.one,
        //detail:lista
      }
      invoperacionesService.anularOne(obj).then(function(res){ 
          $rootScope.showToast('Operación anulada con exito');
          vm.full=true;
      })
    }     

    function agregar(obj){
      if (obj.id_producto){
        var filtro={};
        filtro.id_producto=obj.id_producto;   
        var result = $filter('filter')(lista, filtro, true);
        if (result.length){
          $rootScope.showToast('Producto en lista');
        } else {
          lista.push(obj);
        }
      } else {
        //return null
      }
      vm.gridDetail.data = lista;
      vm.cntProductos=lista.length;
    }

    vm.delProducto = function(ev){
      var sel=vm.rowSel;
      var indice = lista.indexOf(sel);
      lista.splice(indice,1);        
      vm.cntSel=0;
      if (!lista){
        lista=[];        
      } else {
        vm.gridDetail.data = lista;
        vm.cntProductos=lista.length;
      }
    }    


    /************************* DIALOGS *******************************/
    vm.openDialogProductos=function(ev){
        $mdDialog.show({
          controller: 'modalInventarioController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/movimientos/modals/modal.inventario.tpl.html',
          parent: angular.element(document.body),
          locals:{
            listaProductos:_listaProductos,
            listaUnidades:_listaUnidades,
          },
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(producto){
          agregar(producto);
          //$rootScope.showToast('Formula generada con exito');
        });
    }    

  
  }

})();