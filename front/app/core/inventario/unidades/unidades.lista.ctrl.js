(function(){
  'use strict';

  angular.module("sistemaModule")
  .controller("unidadesListaController", unidadesListaController);
  unidadesListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'sisUnidadesService', 'sisListasService', 'CONFIG'];

  function unidadesListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, sisUnidadesService, sisListasService, CONFIG){
    var vm=this;
    vm.filtro="";
    vm.list=[];

    var lista=[];
    var listaTipos=[];
    var listaStatus=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'tipo_nombre', displayName: 'TIPO', sort: { direction: uiGridConstants.ASC }, },
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'factor_conversion', displayName: 'FACTOR', maxWidth: 200},
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/inventario/unidades/unidades.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      //STATUS
      sisListasService.getByCampo('sistema_unidades').then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          listaTipos=res.data.response.datos;
        }
      })

      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return sisUnidadesService.getList().then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return sisUnidadesService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          sisUnidadesService.one={};
          angular.copy(res.data.response.datos,sisUnidadesService.one);
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return sisUnidadesService.getOne(item.id).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          sisUnidadesService.one={};
          angular.copy(res.data.response.datos,sisUnidadesService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        $mdDialog.show({
          controller: 'unidadesDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/unidades/unidades.detail.tpl.html',
          locals: {
            tipos: listaTipos
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Unidad guardado con exito');
          return getList()
          .then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();