(function(){
  'use strict';
  
  angular.module("sistemaModule")
  .controller("unidadesDetailController", unidadesDetailController);
  unidadesDetailController.$inject=['$mdDialog', '$scope', 'sisUnidadesService', 'tipos'];

  function unidadesDetailController($mdDialog, $scope, sisUnidadesService, tipos){
    var vm=this;
    vm.listaTipos=tipos;
    vm.one = {};
    vm.error="";
    vm.full=true;

    activate();

    function activate(){
      Pace.restart();
      vm.one=sisUnidadesService.one;
      vm.one.es_patron=!!+vm.one.es_patron;
      Pace.stop();
    }

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.guardar = function() {
      Pace.restart();
      vm.full=false;
      vm.one.es_patron=+vm.one.es_patron;
      sisUnidadesService.setOne(vm.one).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          $mdDialog.hide();
          Pace.stop();
        } else if (res.data && res.data.code!==0){
          localStorage.setItem("token",res.data.response.token);
          vm.error=res.data.response.token;
          Pace.stop();
          vm.full=true;
        } 
      })
    } 
  }

})();