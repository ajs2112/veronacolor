(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("categoriasListaController", categoriasListaController);
  categoriasListaController.$inject=['$rootScope','$filter', '$state', '$mdDialog', 'uiGridConstants', 'invCategoriasService', 'CONFIG'];

  function categoriasListaController($rootScope, $filter, $state, $mdDialog, uiGridConstants, invCategoriasService,  CONFIG){
    var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
    var vm=this;
    vm.filtro="";
    vm.list=[];
    var lista=[];

    var nuevo={};
    vm.gridOptions = {
        columnDefs: [
          { field: 'padre_nombre', displayName: 'PADRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'nombre', displayName: 'NOMBRE', sort: { direction: uiGridConstants.ASC }, },
          { field: 'descrip', displayName: 'DESCRIPCION', },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null, function (rows) {
            vm.gridApi.expandable.collapseAllRows();
            vm.gridApi.expandable.toggleRowExpansion(rows.entity)
          });        
        },
        expandableRowTemplate: 'front/app/core/inventario/categorias/categorias.lista.row.tpl.html',
        enableExpandableRowHeader: false,
        expandableRowScope: { 
          editar: function(ev){
              var sel=vm.gridApi.selection.getSelectedRows();
              vm.selOne(sel[0])
          } 
        },          

    };

    activate();

    function activate(){
      Pace.restart();
      return getList().then(function(){
          vm.list=lista;
          vm.gridOptions.data = vm.list;          
          vm.filtrar();
          Pace.stop();
      })
    }

    function getList (){        
        return invCategoriasService.getList(logEmpresa.id)
        .then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            lista=res.data.response.datos;
            return lista;
          }
        })
    }

    vm.filtrar = function() {
      vm.gridOptions.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.agregar = function(ev){
      return invCategoriasService.getOne(0).then(function(res){
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invCategoriasService.one={};
          angular.copy(res.data.response.datos,invCategoriasService.one)
          invCategoriasService.one.id_empresa=logEmpresa.id;
          openDialog(ev);            
        }
      })
    }

    vm.selOne = function(item,ev){
      return invCategoriasService.getOne(item.id).then(function(res){
        console.log('res getOne',res)
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          invCategoriasService.one={};
          angular.copy(res.data.response.datos,invCategoriasService.one)
          openDialog(ev);            
        }
      })
    }

    function openDialog(ev){
        var obj={id_padre:'00000000000000000000000000000000'};
        var miLista=$filter('filter')(lista, obj);
        $mdDialog.show({
          controller: 'categoriasDetailController',
          controllerAs: 'ctrl',
          templateUrl: 'front/app/core/inventario/categorias/categorias.detail.tpl.html',
          locals: {
            grupos: miLista
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
        })
        .then(function(){
          $rootScope.showToast('Categoria guardada con exito');
          return getList().then(function(){
              vm.list=lista;
              vm.gridOptions.data = vm.list;          
              vm.filtrar();
          })
        });

    }

  }

})();