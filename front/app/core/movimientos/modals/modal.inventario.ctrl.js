(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("modalInventarioController", modalInventarioController);
  modalInventarioController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'listaProductos', 'listaUnidades', 'invFormulasService', 'tipoOperacion', 'CONFIG'];

  function modalInventarioController($rootScope, $filter, $mdDialog, uiGridConstants, listaProductos, listaUnidades, invFormulasService, tipoOperacion, CONFIG){
    var vm=this;
    vm.filtro="";
    vm.full=true;
    vm.list=[];
    vm.listaUnidades=[];

    vm.nombre="";
    vm.valor="";
    vm.cantidad=1;
    vm.cntSel=0;
    vm.cntComponentes=0;
    vm.rowSel=null;
    vm.id_unidad=0;
    vm.unidad={};
    vm.totalCosto=0;

    vm.searchText=null;
    vm.selectedProducto=null;

    var lista=[];
    var listaProductos=listaProductos;
    var listaUnidades=listaUnidades;


    activate();

    function activate(){
      Pace.restart();
    }

    /****************************************************************************/
    vm.selProducto=function(item){
      if (item){
        //OBTENER SISTEMA DE UNIDADES
        if (item.id_unidad_venta){
          var filtro={};
          filtro.id=item.id_unidad_venta;   
          var result = $filter('filter')(listaUnidades, filtro, true);
          if (result.length){
            var filtroUnd={};
            filtroUnd.id_tipo=result[0].id_tipo;
            vm.listaUnidades = $filter('filter')(listaUnidades, filtroUnd, true);
            vm.id_unidad=item.id_unidad_venta;
            vm.unidad=selUnidad(vm.id_unidad);
          }
        }
      }
    }

    function selUnidad(idUnd){
      //OBTENER SISTEMA DE UNIDADES
      if (idUnd){
        var filtro={};
        filtro.id=idUnd;   
        var result = $filter('filter')(listaUnidades, filtro, true);
        if (result.length){
          return result[0];
        }
      }
    }    

    vm.selUnidad=function(){
      vm.unidad=selUnidad(vm.id_unidad);
    }    

    vm.querySearch = function( item ) {
      var result= $filter('filter')(listaProductos, item);
      return result;
    }

    /**************************************************************************/

    function dbl(numero){
      return parseFloat(numero).toFixed(2);
    }


    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };


    vm.guardar = function(ev){
      var listaSeleccionada=[];

      var one={
        id_producto:vm.selectedProducto.id,
        id_producto_padre:'00000000000000000000000000000000',
        producto_codigo:vm.selectedProducto.codigo,
        producto_nombre:vm.selectedProducto.nombre,
        id_unidad:vm.id_unidad,
        producto_unidad:vm.unidad.nombre_display,
        id_impuesto:vm.selectedProducto.id_impuesto,
        valor_impuesto:vm.selectedProducto.impuesto_valor,                   
        precio:vm.selectedProducto.dolar_precio_inicial,
        cantidad:vm.cantidad,     
      }

      listaSeleccionada.push(one);

      //TIPO OPERACION ORDEN DE PRODUCCION
      if (tipoOperacion=='11E7C25EDE4B02BB877D00E04C6F7E24'){
        //MANOFACTURADO
        if (vm.selectedProducto.id_tipo=='11E7AC1AEC9B3C87A15E00270E383B06'){
          invFormulasService.getList(vm.selectedProducto.id).then(function(res){
            var listaFormula=res.data.response.datos;
            //console.log(listaFormula)
  
            for (var i = 0; i < listaFormula.length; i++) {
              //console.log(listaFormula[i])
              var oneComponente={
                id_producto:listaFormula[i].id_componente,
                id_producto_padre:vm.selectedProducto.id,
                producto_codigo:listaFormula[i].componente_codigo,
                producto_nombre:listaFormula[i].componente_nombre,
                id_unidad:listaFormula[i].id_unidad,
                producto_unidad:listaFormula[i].componente_unidad,
                id_impuesto:vm.selectedProducto.id_impuesto,
                valor_impuesto:vm.selectedProducto.impuesto_valor,                   
                precio:listaFormula[i].componente_dolar_costo_neto,
                cantidad:listaFormula[i].cantidad * vm.cantidad,     
              }
              listaSeleccionada.push(oneComponente);
            }

          })
        }
      }

      console.log('listaSeleccionada')
      console.log(listaSeleccionada)
      Pace.restart();
      $mdDialog.hide(listaSeleccionada);
      Pace.stop();
      /*
      */
    }

  }

})();