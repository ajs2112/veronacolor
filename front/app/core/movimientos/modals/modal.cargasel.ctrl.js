(function(){
  'use strict';

  angular.module("inventarioModule")
  .controller("modalCargaSelController", modalCargaSelController);
  modalCargaSelController.$inject=['$rootScope','$filter', '$mdDialog', 'uiGridConstants', 'headOperacion', 'invMovimientosService', 'CONFIG'];

  function modalCargaSelController($rootScope, $filter, $mdDialog, uiGridConstants, headOperacion, invMovimientosService, CONFIG){
    var vm=this;
    vm.filtro="";
    vm.full=true;
    vm.list=[];
    vm.headOperacion=headOperacion;

    vm.cntSel=0;
    vm.id_tipo=0;
    vm.rowSel=null;


    var lista=[];

    vm.gridModal = {
        columnDefs: [
          { field: 'es_sel', displayName: '', maxWidth: 120, type: 'boolean',cellTemplate: '<input type="checkbox" ng-model="row.entity.es_sel">'},
          { field: 'producto_codigo', displayName: 'CÓDIGO', maxWidth: 140, enableCellEdit: false},
          { field: 'producto_nombre', displayName: 'DESCRIPCION', enableCellEdit: false},
          { field: 'producto_unidad', displayName: 'UNIDAD', maxWidth: 140, enableCellEdit: false},
          { field: 'cantidad', displayName: 'CANTIDAD', maxWidth: 140 },
        ],
        onRegisterApi: function( gridApi ) {
          vm.gridApi=gridApi;
          gridApi.selection.on.rowSelectionChanged(null,function(row){
            vm.rowSel = row.entity;
          });
        },
        enableCellEditOnFocus: true,
        selectionRowHeaderWidth: 40,
        rowHeight: 40,
    };

    activate();

    function activate(){
      //Pace.restart();
      console.log(headOperacion)
      var obj={
        id_tipo:headOperacion.id_tipo,
        id_operacion:headOperacion.id
      }
      return invMovimientosService.getList(obj).then(function(res){        
        if (res.data && res.data.code==0){
          localStorage.setItem("token",res.data.response.token);
          lista=res.data.response.datos;
          vm.filtrar();
          return lista;
        }
      })
    }
    

    vm.filtrar = function() {
      vm.gridModal.data = $filter('multiFiltro')(lista, vm.filtro);
    };

    vm.hide = function() {
      $mdDialog.hide();
    };

    vm.cancel = function() {
      $mdDialog.cancel();
    };

    vm.seleccionar = function(ev){
      console.log(vm.gridModal.data)
      /*
      */
        var filtro={};
        filtro.es_sel=true;   
        var seleccion = $filter('filter')(vm.gridModal.data, filtro, true);
        if (seleccion.length){
          $mdDialog.hide(seleccion);

        } else {
          $rootScope.showToast('Debe seleccionar al menos un producto');
        }
    }

  }

})();