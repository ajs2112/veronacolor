(function(){
	'use strict';

  	angular.module("sistemaModule")
  	.controller("loginController", loginController);

  	loginController.$inject=['$rootScope','jwtHelper','$state', '$mdDialog', '$filter', 'Idle','CONFIG','sisLoginService','sisUsuariosService','lstCotizacionesService'];

  	function loginController($rootScope, jwtHelper,$state,$mdDialog,$filter,Idle,CONFIG,sisLoginService,sisUsuariosService,lstCotizacionesService){
      var vm=this;
      vm.user={};
      

      vm.crear=function(){
          sisUsuariosService.one={};
          $state.go("signup");
          $mdDialog.hide();        
      }

      vm.logIn = function(user){
        sisLoginService.logIn(user).then(function(res){
          if (res.data && res.data.code == 0){
            localStorage.setItem("token",res.data.response.token);
            CONFIG.logUser=res.data.response.datos;

            sessionStorage.setItem("logUser",JSON.stringify(CONFIG.logUser));
            var myLogUser=JSON.parse(sessionStorage.getItem("logUser"));


            lstCotizacionesService.getLast().then(function(res){
              if (res.data && res.data.code==0){
                vm.listaCotizacion=res.data.response.datos;
                //$rootScope.id_moneda="11E8F819279E29CC9E9100270E383B06";
                sessionStorage.setItem("listaCotizacion",JSON.stringify(res.data.response.datos));

                var _lista=res.data.response.datos;
                var filtro={};
                filtro.id_moneda="11E8F819279E29CC9E9100270E383B06";  

                var result = $filter('filter')(_lista, filtro, true);
                if (result.length){
                  $rootScope.factor=result[0].valor;
                  sessionStorage.setItem("oneCotizacion",JSON.stringify(result[0]));
                }

                Idle.watch();
                $state.go("menu.dashboard");
                $mdDialog.hide();
              }
            })



          } else {
            vm.error="Nombre de usuario y/o contraseña inválidos";
          }
        },function(){
        });
      }   

  	}

})();