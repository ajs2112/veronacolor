(function(){
	'use strict';

  	angular.module("sistemaModule")
  	.controller("dashboardController", dashboardController);

  	dashboardController.$inject=['$filter', 'sisDashboardService', 'sisCotizacionesService', 'sisEmpresasService', 'jwtHelper','CONFIG'];

  	function dashboardController($filter, sisDashboardService,sisCotizacionesService,sisEmpresasService,jwtHelper,CONFIG){
      var logEmpresa=JSON.parse(sessionStorage.getItem("logEmpresa"));
      var originatorEv;
      var listaEmpresas=[];
      var lista=[];
      
      var vm=this;
      vm.lista=[];
      vm.listaEmpresas=[];
      vm.one={};
      vm.empresa={};
      activate();

      function activate(){
        sisCotizacionesService.getLast().then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            CONFIG.cotizacion_dolar=res.data.response.datos[0].valor;
            return lista;
          }
        })

        //EMPRESAS
        var permisos={permisos:CONFIG.logUser.empresas};
        sisEmpresasService.getListByUser(permisos).then(function(res){
          if (res.data && res.data.code==0){
            localStorage.setItem("token",res.data.response.token);
            listaEmpresas=res.data.response.datos;
            vm.listaEmpresas=listaEmpresas;
            if (!logEmpresa){
              vm.cambiarEmpresa(res.data.response.datos[0].id);
            } else {
              vm.cambiarEmpresa(logEmpresa.id)
            }
          }
        })

      }


      vm.openEmpresas=function($mdMenu, ev){
        originatorEv=ev;
        $mdMenu.open(ev);
      }

      vm.cambiarEmpresa=function(id){
          var filtro={};
          filtro.id=id;   
          var result = $filter('filter')(listaEmpresas, filtro, true);
          if (result.length){
            vm.empresa=result[0];
            sessionStorage.setItem("logEmpresa", JSON.stringify(vm.empresa));
            CONFIG.logEmpresa=vm.empresa;
          }
      }      

    }

})();