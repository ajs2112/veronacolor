(function(){
	'use strict';

  	angular.module("sistemaModule")
  	.controller("menuController", menuController);

  	menuController.$inject=['$rootScope','$scope', '$filter', '$state', '$mdMedia', '$mdDialog', 'jwtHelper','CONFIG','sisMenuService', 'sisUsuariosService', 'lstCotizacionesService'];

  	function menuController($rootScope,$scope, $filter, $state, $mdMedia, $mdDialog,jwtHelper,CONFIG,sisMenuService,sisUsuariosService,lstCotizacionesService){
      var originatorEv;
    	var vm=this;
      vm.currentNavItem = "";
      vm.lockLeft = true;
      vm.logUser={};
      vm.menuArbol=[];
      vm.menuItems=[];
      vm.titulo="";

      vm.listaCotizacion=JSON.parse(sessionStorage.getItem("listaCotizacion"));
      var oneCotizacion=JSON.parse(sessionStorage.getItem("oneCotizacion"));
      $rootScope.id_moneda=oneCotizacion.id_moneda;
      $rootScope.factor=oneCotizacion.valor;

      activate();

      function activate(){
        vm.titulo="Gids";
        vm.logUser=CONFIG.logUser;
        getMenuByUser();
        getMenuItems();
      }

      vm.seleccionaCotizacion=function(){
        var _lista=vm.listaCotizacion;
        var filtro={};
        filtro.id_moneda=$rootScope.id_moneda;   

        var result = $filter('filter')(_lista, filtro, true);
        if (result.length){
          $rootScope.factor=result[0].valor;
          sessionStorage.setItem("oneCotizacion",JSON.stringify(result[0]));
        }

      }

      

      vm.showLeftSection=function(){
        sisMenuService.toggleSideBar();
      }

      vm.collapseAll = function(data) {
       for(var i in vm.menuArbol) {
         if(vm.menuArbol[i] != data) {

           vm.menuArbol[i].expanded = false;   
         }
       }
       data.expanded = !data.expanded;
       };

      function getMenuByUser (){        
        var permisos={permisos:vm.logUser.permisos};
        return sisMenuService.getMenuByUser(permisos).then(function(res){
          //console.log(res)
          if (res.data && res.data.code==0){
            //console.log('vm.menuArbol',res.data.response.datos)
            vm.menuArbol=res.data.response.datos;
            localStorage.setItem("token",res.data.response.token);
            return sisMenuService.arbol;
          }
        })
      }

      function getMenuItems (){        
        var permisos={permisos:vm.logUser.permisos};   
        return sisMenuService.getListByUser(permisos).then(function(res){
          if (res.data && res.data.code==0){
            CONFIG.menuItems=res.data.response.datos;
            localStorage.setItem("token",res.data.response.token);
            return res.data.response.datos;
          }
        })
      }            

      vm.openConfig=function($mdMenu, ev){
        originatorEv=ev;
        $mdMenu.open(ev);
      }

      vm.openAccount=function(state){
        sisUsuariosService.one=vm.logUser;
        $state.go(state);
      }      

    	vm.goto=function(state){
        //console.log('goto',state)
        var tokenPayload=jwtHelper.decodeToken(localStorage.getItem("token"));
        //sessionStorage.idAgente=tokenPayload.id_agente;
    		$state.go(state);
        sisMenuService.toggleSideBar();        
    	}

  	}

})();