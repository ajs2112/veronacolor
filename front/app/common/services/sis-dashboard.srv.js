(function(){
	'use strict';
	
	angular.module('sistemaModule')
	.factory('sisDashboardService',sisDashboardService) 
	sisDashboardService.$inject=['$http','$q','CONFIG', '$httpParamSerializerJQLike'];

	function sisDashboardService($http,$q,CONFIG,$httpParamSerializerJQLike){
		var mySrv = {
			getList:getList,
			getOne:getOne,
			delOne:delOne,
			setOne:setOne
		}
		return mySrv;


		function getList(id){	
	        var deferred=$q.defer();
			$http({
	            method: 'GET',
				skipAutorization:false,
	            url: CONFIG.APIURL+'sis_dashboard/getList/'+id
	        })				
			.then (function(res){
				//console.log(res)
				deferred.resolve(res);
			})
			.then (function(error){
				deferred.reject(error);
			})
			return deferred.promise;
		}

		function getOne(id){	
	        var deferred=$q.defer();
			$http({
	            method: 'GET',
				skipAutorization:false,
	            url: CONFIG.APIURL+'sis_dashboard/getOne/'+id
	        })				
			.then (function(res){
				deferred.resolve(res);
			})
			.then (function(error){
				deferred.reject(error);
			})
			return deferred.promise;
		}

		function delOne(id){	
	        var deferred=$q.defer();
			$http({
	            method: 'GET',
				skipAutorization:false,
	            url: CONFIG.APIURL+'sis_dashboard/delOne/'+id
	        })				
			.then (function(res){
				deferred.resolve(res);
			})
			.then (function(error){
				deferred.reject(error);
			})
			return deferred.promise;
		}

		function setOne(obj){
	        var deferred=$q.defer();
			$http({
	            method: 'POST',
				skipAutorization:false,
	            url: CONFIG.APIURL+'sis_dashboard/setOne',
	            data: $httpParamSerializerJQLike(obj),
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	        })				
			.then (function(res){
				deferred.resolve(res);
			})
			.then (function(error){
				deferred.reject(error);
			})
			return deferred.promise;
		}		

	}
})();