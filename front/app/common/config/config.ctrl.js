(function(){
	'use strict';

  	angular.module("configModule")
  	.controller("configController", configController);

    configController.$inject=['$rootScope', '$scope', '$state', '$mdMedia', '$mdDialog','$filter'];

  	function configController($rootScope, $scope, $state, $mdMedia, $mdDialog, $filter){
      var originatorEv;
    	var vm=this;
      vm.currentNavItem = "";
      vm.logUser={};
      vm.menuArbol=[];
      vm.menuItems=[];
      vm.titulo="";
      vm.saludo="";

      vm.listaCotizacion=JSON.parse(sessionStorage.getItem("listaCotizacion"));
      var oneCotizacion=JSON.parse(sessionStorage.getItem("oneCotizacion"));
      $rootScope.id_moneda=oneCotizacion.id_moneda;
      $rootScope.factor=oneCotizacion.valor;

      

      activate();

      function activate(){

      }

      vm.seleccionaCotizacion=function(){
        var _lista=vm.listaCotizacion;
        var filtro={};
        filtro.id_moneda=$rootScope.id_moneda;   

        var result = $filter('filter')(_lista, filtro, true);
        if (result.length){
          $rootScope.factor=result[0].valor;
          sessionStorage.setItem("oneCotizacion",JSON.stringify(result[0]));
        }

      }

      vm.openConfig=function($mdOpenMenu, ev){
        originatorEv=ev;
        $mdOpenMenu(ev);
      }

    	vm.goBack=function(){
        var prevState=localStorage.getItem("prevState");
        $state.go(prevState)

    	}

  	}

})();