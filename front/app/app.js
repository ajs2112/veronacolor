(function (){
	angular.module('myApp',[
		'angular-jwt',
	    'ui.router',
	    'oc.lazyLoad',
	    'ct.ui.router.extras',
	    'ngAnimate',
	    'ngMessages',
	    'ngIdle',
	    'ngMaterial',
	    'ui.grid',
	    'ui.grid.selection',
	    'ui.grid.expandable',
	    'ui.grid.autoResize',
		'ui.grid.edit',
		'ui.grid.rowEdit',
		'ui.grid.cellNav',	    	    
		])

	.constant('CONFIG',{
		'appName':'Verona Color',
		/* env:dev */
    		'APIURL':'rest/',		
			'URL':'front/',	
    	/* env:dev:end */
    	
    	/* env:prod *#/
     		'APIURL':'rest/',		
			'URL':'front/',	
     	/* env:prod:end */
		
		'logUser':{},
		'logEmpresa':{},
		'cotizacion_dolar':0,
	})

	.run(['$rootScope', '$state', 'CONFIG', 'jwtHelper', 'Idle', '$mdDialog', '$mdToast', function ($rootScope, $state, CONFIG, jwtHelper, Idle, $mdDialog, $mdToast) {
		$rootScope.appSeccion="";
		$rootScope.formColor="background-50";
		$rootScope.formBcolor="background-400";
		
		/*********** IDLE *********************/
		function closeWarning(){
			$mdDialog.hide();
		}
		function openWarning(){
          	$mdDialog.show({
	          	templateUrl: 'front/app/common/templates/idle-warning.tpl.html',
	          	parent: angular.element(document.body),
	          	clickOutsideToClose:false
	        });
		}

        $rootScope.$on('IdleStart', function() {
        	openWarning();
        });        

        $rootScope.$on('IdleEnd', function() {
       		closeWarning();
        });        

        $rootScope.$on('IdleTimeout', function() {
        	closeWarning();
		   	$state.go('login');
        });

        /*
		$rootScope.$on('IdleWarn', function() {
        });*/


        /************** ROUTE CHANGE ************/
		$rootScope.$on('$stateChangeStart', function(event, toState) {
			$rootScope.preloader = false;

		   	if (toState.name === "login") {
		      	return; // already going to login
		   	}

		   	if (toState.name === "signup") {
		      	return; // already going to signup
		   	}

		   	var token=localStorage.getItem("token");

		   	if (!token) {
		   		event.preventDefault();
		   		$state.go('login');

		   	} else {
		        //VERIFICA PERMISOS 
		   	}

		   	var bool = jwtHelper.isTokenExpired(token);
        	if(bool === true){     
        		//console.log('token expirado')
		   		event.preventDefault();        	       
        		$state.go('login');
        	} else {
        		CONFIG.logUser=jwtHelper.decodeToken(token);
        	}
		});

		$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams,CONFIG) {
			$rootScope.preloader = true;

			if (from.name){
				localStorage.setItem("prevState",from.name);
			}
			var obj={	
						sectionName:'nada'
					};
			if (to.params){

				obj=to.params;
			}
			$rootScope.appSeccion=obj.sectionName;
		});

		$rootScope.$state=$state;

		$rootScope.showToast =  function(texto) {
	      $mdToast.show(
	        $mdToast.simple()
	          .textContent(texto)
	          .action('CERRAR')
	          .highlightAction(true)
	          .highlightClass('md-accent')
	          .position('top right')
	      ).then(function(response) {
	          if ( response == 'ok' ) {
	            $mdToast.hide();
	          }
	      });
	    };


	}])

	.filter('optionGridDropdownFilter', function () {
		return function (input, context) {
			if (context.constructor.name!='c'){
		        var fieldLevel = (context.editDropdownOptionsArray === undefined) ? context.col.colDef : context;
		        var map = fieldLevel.editDropdownOptionsArray;
		        var idField = fieldLevel.editDropdownIdLabel;
		        var valueField = fieldLevel.editDropdownValueLabel;
		        var initial = context.row.entity[context.col.field];
		        if (typeof map !== "undefined") {
		            for (var i = 0; i < map.length; i++) {
		                if (map[i][idField] == input) {
		                    return map[i][valueField];
		                }
		            }
		        }
		        else if (initial) {
		            return initial;
		        }
		        return input;
		    }
				
		};
	})

	/* ********* DIRECTIVAS ******************* */
	/* FORMATO NUMERICO */

	.directive('formatoNumerico', ['$filter', function ($filter)  {
	  	var decimalCases = 2,
        whatToSet = function (str) {
          /**
           * TODO:
           * don't allow any non digits character, except decimal seperator character
           */
          return str ? Number(str) : '';
        },
        whatToShow = function (num) {
          return $filter('number')(num, decimalCases);
        };

        return {
          restrict: 'A',
          require: 'ngModel',
          link: function (scope, element, attr, ngModel) {
            ngModel.$parsers.push(whatToSet);
            ngModel.$formatters.push(whatToShow);

            element.bind('blur', function() {
              element.val(whatToShow(ngModel.$modelValue));
            });
            element.bind('focus', function () {
              element.val(ngModel.$modelValue);
            });
          }
        };
	}])
	
	/* ********* FILTRO ******************* */
	/* MULTI FILTRO */
	.filter('multiFiltro', ['$filter', function($filter) {
	  	return function multiFiltro(items, predicates, group) {
	      predicates = predicates.split(' ');
	      if (group){
	        items=$filter('filter')(items, group, true);
	      }

	      angular.forEach(predicates, function(predicate) {
	        items = $filter('filter')(items, predicate.trim());
	      });
	      return items;
	  			
	    };
	}]);

})();