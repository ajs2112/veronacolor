-  
front
	* app.config.js
	* common\menu\menu.ctrl.js
	+ common\modules\listas.mdl.js
	+ common\services\lst-cotizacion.srv.js
	* common\login\*.*
	* common\menu\*.*
	* common\config\*.*
	* core\ventas\operaciones\vntoperaciones.detail.ctrl.js

rest
	+ controllers\Lst_cotizacion.php
	+ models\Lst_cotizacion_m.php
	+ SP\listas\listas_cotizaciones_detail_INS
	+ SP\listas\listas_cotizaciones_detail_SEL
	+ SP\listas\listas_cotizaciones_INS
	+ SP\listas\listas_cotizaciones_LAST
	+ SP\listas\listas_cotizaciones_LASTN
	+ SP\listas\listas_cotizaciones_ONE
	+ SP\listas\listas_cotizaciones_SEL
	+ SP\listas\listas_cotizaciones_UPD
	* SP\inventario\inventario_productos_SEL
	* SP\inventario\inventario_productos_ONE
	* SP\inventario_movimientos_SEL